﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using CrearNuevoMenu.Model;
namespace CrearNuevoMenu.Mapper
{
    public class MapperFactura
    {
        private DataAccess.DataAccess _dataAccess;
        private DataAccess.DataAccess DataAccess
        {
            get { return _dataAccess; }
        }
        public MapperFactura(DataAccess.DataAccess da)
        {
            _dataAccess = da;
        }
        public int InsertarFactura(Factura fac, out string outError)
        {
            string commandText;
            commandText = "INSERT INTO Factura (horaEmision, fechaEmision, ciCliente,codOrden) VALUES (@hEmi,@fEmi, @ciCl, @cOrd);";
            OleDbCommand command = new OleDbCommand(commandText);
            string hora = fac.horaEmision.ToString("hh:mm:ss tt");
            command.Parameters.AddWithValue("@hEmi", hora);
            string fecha = fac.horaEmision.ToString("mm/dd/yy");
            command.Parameters.AddWithValue("@fEmi", fecha);
            command.Parameters.AddWithValue("@ciCl", fac.ciCliente);
            command.Parameters.AddWithValue("@cOrd", fac.codOrden);
            int recordsAffected = 0;
            if (DataAccess.Open()){
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }
            return recordsAffected;
        }
        public DataView conseguirLasFacturas()
        {
            string sqlSentence;
            sqlSentence = "SELECT  a.* FROM Facturas a ORDER BY a.codFactura";
            DataView dw = null;
            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                if (dt != null)
                    dw = dt.DefaultView;
                DataAccess.Close();
            }
            return dw;
        }
        public List<Factura> conseguirFacturasList()
        {
            string sqlSentence;
            List<Factura> list = new List<Factura>();
            sqlSentence = "SELECT a.* FROM Factura a";
            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                for (int i = 0; i < dt.Rows.Count; i++)
                {//HoraEmision, FechaEmision, ciCliente,codOrden
                    DataRow dr = dt.Rows[i];
                    int id = Convert.ToInt32(dr["codFactura"]);
                    DateTime horE = Convert.ToDateTime(dr["HoraEmision"]);
                    DateTime fecE = Convert.ToDateTime(dr["FechaEmision"]);
                    int ciCl = Convert.ToInt32(dr["ciCliente"]);
                    int cOrd = Convert.ToInt32(dr["codOrden"]);
                    list.Add(new Factura(this, DataAccess, id, horE, fecE, ciCl, cOrd));
                }
                DataAccess.Close();
            }
            return list;
        }
        public int borrarFactura(Factura fac, out string outError)
        {
            string commandText;
            int recordsAffected = 0;
            commandText = "DELETE FROM Factura WHERE codFactura=@cdF";
            OleDbCommand command = new OleDbCommand(commandText);
            command.Parameters.Add("@cdF", SqlDbType.Int);
            command.Parameters["@cdF"].Value = fac.codFac;
            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }
            return recordsAffected;
        }
        public int actualizarFactura(Factura fact, out string outError)
        {
            string commandText;
            int recordsAffected = 0;
            commandText = "UPDATE Factura " +
                            "SET  horaEmision= @hEmi, fechaEmision= @fEmi, ciCliente=@ciCl, " +
                            "codOrden=@cOrd WHERE [codFactura] = @cFac";
            OleDbCommand command = new OleDbCommand(commandText);
            string hora = fact.horaEmision.ToString("hh:mm:ss tt");
            command.Parameters.AddWithValue("@hEmi", hora);
            string fecha = fact.horaEmision.ToString("mm/dd/yy");
            command.Parameters.AddWithValue("@fEmi", fecha);
            command.Parameters.AddWithValue("@ciCl", fact.ciCliente);
            command.Parameters.AddWithValue("@cOrd", fact.codOrden);
            command.Parameters.Add("@cFac", SqlDbType.Int);
            command.Parameters["@cFac"].Value = fact.codFac;
            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "No se pudo establecer la conexion.";
            }

            return recordsAffected;
        }
    }
}
