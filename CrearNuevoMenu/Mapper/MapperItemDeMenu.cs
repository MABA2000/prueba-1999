﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using CrearNuevoMenu.Model;

namespace CrearNuevoMenu.Mapper
{
    public class MapperItemDeMenu
    {
        private DataAccess.DataAccess _dataAccess;
        private DataAccess.DataAccess DataAccess
        {
            get { return _dataAccess; }
        }
        public MapperItemDeMenu(DataAccess.DataAccess da)
        {
            _dataAccess = da;
        }
        public int ModificiarItemMenu(ItemDeMenu item, out string outError)
        {
            string commandText;

            int recordsAffected = 0;

            commandText = "UPDATE ItemDeMenu SET precio = @precio WHERE [codItem] = @codItem";
            OleDbCommand command = new OleDbCommand(commandText);

            command.Parameters.AddWithValue("@precio", item.precio);
            command.Parameters.Add("@codItem", SqlDbType.Int);

            command.Parameters["@codItem"].Value = item.codItem;


            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }

            return recordsAffected;
        }
        public int InsertarItemDeMenu(ItemDeMenu itemDeMenu, out string outError)
        {

            string commandText;
            commandText = "INSERT INTO ItemDeMenu (codItem, codMenu, precio, fechaDeAdicion) VALUES (@codItem,@codMenu, @precio, @fechaDeAdicion);";
            OleDbCommand command = new OleDbCommand(commandText);
            command.Parameters.AddWithValue("@codItem", itemDeMenu.codItem);
            command.Parameters.AddWithValue("@codMenu", itemDeMenu.codMenu);
            command.Parameters.AddWithValue("@precio", itemDeMenu.precio);
            command.Parameters.AddWithValue("@fechaDeAdicion", itemDeMenu.fechaDeAdicion);
        
            int recordsAffected = 0;
            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }
            return recordsAffected;
        }
        public DataView conseguirLosItemsDeMenu(int idMenu)
        {
            string sqlSentence;
            sqlSentence = "SELECT Item.codItem, Item.nombre, Item.tipo, ItemDeMenu.precio, ItemDeMenu.fechaDeAdicion FROM Item INNER JOIN ItemDeMenu ON Item.codItem = ItemDeMenu.codItem WHERE " + idMenu + "= ItemDeMenu.CodMenu;";
            DataView dw = null;
            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                if (dt != null)
                    dw = dt.DefaultView;
                DataAccess.Close();
            }
            return dw;
        }
        public int conseguirCantidadDeItemsDelMenu(int idMenu)
        {
            string sqlSentence;
            sqlSentence = "SELECT a.* FROM ItemDeMenu a";
            int cantidadDeItems = 0;
            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    if (Convert.ToInt32(dr["codMenu"]) == idMenu)
                    {
                        cantidadDeItems++;
                    }

                }
                DataAccess.Close();
            }
            return cantidadDeItems;
        }
        public List<ItemDeMenu> conseguirItemsDeMenuList(int idMenu)
        {
            string sqlSentence;
            List<ItemDeMenu> list = new List<ItemDeMenu>();
            sqlSentence = "SELECT Item.codItem, Item.nombre, Item.tipo, ItemDeMenu.CodMenu,ItemDeMenu.precio, ItemDeMenu.fechaDeAdicion FROM Item INNER JOIN ItemDeMenu ON Item.codItem = ItemDeMenu.codItem WHERE " + idMenu + "= ItemDeMenu.CodMenu;";
            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    int codItem = Convert.ToInt32(dr["codItem"]);
                    int codMenu = Convert.ToInt32(dr["codMenu"]);
                    float precio = Convert.ToSingle(dr["precio"]);
                    DateTime fechaDeAdicion = Convert.ToDateTime(dr["fechaDeAdicion"]);
                    list.Add(new ItemDeMenu(this, DataAccess, codItem, codMenu, precio, fechaDeAdicion));
                }
                DataAccess.Close();
            }
            return list;
        }
     
        public int eliminarItemDElMenu(ItemDeMenu item, out string outError)
        {
            string commandText;

            int recordsAffected = 0;

            commandText = "DELETE FROM ItemDeMenu WHERE codMenu=@codMenu AND codItem=@codItem;";
            OleDbCommand command = new OleDbCommand(commandText);
            //DELETE FROM ItemDeMenu WHERE codMenu = 1 AND codItem = 3;
            //command.Parameters.Add("@codItem", SqlDbType.Int);
            command.Parameters.Add("@codMenu", SqlDbType.Int);
            command.Parameters.Add("@codItem", SqlDbType.Int);


            command.Parameters["@codMenu"].Value= item.codMenu;
            command.Parameters["@codItem"].Value= item.codItem;


            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }

            return recordsAffected;
        }
        public bool itemExisteEnMenu(int codMenu, int codItem)
        {
            string sqlSentence;
            bool exists = false;

            sqlSentence = "SELECT a.* FROM ItemDeMenu a WHERE a.CodMenu="+codMenu+" and a.CodItem="+ codItem+";" ;

            if (DataAccess.Open())
            {
                var dt = DataAccess.GetDataTable(sqlSentence);
                exists = dt.Rows.Count > 0;
            }
            DataAccess.Close();
            return exists;
        }
    }
}
