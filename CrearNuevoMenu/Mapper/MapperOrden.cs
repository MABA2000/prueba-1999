﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using CrearNuevoMenu.Model;
namespace CrearNuevoMenu.Mapper
{
    public class MapperOrden
    {
        private DataAccess.DataAccess _dataAccess;
        private DataAccess.DataAccess DataAccess
        {
            get { return _dataAccess; }
        }
        public MapperOrden(DataAccess.DataAccess da)
        {
            _dataAccess = da;
        }

        public int InsertarOrden(Orden ord, out string outError)
        {
            string commandText;
            commandText = "INSERT INTO Orden (estadoItem, numeroMesa, horaDeFinalización)" +
                " VALUES (@eItm,@nMes, @hDFi);";
            OleDbCommand command = new OleDbCommand(commandText);
            command.Parameters.AddWithValue("@eItm", ord.estadoItem? 1:0);
            command.Parameters.AddWithValue("@nMes", ord.numeroMesa);
            command.Parameters.AddWithValue("@hDFi", ord.horaDeFinalizacion);
            int filaAfectada = 0;
            if (DataAccess.Open())
            {
                filaAfectada = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }
            return filaAfectada;
        }
        public DataView conseguirLasOrdenes()
        {
            string sqlSentence;
            sqlSentence = "SELECT  a.* FROM Orden a ORDER BY a.codOrdenDESC";
            DataView dw = null;
            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                if (dt != null)
                    dw = dt.DefaultView;
                DataAccess.Close();
            }
            return dw;
        }
        private List<Orden> conseguirOrdenes(string sqlSentence)
        {
            List<Orden> list = new List<Orden>();
            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    int ord = Convert.ToInt32(dr["codOrden"]);
                    bool est = Convert.ToInt32(dr["estadoItem"]) == 1;
                    int numMesa = Convert.ToInt32(dr["numeroMesa"]);
                    DateTime hora = Convert.ToDateTime(dr["horaDeFinalización"]);

                    list.Add(new Orden(this, DataAccess, ord, est, numMesa, hora));
                }
                DataAccess.Close();
            }
            return list;
        }
        public List<Orden> conseguirOrdenesList()
        {
            string sqlSentence = "SELECT a.* FROM Orden a ORDER BY a.codOrden DESC";
            return conseguirOrdenes(sqlSentence);
        }
        public List<Orden> conseguirOrdenesList(int numM)
        {
            string sqlSentence="SELECT a.* FROM Orden a WHERE numeroMesa="+numM.ToString()+ " ORDER BY a.codOrden DESC;";
            return conseguirOrdenes(sqlSentence);
        }
            public int borrarOrden(Orden ord, out string outError)
        {
            string commandText;
            int filaAfectada = 0;
            commandText = "DELETE FROM Orden WHERE codOrden=@cOrd";
            OleDbCommand command = new OleDbCommand(commandText);
            command.Parameters.Add("@cOrd", SqlDbType.Int);
            command.Parameters["@cOrd"].Value = ord.codOrden;
            if (DataAccess.Open())
            {
                filaAfectada = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }
            return filaAfectada;
        }
        public int actualizarOrdenes(Orden ord, out string outError)
        {
            string commandText;
            int filaAfectada = 0;
            commandText = "UPDATE Orden " +
                            "SET  estadoItem= @eItm, numeroMesa= @nMes, " +
                            "horaDeFinalizacion=@hDFi WHERE [codOrden] = @cOrd";
            OleDbCommand command = new OleDbCommand(commandText);
            command.Parameters.AddWithValue("@eItm", ord.estadoItem);
            command.Parameters.AddWithValue("@nMes", ord.numeroMesa);
            command.Parameters.AddWithValue("@hDFi", ord.horaDeFinalizacion);
            command.Parameters.Add("cOrd", SqlDbType.Int);
            command.Parameters["@cOrd"].Value = ord.codOrden;
            if (DataAccess.Open())
            {
               filaAfectada = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "No se pudo establecer la conexion.";
            }

            return filaAfectada;
        }
    }
}