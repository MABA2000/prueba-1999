﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using CrearNuevoMenu.Model;

namespace CrearNuevoMenu.Mapper
{
    public class MapperItem
    {
        private DataAccess.DataAccess _dataAccess;
        private DataAccess.DataAccess DataAccess
        {
            get { return _dataAccess; }
        }
        public MapperItem(DataAccess.DataAccess da)
        {
            _dataAccess = da;
        }
        public int InsertarItem(Item item, out string outError)
        {
           
            string commandText;
            commandText = "INSERT INTO Item (nombre, tiempoElaboracion, precioDeElaboracion, descripcion) VALUES (@nom,@tiempoE, @precio, @descripcion);";
            OleDbCommand command = new OleDbCommand(commandText);
            command.Parameters.AddWithValue("@nom", item.nombre);
            command.Parameters.AddWithValue("@tiempoE", item.tiempoElaboracion);
            command.Parameters.AddWithValue("@precio", item.precioDeElaboracion);
            command.Parameters.AddWithValue("@descripcion", item.descripcion);

            int recordsAffected = 0;
            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }
            return recordsAffected;
        }
        public DataView conseguirLosItems()
        {
            string sqlSentence;

            sqlSentence = "SELECT  a.* FROM Item a ORDER BY a.Code";

            DataView dw = null;
            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                if (dt != null)
                    dw = dt.DefaultView;
                DataAccess.Close();
            }
            return dw;
        }
        public List<Item> conseguirItemsList()
        {
            string sqlSentence;

            List<Item> list = new List<Item>();

            sqlSentence = "SELECT a.* FROM Item a";
          

            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    int codItem = Convert.ToInt32(dr["codItem"]);
                    string Nombre = dr["nombre"].ToString() ;
                    int TiempoElaboracion = Convert.ToInt32(dr["tiempoElaboracion"]);//en minutos
                    int gradoAlc = Convert.ToInt32(dr["GradoAlcohólico"]);

                    int Calorias = Convert.ToInt32(dr["Calorías"]);
                    float PrecioElaboracion = float.Parse(dr["precioDeElaboracion"].ToString());
                    //MessageBox.Show(gradoAlc.ToString()+"    "+PrecioElaboracion.ToString(), "VALOR PREECIO DE ELABORACION");





                    string Descripcion = dr["descripcion"].ToString();
                    string Tipo = dr["tipo"].ToString();
                    int CIAdministrador = Convert.ToInt32(dr["CI"]);

                    Item item = new Item(this, DataAccess, codItem, Nombre, TiempoElaboracion, gradoAlc, Calorias, PrecioElaboracion, Descripcion, Tipo, CIAdministrador);
                    //  item.mostraValores();
                    list.Add(item);
                }
                DataAccess.Close();
            }
            return list;
        }
        public int borrarItem(Item men, out string outError)
        {
            string commandText;

            int recordsAffected = 0;

            commandText = "DELETE FROM Item WHERE Id=@Id";
            OleDbCommand command = new OleDbCommand(commandText);
            command.Parameters["@codItem"].Value = men.codItem;
            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }
            return recordsAffected;
        }
        public int conseguirCantidadDeItemsDelSistema()
        {
            string sqlSentence;
            sqlSentence = "SELECT a.* FROM Item a";
            int cantidadDeItems = 0;
            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                cantidadDeItems= dt.Rows.Count;
                DataAccess.Close();
            }
            return cantidadDeItems;
        }
        public int DeleteItem(Item item, out string outError)
        {
            string commandText;

            int recordsAffected = 0;

            commandText = "DELETE FROM Item WHERE codItem=@codItem";
            OleDbCommand command = new OleDbCommand(commandText);
            command.Parameters.Add("@codItem", SqlDbType.Int);
            command.Parameters["@codItem"].Value = item.codItem;

            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }

            return recordsAffected;
        }
        public int UpdateItem(Item item, out string outError)
        {
            string commandText;

            int recordsAffected = 0;

            commandText = "UPDATE Item SET Description = @descripcion WHERE [codItem] = @codItem";
            OleDbCommand command = new OleDbCommand(commandText);

            command.Parameters.AddWithValue("@descripcion", item.descripcion);

            command.Parameters.Add("@codItem", SqlDbType.Int);
            command.Parameters["@codItem"].Value = item.codItem;


            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }

            return recordsAffected;
        }
        public int InsertItem(Item item, out string outError)
        {
            string sqlSentence;





            sqlSentence = "INSERT INTO Item (nombre, tiempoElaboracion, GradoAlcohólico, Calorías, precioDeElaboracion,descripcion,tipo, CI) VALUES (@nom,@tiem, @grad, @cal,@pre,@des,@tip,@ci);";

            OleDbCommand command = new OleDbCommand(sqlSentence);            command.Parameters.AddWithValue("@nom", item.nombre);            command.Parameters.AddWithValue("@tiem", item.tiempoElaboracion);            command.Parameters.AddWithValue("@grad", item.GradoAlc);            command.Parameters.AddWithValue("@cal", item.Calorias);
            command.Parameters.AddWithValue("@pre", item.precioDeElaboracion);
            command.Parameters.AddWithValue("@des", item.descripcion);
            command.Parameters.AddWithValue("@tip", item.tipo);
            command.Parameters.AddWithValue("@ci", item.ci);


            // MessageBox.Show(sqlSentence,"sqlasp");

            int recordsAffected = 0;
            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }
            return recordsAffected;
        }
        public bool ItemCodeExists(string code)
        {
            string sqlSentence;
            bool exists = false;

            sqlSentence = "SELECT a.* FROM Item a WHERE a.nombre='" + code + "'";

            if (DataAccess.Open())
            {
                var dt = DataAccess.GetDataTable(sqlSentence);
                exists = dt.Rows.Count > 0;
            }
            DataAccess.Close();
            return exists;
        }
    }
}
