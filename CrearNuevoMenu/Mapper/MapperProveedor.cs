﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using CrearNuevoMenu.Model;
using CrearNuevoMenu.Controller;
using System.Windows.Forms;


namespace CrearNuevoMenu.Mapper
{
    public class MapperProveedor
    {
        private DataAccess.DataAccess _dataAccess;
        private DataAccess.DataAccess DataAccess
        {
            get { return _dataAccess; }
        }
        public MapperProveedor(DataAccess.DataAccess da)
        {
            _dataAccess = da;
        }
        public int InsertarProveedor(Proveedor prov, out string outError)
        {
            

            string commandText;
            commandText = "INSERT INTO Proveedor (CI,Nombre,DirLocal,NumCel,Email) VALUES (@CI,@Nombre,@DirLocal,@NumCel,@Email);";
            OleDbCommand command = new OleDbCommand(commandText);
            command.Parameters.AddWithValue("@CI", prov.ci);
            command.Parameters.AddWithValue("@Nombre", prov.nombre);
            command.Parameters.AddWithValue("@DirLocal", prov.dirLocal);
            command.Parameters.AddWithValue("@NumCel", prov.numCel);
            command.Parameters.AddWithValue("@Email", prov.email);

            int recordsAffected = 0;
            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }
            return recordsAffected;
        }
        public DataView conseguirLosProveedors()
        {
            string sqlSentence;

            sqlSentence = "SELECT  a.* FROM Proveedor a ORDER BY a.Code";

            DataView dw = null;
            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                if (dt != null)
                    dw = dt.DefaultView;
                DataAccess.Close();
            }
            return dw;
        }
        public List<Proveedor> conseguirProveedorsList()
        {
            string sqlSentence;

            List<Proveedor> list = new List<Proveedor>();

            sqlSentence = "SELECT a.* FROM Proveedor a";
         

            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    int CI = Convert.ToInt32(dr["CI"]);
                    string nombre = dr["nombre"].ToString();
                    string email = dr["email"].ToString();
                    string dirLocal = dr["dirLocal"].ToString();
                    int numCel = Convert.ToInt32(dr["numCel"]);
                    list.Add(new Proveedor(this, DataAccess, CI, nombre, email, dirLocal, numCel));
                }
                DataAccess.Close();
            }
            return list;
        }
        public List<string> conseguirNombresDeProveedorsList()
        {
            string sqlSentence;

            List<string> list = new List<string>();

            sqlSentence = "SELECT a.* FROM Proveedor a";


            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    string nombre = dr["nombre"].ToString();
                    list.Add(nombre);
                }
                DataAccess.Close();
            }
            return list;
        }
        public int borrarProveedor(Proveedor insum, out string outError)
        {
            string commandText;

            int recordsAffected = 0;


            commandText = "DELETE FROM Proveedor WHERE CI=@CI";
            OleDbCommand command = new OleDbCommand(commandText);
            command.Parameters.Add("@CI", SqlDbType.Int);
            command.Parameters["@CI"].Value = insum.ci;
            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }
            return recordsAffected;
        }
        public int ModificiarProveedor(Proveedor Proveedor, out string outError)
        {
            string commandText;

            int recordsAffected = 0;

            commandText = "UPDATE Proveedor SET nombre = @nombre, email=@email, dirLocal=@dirLocal ,numCel=@numCel WHERE [CI] = @CI";
            OleDbCommand command = new OleDbCommand(commandText);

            command.Parameters.AddWithValue("@nombre", Proveedor.nombre);
            command.Parameters.AddWithValue("@email", Proveedor.email);
            command.Parameters.AddWithValue("@dirLocal", Proveedor.dirLocal);
            command.Parameters.AddWithValue("@numCel", Proveedor.numCel);
            command.Parameters.Add("@CI", SqlDbType.Int);

            command.Parameters["@CI"].Value = Proveedor.ci;


            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }

            return recordsAffected;
        }
    }
}
