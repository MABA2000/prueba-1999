﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using CrearNuevoMenu.Model;
namespace CrearNuevoMenu.Mapper
{
    class MapperCredencial
    {
        private DataAccess.DataAccess _dataAccess;
        private DataAccess.DataAccess DataAccess
        {
            get { return _dataAccess; }
        }
        public MapperCredencial(DataAccess.DataAccess da)
        {
            _dataAccess = da;
        }
        public Credencial buscarUsuario(Credencial cred, out string outError)
        {
            string commandText;
            string e = cred.email;
            string c = cred.contraseña;
            //commandText = "SELECT a.* FROM Credencial a WHERE Email='@ema' And Contraseña='@cont';";
            commandText = "SELECT Ci, Asociacion FROM Credencial WHERE Email= '" +
                cred.email+ "' And Contraseña= '" + cred.contraseña+ "'";
            OleDbCommand command = new OleDbCommand(commandText);
            DataTable dt;
            DataRow dr;
            if (DataAccess.Open())
            {

                dt = DataAccess.ExecuteCommand(command);
                outError = "";
                if (dt != null && dt.Rows.Count==1) {
                    dr = dt.Rows[0];
                    cred.ci = Convert.ToInt32(dr["Ci"]);
                    cred.asociacion = dr["Asociacion"].ToString();
                    //if (cred.ci== 123)// && cred.asociacion=="administrador")
                     cred.encontrado = true;
                }
            }
            else
            {
                outError = "Conexion no pudo abrir.";
            }
            return cred;
        }
    }
}
