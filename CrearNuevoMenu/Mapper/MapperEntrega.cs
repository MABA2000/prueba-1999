﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using CrearNuevoMenu.Model;
using CrearNuevoMenu.Controller;
using System.Windows.Forms;


namespace CrearNuevoMenu.Mapper
{
        public class MapperEntrega
    {
        private DataAccess.DataAccess _dataAccess;
        private DataAccess.DataAccess DataAccess
        {
            get { return _dataAccess; }
        }
        public MapperEntrega(DataAccess.DataAccess da)
        {
            _dataAccess = da;
        }
        public int InsertarEntrega(Entrega ent, out string outError)
        {

           

            string commandText;
            commandText = "INSERT INTO Entregas (PrecioUnidad, Cantidad, fechaDeEntrega,CodInsumo, CIProveedor) VALUES (@PrecioUnidad,@Cantidad, @fechaDeEntrega,@CodInsumo, @CIProveedor);";
            OleDbCommand command = new OleDbCommand(commandText);
            command.Parameters.AddWithValue("@PrecioUnidad", ent.PrecioUnidad);
            command.Parameters.AddWithValue("@Cantidad", ent.Cantidad);
            command.Parameters.AddWithValue("@fechaDeEntrega", ent.fechaDeEntrega);
            command.Parameters.AddWithValue("@CodInsumo", ent.CodInsumo);
            command.Parameters.AddWithValue("@CIProveedor", ent.CIProveedor);

            int recordsAffected = 0;
            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }
            return recordsAffected;
        }
        public int editarEntrega(Entrega ent, out string outError)
        {
            string commandText;
            commandText = "UPDATE  Entregas set @PrecioUnidad,@Cantidad, @fechaDeEntrega,@CodInsumo, @CIProveedor WHERE [codEntrega]=@codEntrega);";
            OleDbCommand command = new OleDbCommand(commandText);
            command.Parameters.AddWithValue("@PrecioUnidad", ent.PrecioUnidad);
            command.Parameters.AddWithValue("@Cantidad", ent.Cantidad);
            command.Parameters.AddWithValue("@fechaDeEntrega", ent.fechaDeEntrega);
            command.Parameters.AddWithValue("@CodInsumo", ent.CodInsumo);
            command.Parameters.AddWithValue("@CIProveedor", ent.CIProveedor);
            command.Parameters.AddWithValue("@codEntrega", ent.CodEntrega);
            command.Parameters.Add("@codEntrega", SqlDbType.Int);

            int recordsAffected = 0;
            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }
            return recordsAffected;
        }
        public DataView conseguirLasEntregasPorSentencia()
        {
            string sqlSentence;
            
            sqlSentence = "SELECT Entregas.CodEntrega, Proveedor.Nombre, Insumos.Nombre, Entregas.Cantidad, Insumos.Unidad, Entregas.PrecioUnidad, Entregas.fechaDeEntrega FROM Proveedor INNER JOIN (Insumos INNER JOIN Entregas ON Insumos.CodInsumo = Entregas.CodInsumo) ON Proveedor.CI = Entregas.CIProveedor;";

            DataView dw = null;
            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                if (dt != null)
                    dw = new DataView(dt);
                DataAccess.Close();
            }
            return dw;
        }
        public List<Entrega> conseguirEntregasList()
        {
            string sqlSentence;

            List<Entrega> list = new List<Entrega>();

            sqlSentence = "SELECT a.* FROM Entregas a";
           
            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    int CodEntrega = Convert.ToInt32(dr["CodEntrega"]);
                    int CodInsumo = Convert.ToInt32(dr["CodInsumo"]);
                    int CIProveedor = Convert.ToInt32(dr["CIProveedor"]);
                    float PrecioUnidad = Convert.ToSingle(dr["PrecioUnidad"]);
                    DateTime fechaDeEntrega = Convert.ToDateTime(dr["fechaDeEntrega"]);
                    float Cantidad = Convert.ToSingle(dr["Cantidad"]);
                    list.Add(new Entrega(this, DataAccess, CodEntrega,CodInsumo, CIProveedor, PrecioUnidad, Cantidad, fechaDeEntrega));
                }
                DataAccess.Close();
            }
            return list;
        }
        public int borrarEntrega(Entrega ent, out string outError)
        {
            string commandText;

            int recordsAffected = 0;

            commandText = "DELETE FROM Entregas WHERE codEntrega=@codEntrega";
            OleDbCommand command = new OleDbCommand(commandText);
            command.Parameters.Add("@codEntrega", SqlDbType.Int);
            command.Parameters["@codEntrega"].Value = ent.CodEntrega;
            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }
            return recordsAffected;
        }
        public int ModificiarEntrega(Entrega Entrega, out string outError)
        {
            string commandText;

            int recordsAffected = 0;

            commandText = "UPDATE Entregas SET CodInsumo = @CodInsumo, CIProveedor=@CIProveedor, PrecioUnidad=@PrecioUnidad, Cantidad=@Cantidad WHERE [codEntrega] = @codEntrega";
            OleDbCommand command = new OleDbCommand(commandText);

            command.Parameters.AddWithValue("@CodInsumo", Entrega.CodInsumo);
            command.Parameters.AddWithValue("@CIProveedor", Entrega.CIProveedor);
            command.Parameters.AddWithValue("@PrecioUnidad", Entrega.PrecioUnidad);
            command.Parameters.AddWithValue("@Cantidad", Entrega.Cantidad);
            command.Parameters.Add("@codEntrega", SqlDbType.Int);

            command.Parameters["@codEntrega"].Value = Entrega.CodEntrega;


            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }

            return recordsAffected;
        }
    }
}
