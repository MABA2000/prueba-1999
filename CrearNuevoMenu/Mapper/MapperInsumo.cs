﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using CrearNuevoMenu.Model;
using CrearNuevoMenu.Controller;
using System.Windows.Forms;


namespace CrearNuevoMenu.Mapper
{
    public class MapperInsumo
    {
        private DataAccess.DataAccess _dataAccess;
        private DataAccess.DataAccess DataAccess
        {
            get { return _dataAccess; }
        }
        public MapperInsumo(DataAccess.DataAccess da)
        {
            _dataAccess = da;
        }
        public int InsertarInsumoDeUnItem (Insumo insumo, out string outError)
        {
            
             
            /*
            string commandText;
            commandText = "INSERT INTO Insumos (nombre, unidad) VALUES (@nombre,@cantidad);";
            OleDbCommand command = new OleDbCommand(commandText);
            command.Parameters.AddWithValue("@nombre", men.nombre);
            //command.Parameters.AddWithValue("@cantidad", men.cantidad);
            command.Parameters.AddWithValue("@unidad", men.unidad);*/

            string sqlSentence = "INSERT INTO InsumosDeItem (CodInsumo, CodItem, Medida, Cantidad) VALUES (@codIn,@codIt, @med, @cant);";

            OleDbCommand command = new OleDbCommand(sqlSentence);
            command.Parameters.AddWithValue("@codIn", insumo.codInsumo);
            command.Parameters.AddWithValue("@codIt", insumo.CodItem);
            command.Parameters.AddWithValue("@med", insumo.Medida);
            command.Parameters.AddWithValue("@cant", insumo.cantidad);

            int recordsAffected = 0;
            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }
            return recordsAffected;
        }
        public int InsertarInsumo(Insumo men, out string outError) 
        {


            
            string commandText;
            commandText = "INSERT INTO Insumos (nombre, unidad) VALUES (@nombre,@cantidad);";
            OleDbCommand command = new OleDbCommand(commandText);
            command.Parameters.AddWithValue("@nombre", men.nombre);
            //command.Parameters.AddWithValue("@cantidad", men.cantidad);
            command.Parameters.AddWithValue("@unidad", men.unidad);

            int recordsAffected = 0;
            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }
            return recordsAffected;
        }
        public DataView conseguirLosInsumos()
        {
            string sqlSentence;

            sqlSentence = "SELECT  a.* FROM Insumos a ORDER BY a.Code";

            DataView dw = null;
            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                if (dt != null)
                    dw = dt.DefaultView;
                DataAccess.Close();
            }
            return dw;
        }
        public int DeleteItem(Insumo insumo, out string outError)
        {
            string commandText;

            int recordsAffected = 0;
           // MessageBox.Show("CodInsumo = " + insumo.codInsumo.ToString());
            commandText = "DELETE FROM InsumosDeItem WHERE CodInsumo=@CodInsumo";
            OleDbCommand command = new OleDbCommand(commandText);
            command.Parameters.Add("@CodInsumo", SqlDbType.Int);
            command.Parameters["@CodInsumo"].Value = insumo.codInsumo;

            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }

            return recordsAffected;
        }
        public List<Insumo> GetAllInsumodeUnItem(string codItem)//dado un Item que muestre sus insumo que tiene
        {
            string sqlSentence;

            List<Insumo> list = new List<Insumo>();



            sqlSentence = "SELECT  a.CodInsumo,a.Nombre,a.Unidad, b.Medida, b.Cantidad FROM Insumos a , InsumosDeItem b WHERE a.CodInsumo=b.CodInsumo and b.CodItem=" + codItem;



            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];


                    int codInsumo = Convert.ToInt32(dr["CodInsumo"]);
                    string Nombre = dr["Nombre"].ToString();
                    string Unidad = dr["Unidad"].ToString();
                    double Medida = Convert.ToDouble(dr["Medida"]);
                    int Cantidad = Convert.ToInt32(dr["Cantidad"]);


                    list.Add(new Insumo(this, DataAccess, codInsumo, Convert.ToInt32(codItem), Nombre, Unidad, Medida, Cantidad));
                }
                DataAccess.Close();
            }
            return list;
        }
        public List<Insumo> GetInsumoSimpleConditional(string codeItem)//insumos que no estan en el item
        {
            string sqlSentence;

            List<Insumo> list = new List<Insumo>();



            sqlSentence = "SELECT a.* FROM Insumos a where a.codInsumo not in (SELECT x.codInsumo FROM Insumos x,InsumosDeItem b where x.codInsumo=b.codInsumo and b.codItem=" + codeItem + ")";



            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    int codInsumo = Convert.ToInt32(dr["CodInsumo"]);
                    string Nombre = dr["Nombre"].ToString();
                    float cantidad = 0;
                    string Unidad = dr["Unidad"].ToString();



                    list.Add(new Insumo(this, DataAccess, codInsumo, Nombre,cantidad, Unidad));
                }
                DataAccess.Close();
            }
            return list;
        }
        public bool existeInsumo(string nombre)
        {
            string sqlSentence;
            bool exists = false;

            sqlSentence = "SELECT a.* FROM Insumos a WHERE a.Nombre ='" + nombre+"';";

            if (DataAccess.Open())
            {
                var dt = DataAccess.GetDataTable(sqlSentence);
                exists = dt.Rows.Count > 0;
            }
            DataAccess.Close();
            return exists;
        }
        public List<Insumo> conseguirInsumosList()
        {
            string sqlSentence;

            List<Insumo> list = new List<Insumo>();

            sqlSentence = "SELECT a.* FROM Insumos a";
            

            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    int id = Convert.ToInt32(dr["codInsumo"]);
                    string nam = dr["nombre"].ToString();
                    float cantidad = 0;// Convert.ToSingle(dr["cantidad"]);
                    string unidad =  dr["unidad"].ToString();
                    list.Add(new Insumo(this, DataAccess, id, nam, cantidad, unidad));
                }
                DataAccess.Close();
            }
            return list;
        }
        public int borrarInsumo(Insumo insum, out string outError)
        {
            string commandText;

            int recordsAffected = 0;

            commandText = "DELETE FROM Insumos WHERE codInsumo=@codInsumo";
            OleDbCommand command = new OleDbCommand(commandText);
            command.Parameters.Add("@codInsumo", SqlDbType.Int);
            command.Parameters["@codInsumo"].Value = insum.codInsumo;
            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }
            return recordsAffected;
        }
        public int ModificiarInsumo(Insumo insumo, out string outError)
        {
            string commandText;

            int recordsAffected = 0;

            commandText = "UPDATE Insumos SET nombre = @nombre, unidad=@unidad WHERE [codInsumo] = @codInsumo";
            OleDbCommand command = new OleDbCommand(commandText);

            command.Parameters.AddWithValue("@nombre", insumo.nombre);
            command.Parameters.AddWithValue("@unidad", insumo.unidad);
            command.Parameters.Add("@codInsumo", SqlDbType.Int);

            command.Parameters["@codInsumo"].Value = insumo.codInsumo;


            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }

            return recordsAffected;
        }
        public List<Insumo> GetAllInsumosDelItemList()
        {
            string sqlSentence;

            List<Insumo> list = new List<Insumo>();



            sqlSentence = "SELECT  a.CodInsumo,a.Nombre,a.Unidad, b.Medida, b.Cantidad FROM Insumos a , InsumosDeItem b WHERE a.CodInsumo=b.CodInsumo ";



            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    int codInsumo = Convert.ToInt32(dr["CodInsumo"]);
                    string Nombre = dr["Nombre"].ToString();
                    string Unidad = dr["Unidad"].ToString();
                    double Medida = Convert.ToDouble(dr["Medida"]);
                    int Cantidad = Convert.ToInt32(dr["Cantidad"]);


                    list.Add(new Insumo(this, DataAccess, codInsumo, -1, Nombre, Unidad, Medida, Cantidad));
                }
                DataAccess.Close();
            }
            return list;
        }
        public List<string> conseguirNombresDeInsumosList()
        {
            string sqlSentence;

            List<string> list = new List<string>();

            sqlSentence = "SELECT a.* FROM Insumos a";


            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    string nombre = dr["nombre"].ToString();
                    list.Add(nombre);
                }
                DataAccess.Close();
            }
            return list;
        }
    }
}
