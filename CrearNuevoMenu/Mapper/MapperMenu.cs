﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using CrearNuevoMenu.Model;

namespace CrearNuevoMenu.Mapper
{
    public class MapperMenu
    {
        private DataAccess.DataAccess _dataAccess;
        private DataAccess.DataAccess DataAccess
        {
            get { return _dataAccess; }
        }
        public MapperMenu(DataAccess.DataAccess da)
        {
            _dataAccess = da;
        }
        public int actualizarMenu(Menu men, out string outError)
        {
            string commandText;
            int recordsAffected = 0;
            commandText = "UPDATE Menu " +
                            "SET  Nombre= @nom, Fecha= @fec, Descripcion=@des, " +
                            "Visible=@vis WHERE [Id] = @Id";
            OleDbCommand command = new OleDbCommand(commandText);
            command.Parameters.AddWithValue("@nom", men.nombre);
            command.Parameters.AddWithValue("@fec", men.conseguirFecha());
            command.Parameters.AddWithValue("@des", men.descripcion);
            command.Parameters.AddWithValue("@vis", men.visible ? 1 : 0);
            command.Parameters.Add("@Id", SqlDbType.Int);
            command.Parameters["@Id"].Value = men.Id;
            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "No se pudo establecer la conexion.";
            }

            return recordsAffected;
        }
        public int InsertarMenu(Menu men, out string outError)
        {
            string commandText;
            commandText = "INSERT INTO Menu (nombre, fecha, descripcion, visible) VALUES (@nom,@fec, @des, @vis);";
            OleDbCommand command = new OleDbCommand(commandText);
            command.Parameters.AddWithValue("@nom", men.nombre);
            command.Parameters.AddWithValue("@fec", men.conseguirFecha());
            command.Parameters.AddWithValue("@des", men.descripcion);
            command.Parameters.AddWithValue("@vis", men.visible ? 1 : 0);

            int recordsAffected = 0;
            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }
            return recordsAffected;
        }
        public DataView conseguirLosMenus()
        {
            string sqlSentence;

            sqlSentence = "SELECT  a.* FROM Menu a ORDER BY a.Code";

            DataView dw = null;
            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                if (dt != null)
                    dw = dt.DefaultView;
                DataAccess.Close();
            }
            return dw;
        }
        public List<Menu> conseguirMenusList()
        {
            string sqlSentence;

            List<Menu> list = new List<Menu>();

            sqlSentence = "SELECT a.* FROM Menu a";


            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    int id = Convert.ToInt32(dr["Id"]);
                    string nam = dr["nombre"].ToString();
                    DateTime fec = Convert.ToDateTime(dr["fecha"]);
                    string des = dr["descripcion"].ToString();
                  //  int cant = Convert.ToInt32(dr["cantidad"]);
                    bool vis = Convert.ToInt32(dr["visible"]) == 1;
                    list.Add(new Menu(this, DataAccess, id, nam, fec ,des, vis));
                }
                DataAccess.Close();
            }
            return list;
        }
        public int borrarMenu(Menu men, out string outError)
        {
            string commandText;

            int recordsAffected = 0;

            commandText = "DELETE FROM Menu WHERE Id=@Id";
            OleDbCommand command = new OleDbCommand(commandText);
            command.Parameters.Add("@Id", SqlDbType.Int);
            command.Parameters["@Id"].Value = men.Id;
            if (DataAccess.Open())
            {
                recordsAffected = DataAccess.ExecuteCommand(command, out outError);
            }
            else
            {
                outError = "Connection could´t be opened";
            }
            return recordsAffected;
        }
    }
}
