﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using CrearNuevoMenu.Model;
namespace CrearNuevoMenu.Mapper
{
    public class MapperPersonal
    {
        private DataAccess.DataAccess _dataAccess;
        private DataAccess.DataAccess DataAccess
        {
            get { return _dataAccess; }
        }
        public MapperPersonal(DataAccess.DataAccess da)
        {
            _dataAccess = da;
        }
        public DataView conseguirPersonales()
        {
            string sqlSentence;

            sqlSentence = "SELECT  a.* FROM Personal a ORDER BY a.ApellidoPaterno;";
            DataView dw = null;
            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                if (dt != null)
                    dw = dt.DefaultView;
                DataAccess.Close();
            }
            return dw;
        }
        public Personal conseguirPersonal(int ciClave)
        {
            Personal per;
            string sqlSentence;
            sqlSentence = "SELECT a.* FROM Personal a where ci=" + ciClave+";";
            if (DataAccess.Open())
            {
                DataTable dt= DataAccess.GetDataTable(sqlSentence);
                if (dt.Rows.Count==1) {
                    DataRow dr = dt.Rows[0];
                    int ci = Convert.ToInt32(dr["Ci"]);
                    string nombre = dr["nombre"].ToString();
                    string apellidoPat = dr["ApellidoPaterno"].ToString();
                    string apellidoMat = dr["ApellidoMaterno"].ToString();
                    DateTime fechaNac = Convert.ToDateTime(dr["FechaNacimiento"]);
                    DateTime fechaCont = Convert.ToDateTime(dr["FechaContrato"]);
                    int sueldo = Convert.ToInt32(dr["Sueldo"]);
                    int numCel = Convert.ToInt32(dr["NumeroCelular"]);
                    string direccion = dr["Direccion"].ToString();
                    per = new Personal(this, DataAccess, ci, nombre, apellidoPat, apellidoMat, fechaNac,
                            fechaCont, sueldo, numCel, direccion);
                    DataAccess.Close();
                }
                else
                {
                    per = null;
                }
            }
            else
            {
                per = null;
            }
            return per;
        }
        public List<Personal> conseguirPersonalesList()
        {
            string sqlSentence;
            List<Personal> list = new List<Personal>();
            sqlSentence = "SELECT a.* FROM Personal a";
            if (DataAccess.Open())
            {
                DataTable dt = DataAccess.GetDataTable(sqlSentence);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    int ci = Convert.ToInt32(dr["Ci"]); 
                    string nombre = dr["nombre"].ToString();
                    string apellidoPat = dr["ApellidoPaterno"].ToString();
                    string apellidoMat = dr["ApellidoMaterno"].ToString();
                    DateTime fechaNac = Convert.ToDateTime(dr["FechaNacimiento"]);
                    DateTime fechaCont = Convert.ToDateTime(dr["FechaContrato"]); 
                    int sueldo = Convert.ToInt32(dr["Sueldo"]);
                    int numCel = Convert.ToInt32(dr["NumeroCelular"]);
                    string direccion = dr["Direccion"].ToString();
                    list.Add(new Personal(this, DataAccess ,ci, nombre, apellidoPat, apellidoMat, fechaNac,
                        fechaCont, sueldo, numCel, direccion));
                }
                DataAccess.Close();
            }
            return list;
        }
    }
}
