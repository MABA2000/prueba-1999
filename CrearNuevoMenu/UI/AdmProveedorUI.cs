﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrearNuevoMenu.Controller;

namespace CrearNuevoMenu.UI
{
    public partial class AdmProveedorUI : Form
    {
        private ProveedorControlador controlador;
        public AdmProveedorUI()
        {
            InitializeComponent();
        }

        private void AdmProveedorUI_Load(object sender, EventArgs e)
        {
            controlador = new ProveedorControlador();
            cambiarGridProveedor();

        }
 

        public ProveedorControlador Controlador
        {
            get
            {
                return controlador;
            }
        }

        private void cambiarGridProveedor()
        {
            ProvedorDG.AutoGenerateColumns = false;
            agregarColumnasProveedorGrid();
            verProveedors();
        }
        private void verProveedors()
        {
            var source = new BindingSource();
            var ProveedorList = controlador.ListaDeProveedor;
            source.DataSource = ProveedorList;
            ProvedorDG.DataSource = source;
        }
        private void agregarColumnasProveedorGrid()
        {

            DataGridViewColumn dataGridViewColumn;
            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "ci";
            dataGridViewColumn.HeaderText = "CI";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "ci";
            dataGridViewColumn.ReadOnly = true;
            ProvedorDG.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "nombre";
            dataGridViewColumn.HeaderText = "Nombre";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "nombre";
            ProvedorDG.Columns.Add(dataGridViewColumn);
            
            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "DirLocal";
            dataGridViewColumn.HeaderText = "Dirección";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "DirLocal";
            ProvedorDG.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "NumCel";
            dataGridViewColumn.HeaderText = "Celular";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "NumCel";
            ProvedorDG.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "Email";
            dataGridViewColumn.HeaderText = "Email";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "Email";
            //dataGridViewColumn.Visible = false;
            ProvedorDG.Columns.Add(dataGridViewColumn);


            var deleteButton = new DataGridViewButtonColumn();
            deleteButton.DataPropertyName = "borrar";
            deleteButton.Name = "Borrar";
            deleteButton.HeaderText = "borrar";
            deleteButton.Text = "Borrar";
            deleteButton.DefaultCellStyle.BackColor = Color.Red;
            deleteButton.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            deleteButton.CellTemplate = new DataGridViewButtonCell();
            deleteButton.UseColumnTextForButtonValue = true;
            ProvedorDG.Columns.Add(deleteButton);
        }


            private void borrarProveedor(DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;

            DialogResult dialogResult = MessageBox.Show("Esta seguro de borrar el Proveedor?", "Confirmar el dialogo.", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                string message;
                int recordsAffected = controlador.borrarProveedorDelIndice(index, out message);

                if (recordsAffected == 1)
                {
                    ProvedorDG.DataSource = null;
                    verProveedors();
                    MessageBox.Show("Proveedor correctamente borrado.");
                }
                else
                {
                    MessageBox.Show("Hubo problemas al borrar la columna:" + message, "Error");
                }
            }
        }
         
        
        private void añadirProveedor_Click(object sender, EventArgs e)
        {
            AñadirProveedor interfazNuevoProveedor = new AñadirProveedor();
            interfazNuevoProveedor.ShowDialog(this);
            ProvedorDG.DataSource = null;
            controlador.conseguirProveedors();
            verProveedors();
            interfazNuevoProveedor.Dispose();
        }

        private void guardarBtn_Click_1(object sender, EventArgs e)
        {
            string message;
            int recordsAffected = controlador.guardarProveedorDeMenuModificado(out message);
            if (message.Length > 0)
            {
                MessageBox.Show("Hubo errores al modificar:" + message);
            }
            else
            {
                MessageBox.Show(recordsAffected.ToString() + " se modificaron los datos");
            }
        }

        private void ProvedorDG_CellEndEdit_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == ProvedorDG.Columns["Email"].Index || e.ColumnIndex == ProvedorDG.Columns["DirLocal"].Index || e.ColumnIndex == ProvedorDG.Columns["nombre"].Index || e.ColumnIndex == ProvedorDG.Columns["NumCel"].Index)
            {
                int index = e.RowIndex;
                controlador.ListaDeProveedor[index].IsDirty = true;
            }
        }

        private void cancelarCambiosBtn_Click_1(object sender, EventArgs e)
        {
            ProvedorDG.DataSource = null;
            controlador.conseguirProveedors();
            verProveedors();
            MessageBox.Show("Los cambios fueron cancelados!");
        }

        private void volverBtn_Click_1(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void ProvedorDG_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == ProvedorDG.NewRowIndex || e.RowIndex < 0)
                return;

        
            if (e.ColumnIndex == ProvedorDG.Columns["borrar"].Index)
            {
                borrarProveedor(e);
            }
           
        }
    }
}

