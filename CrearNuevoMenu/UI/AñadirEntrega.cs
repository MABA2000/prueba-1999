﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrearNuevoMenu.Controller;
namespace CrearNuevoMenu.UI
{
    public partial class AñadirEntrega : Form
    {
        ProveedorControlador controladorProv;
        InsumoControlador controladorInsumo;
        EntregaControlador controladorEntrega;
        public AñadirEntrega()
        {
            controladorProv = new ProveedorControlador();
            controladorInsumo = new InsumoControlador();
            controladorEntrega = new EntregaControlador();
            InitializeComponent();
            configurarProveedores();
            configurarInsumos();
        }
        private void configurarProveedores()
        {
            proveedorCMB.DataSource = controladorProv.ListaNombreProv;
        }
        private void configurarInsumos()
        {
            insumoCMB.DataSource = controladorInsumo.ListaNombreInsum;
        }

        private void AñadirEntrega_Load(object sender, EventArgs e)
        {

        }

        private void añadirBtn_Click(object sender, EventArgs e)
        {
            float precio,cantidad;

            if (!float.TryParse(precioTxt.Text, out precio) || !float.TryParse(cantidadTxt.Text, out cantidad))
            {
                MessageBox.Show("El precio y cantidad no puedes ser un caracter o estar vacio!", "Error");

                return;
            }
            if (!controladorInsumo.comprobarQueNoHayPunto(precioTxt.Text) || !controladorInsumo.comprobarQueNoHayPunto(cantidadTxt.Text))
            {
                MessageBox.Show("Utilice ',' en vez de '.'!", "Error");
                return;
            }
            if (precio <= 0 || cantidad <= 0)
            {
                MessageBox.Show("Es obligatorio que se añada un precio y cantidad distintos a 0 o mayor .", "Error");
                return;
            }
            var insumo = insumoCMB.SelectedValue;
            var provedor = proveedorCMB.SelectedValue;
            
            var result = controladorEntrega.InsertarEntrega(-1, controladorInsumo.devolverIDDeNombre((string)insumo), controladorProv.devolverIDDeNombre((string)provedor), cantidad, precio);
            if (result > 0)
            {
                MessageBox.Show("La entrega fue añadida!", "Mensaje");
                this.Close();
                this.Dispose();

            }
            else
            {
                MessageBox.Show("Hubo un problema guardando los datos.", "Error");
            }
        }

        private void proveedorCMB_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void precioTxt_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void cantidadTxt_TextChanged(object sender, EventArgs e)
        {

        }

        private void insumoCMB_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
    }
}
