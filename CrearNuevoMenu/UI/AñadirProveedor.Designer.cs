﻿namespace CrearNuevoMenu.UI
{
    partial class AñadirProveedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AñadirProveedor));
            this.label3 = new System.Windows.Forms.Label();
            this.nombreTxt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.CITxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.emailTxT = new System.Windows.Forms.TextBox();
            this.dirLocar = new System.Windows.Forms.Label();
            this.direccionTxt = new System.Windows.Forms.TextBox();
            this.añadirBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.CELULARtXT = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Green;
            this.label3.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(60, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 21);
            this.label3.TabIndex = 53;
            this.label3.Text = "NOMBRE";
            // 
            // nombreTxt
            // 
            this.nombreTxt.Location = new System.Drawing.Point(156, 62);
            this.nombreTxt.Name = "nombreTxt";
            this.nombreTxt.Size = new System.Drawing.Size(100, 20);
            this.nombreTxt.TabIndex = 52;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Green;
            this.label4.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(114, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 21);
            this.label4.TabIndex = 51;
            this.label4.Text = "CI";
            // 
            // CITxt
            // 
            this.CITxt.Location = new System.Drawing.Point(156, 23);
            this.CITxt.Name = "CITxt";
            this.CITxt.Size = new System.Drawing.Size(100, 20);
            this.CITxt.TabIndex = 50;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Green;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(80, 166);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 21);
            this.label1.TabIndex = 49;
            this.label1.Text = "EMAIL";
            // 
            // emailTxT
            // 
            this.emailTxT.Location = new System.Drawing.Point(156, 168);
            this.emailTxT.Name = "emailTxT";
            this.emailTxT.Size = new System.Drawing.Size(100, 20);
            this.emailTxT.TabIndex = 48;
            // 
            // dirLocar
            // 
            this.dirLocar.AutoSize = true;
            this.dirLocar.BackColor = System.Drawing.Color.Green;
            this.dirLocar.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dirLocar.Location = new System.Drawing.Point(39, 135);
            this.dirLocar.Name = "dirLocar";
            this.dirLocar.Size = new System.Drawing.Size(102, 21);
            this.dirLocar.TabIndex = 47;
            this.dirLocar.Text = "DIRECCION";
            // 
            // direccionTxt
            // 
            this.direccionTxt.Location = new System.Drawing.Point(156, 135);
            this.direccionTxt.Name = "direccionTxt";
            this.direccionTxt.Size = new System.Drawing.Size(100, 20);
            this.direccionTxt.TabIndex = 46;
            // 
            // añadirBtn
            // 
            this.añadirBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.añadirBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.añadirBtn.Location = new System.Drawing.Point(25, 210);
            this.añadirBtn.Name = "añadirBtn";
            this.añadirBtn.Size = new System.Drawing.Size(83, 34);
            this.añadirBtn.TabIndex = 45;
            this.añadirBtn.Text = "AÑADIR ";
            this.añadirBtn.UseVisualStyleBackColor = false;
            this.añadirBtn.Click += new System.EventHandler(this.añadirBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Green;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(-1, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(156, 21);
            this.label2.TabIndex = 55;
            this.label2.Text = "NUM. DE CELULAR";
            // 
            // CELULARtXT
            // 
            this.CELULARtXT.Location = new System.Drawing.Point(156, 99);
            this.CELULARtXT.Name = "CELULARtXT";
            this.CELULARtXT.Size = new System.Drawing.Size(100, 20);
            this.CELULARtXT.TabIndex = 54;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(164, 210);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(92, 34);
            this.button1.TabIndex = 56;
            this.button1.Text = "CANCELAR";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // AñadirProveedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.ClientSize = new System.Drawing.Size(284, 277);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.CELULARtXT);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nombreTxt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CITxt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.emailTxT);
            this.Controls.Add(this.dirLocar);
            this.Controls.Add(this.direccionTxt);
            this.Controls.Add(this.añadirBtn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AñadirProveedor";
            this.Text = "AñadirProveedor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox nombreTxt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox CITxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox emailTxT;
        private System.Windows.Forms.Label dirLocar;
        private System.Windows.Forms.TextBox direccionTxt;
        private System.Windows.Forms.Button añadirBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox CELULARtXT;
        private System.Windows.Forms.Button button1;
    }
}