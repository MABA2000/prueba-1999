﻿namespace CrearNuevoMenu.UI
{
    partial class AdmProveedorUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdmProveedorUI));
            this.label1 = new System.Windows.Forms.Label();
            this.añadirProveedor = new System.Windows.Forms.Button();
            this.cancelarCambiosBtn = new System.Windows.Forms.Button();
            this.guardarBtn = new System.Windows.Forms.Button();
            this.volverBtn = new System.Windows.Forms.Button();
            this.ProvedorDG = new System.Windows.Forms.DataGridView();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.ProvedorDG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Gold;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(193, 201);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(578, 31);
            this.label1.TabIndex = 37;
            this.label1.Text = "PROVEEDORES";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // añadirProveedor
            // 
            this.añadirProveedor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.añadirProveedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.añadirProveedor.Location = new System.Drawing.Point(605, 438);
            this.añadirProveedor.Name = "añadirProveedor";
            this.añadirProveedor.Size = new System.Drawing.Size(177, 34);
            this.añadirProveedor.TabIndex = 36;
            this.añadirProveedor.Text = "AÑADIR NUEVO PROVEEDOR";
            this.añadirProveedor.UseVisualStyleBackColor = false;
            this.añadirProveedor.Click += new System.EventHandler(this.añadirProveedor_Click);
            // 
            // cancelarCambiosBtn
            // 
            this.cancelarCambiosBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.cancelarCambiosBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarCambiosBtn.Location = new System.Drawing.Point(441, 438);
            this.cancelarCambiosBtn.Name = "cancelarCambiosBtn";
            this.cancelarCambiosBtn.Size = new System.Drawing.Size(144, 34);
            this.cancelarCambiosBtn.TabIndex = 35;
            this.cancelarCambiosBtn.Text = "CANCELAR CAMBIOS";
            this.cancelarCambiosBtn.UseVisualStyleBackColor = false;
            this.cancelarCambiosBtn.Click += new System.EventHandler(this.cancelarCambiosBtn_Click_1);
            // 
            // guardarBtn
            // 
            this.guardarBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.guardarBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.guardarBtn.Location = new System.Drawing.Point(325, 438);
            this.guardarBtn.Name = "guardarBtn";
            this.guardarBtn.Size = new System.Drawing.Size(84, 34);
            this.guardarBtn.TabIndex = 34;
            this.guardarBtn.Text = "GUARDAR";
            this.guardarBtn.UseVisualStyleBackColor = false;
            this.guardarBtn.Click += new System.EventHandler(this.guardarBtn_Click_1);
            // 
            // volverBtn
            // 
            this.volverBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.volverBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.volverBtn.Location = new System.Drawing.Point(190, 438);
            this.volverBtn.Name = "volverBtn";
            this.volverBtn.Size = new System.Drawing.Size(84, 34);
            this.volverBtn.TabIndex = 33;
            this.volverBtn.Text = "VOLVER";
            this.volverBtn.UseVisualStyleBackColor = false;
            this.volverBtn.Click += new System.EventHandler(this.volverBtn_Click_1);
            // 
            // ProvedorDG
            // 
            this.ProvedorDG.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ProvedorDG.Location = new System.Drawing.Point(190, 232);
            this.ProvedorDG.Name = "ProvedorDG";
            this.ProvedorDG.Size = new System.Drawing.Size(584, 200);
            this.ProvedorDG.TabIndex = 32;
            this.ProvedorDG.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ProvedorDG_CellContentClick);
            this.ProvedorDG.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.ProvedorDG_CellEndEdit_1);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(358, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(253, 168);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 38;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.UseWaitCursor = true;
            // 
            // AdmProveedorUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.ClientSize = new System.Drawing.Size(938, 661);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.añadirProveedor);
            this.Controls.Add(this.cancelarCambiosBtn);
            this.Controls.Add(this.guardarBtn);
            this.Controls.Add(this.volverBtn);
            this.Controls.Add(this.ProvedorDG);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AdmProveedorUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AdmProveedorUI";
            this.Load += new System.EventHandler(this.AdmProveedorUI_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ProvedorDG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button añadirProveedor;
        private System.Windows.Forms.Button cancelarCambiosBtn;
        private System.Windows.Forms.Button guardarBtn;
        private System.Windows.Forms.Button volverBtn;
        private System.Windows.Forms.DataGridView ProvedorDG;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}