﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrearNuevoMenu.Controller;

namespace CrearNuevoMenu.UI
{
    public partial class AñadirProveedor : Form
    {
        ProveedorControlador controladorProv;
        public AñadirProveedor()
        {
            controladorProv = new ProveedorControlador();
            InitializeComponent();
        }
       
        private void AñadirProveedor_Load(object sender, EventArgs e)
        {
           
        }

        private void añadirBtn_Click(object sender, EventArgs e)
        {
            int ci, numeroCel;

            if (!int.TryParse(CITxt.Text, out ci) || !int.TryParse(CELULARtXT.Text, out numeroCel))
            {
                MessageBox.Show("El CI y Celular no pueden ser un caracter o estar vacio!", "Error");

                return;
            }
      
            if (ci <= 0 || numeroCel <= 0)
            {
                MessageBox.Show("Es obligatorio que se añada un CI y nu. de celular distintos a 0 o mayor .", "Error");
                return;
            }
         

            var result = controladorProv.InsertarProveedor(ci, nombreTxt.Text, emailTxT.Text,direccionTxt.Text, numeroCel);
            if (result > 0)
            {
                MessageBox.Show("La Proveedor fue añadido!", "Mensaje");
                this.Close();
                this.Dispose();

            }
            else
            {
                MessageBox.Show("Hubo un problema guardando los datos.", "Error");
            }
        }

        private void proveedorCMB_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void precioTxt_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void cantidadTxt_TextChanged(object sender, EventArgs e)
        {

        }

        private void insumoCMB_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();

        }
    }
}
