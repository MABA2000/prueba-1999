﻿namespace CrearNuevoMenu.UI
{
    partial class AdmInsumoUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdmInsumoUI));
            this.InsumosDG = new System.Windows.Forms.DataGridView();
            this.volverBtn = new System.Windows.Forms.Button();
            this.guardarBtn = new System.Windows.Forms.Button();
            this.cancelarCambiosBtn = new System.Windows.Forms.Button();
            this.añadirInsumo = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.InsumosDG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // InsumosDG
            // 
            this.InsumosDG.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.InsumosDG.Location = new System.Drawing.Point(177, 251);
            this.InsumosDG.Name = "InsumosDG";
            this.InsumosDG.Size = new System.Drawing.Size(584, 200);
            this.InsumosDG.TabIndex = 0;
            this.InsumosDG.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.InsumosDG_CellClick_1);
            this.InsumosDG.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.InsumosDG_CellEndEdit);
            // 
            // volverBtn
            // 
            this.volverBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.volverBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.volverBtn.Location = new System.Drawing.Point(177, 457);
            this.volverBtn.Name = "volverBtn";
            this.volverBtn.Size = new System.Drawing.Size(84, 34);
            this.volverBtn.TabIndex = 1;
            this.volverBtn.Text = "VOLVER";
            this.volverBtn.UseVisualStyleBackColor = false;
            this.volverBtn.Click += new System.EventHandler(this.volverBtn_Click);
            // 
            // guardarBtn
            // 
            this.guardarBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.guardarBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.guardarBtn.Location = new System.Drawing.Point(312, 457);
            this.guardarBtn.Name = "guardarBtn";
            this.guardarBtn.Size = new System.Drawing.Size(84, 34);
            this.guardarBtn.TabIndex = 2;
            this.guardarBtn.Text = "GUARDAR";
            this.guardarBtn.UseVisualStyleBackColor = false;
            this.guardarBtn.Click += new System.EventHandler(this.guardarBtn_Click);
            // 
            // cancelarCambiosBtn
            // 
            this.cancelarCambiosBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.cancelarCambiosBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarCambiosBtn.Location = new System.Drawing.Point(428, 457);
            this.cancelarCambiosBtn.Name = "cancelarCambiosBtn";
            this.cancelarCambiosBtn.Size = new System.Drawing.Size(144, 34);
            this.cancelarCambiosBtn.TabIndex = 3;
            this.cancelarCambiosBtn.Text = "CANCELAR CAMBIOS";
            this.cancelarCambiosBtn.UseVisualStyleBackColor = false;
            this.cancelarCambiosBtn.Click += new System.EventHandler(this.cancelarCambiosBtn_Click);
            // 
            // añadirInsumo
            // 
            this.añadirInsumo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.añadirInsumo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.añadirInsumo.Location = new System.Drawing.Point(592, 457);
            this.añadirInsumo.Name = "añadirInsumo";
            this.añadirInsumo.Size = new System.Drawing.Size(158, 34);
            this.añadirInsumo.TabIndex = 4;
            this.añadirInsumo.Text = "AÑADIR INSUMO NUEVO";
            this.añadirInsumo.UseVisualStyleBackColor = false;
            this.añadirInsumo.Click += new System.EventHandler(this.añadirInsumo_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Gold;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(180, 220);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(578, 31);
            this.label1.TabIndex = 17;
            this.label1.Text = "INSUMOS DEL SISTEMA";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(341, 26);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(253, 168);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 27;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.UseWaitCursor = true;
            // 
            // AdmInsumoUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.ClientSize = new System.Drawing.Size(938, 661);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.añadirInsumo);
            this.Controls.Add(this.cancelarCambiosBtn);
            this.Controls.Add(this.guardarBtn);
            this.Controls.Add(this.volverBtn);
            this.Controls.Add(this.InsumosDG);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AdmInsumoUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Administrar Insumo";
            this.Load += new System.EventHandler(this.AdmInsumoUI_Load);
            ((System.ComponentModel.ISupportInitialize)(this.InsumosDG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView InsumosDG;
        private System.Windows.Forms.Button volverBtn;
        private System.Windows.Forms.Button guardarBtn;
        private System.Windows.Forms.Button cancelarCambiosBtn;
        private System.Windows.Forms.Button añadirInsumo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}