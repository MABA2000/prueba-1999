﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrearNuevoMenu.Controller;


using System.Threading.Tasks;

using CrearNuevoMenu.UIIngresar;
using CrearNuevoMenu.Model;
using CrearNuevoMenu.CrearMenuForm;

using CrearNuevoMenu.UI;
namespace CrearNuevoMenu.UIPrincipal
{
    public partial class Principal : Form
    {
        private PersonalControlador controlador;
        public Principal()
        {
            controlador = new PersonalControlador();
            InitializeComponent();
            lblUsuario.Text = GlobalData.GlobalData.usuarioPrincipal.nombre;
            lblCi.Text= GlobalData.GlobalData.usuarioPrincipal.ci.ToString();
            lblFecha.Text = DateTime.Today.ToShortDateString();
            
        }

        private void BCrear_Click(object sender, EventArgs e)
        {
            CrearMenu interfazCrearMenu = new CrearMenu();
            this.Hide();
            interfazCrearMenu.ShowDialog(this);
            this.Show();
            interfazCrearMenu.Close();
            interfazCrearMenu.Dispose();
        }

        private void bSalir_Click(object sender, EventArgs e)
        {
            Ingresar interfazIngresar = new Ingresar();
            this.Hide();
            interfazIngresar.ShowDialog(this);
            this.Close();
            this.Dispose();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            
            AdmInsumoUI interfazIngresar = new AdmInsumoUI();
            this.Hide();
            interfazIngresar.ShowDialog();
            this.Show();
            interfazIngresar.Close();
            interfazIngresar.Dispose();
        }

     

        private void button6_Click(object sender, EventArgs e)
        {
            AdmEntregasUI interfazIngresar = new AdmEntregasUI();
            this.Hide();
            interfazIngresar.ShowDialog();
            this.Show();
            interfazIngresar.Close();
            interfazIngresar.Dispose();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AdmProveedorUI interfazIngresar = new AdmProveedorUI();
            this.Hide();
            interfazIngresar.ShowDialog();
            this.Show();
            interfazIngresar.Close();
            interfazIngresar.Dispose();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            AdministrarFacturas interfazFacturas = new AdministrarFacturas();
            this.Hide();
            interfazFacturas.ShowDialog();
            this.Show();
            interfazFacturas.Close();
            interfazFacturas.Dispose();
        }

        private void btn_items_Click(object sender, EventArgs e)
        {

            ItemUI interfazFacturas = new ItemUI();
            this.Hide();
            interfazFacturas.ShowDialog();
            this.Show();
            interfazFacturas.Close();
            interfazFacturas.Dispose();
        }
    }
}
