﻿namespace CrearNuevoMenu.UI
{
    partial class AdministrarFacturas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdministrarFacturas));
            this.BVolver = new System.Windows.Forms.Button();
            this.DGVFacturas = new System.Windows.Forms.DataGridView();
            this.BCrear = new System.Windows.Forms.Button();
            this.BCancelarCambios = new System.Windows.Forms.Button();
            this.BGuardar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.DGVFacturas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // BVolver
            // 
            this.BVolver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.BVolver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BVolver.Font = new System.Drawing.Font("Palatino Linotype", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BVolver.Location = new System.Drawing.Point(142, 454);
            this.BVolver.Name = "BVolver";
            this.BVolver.Size = new System.Drawing.Size(122, 43);
            this.BVolver.TabIndex = 1;
            this.BVolver.Text = "VOLVER";
            this.BVolver.UseVisualStyleBackColor = false;
            this.BVolver.Click += new System.EventHandler(this.BVolver_Click);
            // 
            // DGVFacturas
            // 
            this.DGVFacturas.BackgroundColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVFacturas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DGVFacturas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGreen;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVFacturas.DefaultCellStyle = dataGridViewCellStyle4;
            this.DGVFacturas.Location = new System.Drawing.Point(142, 203);
            this.DGVFacturas.Name = "DGVFacturas";
            this.DGVFacturas.Size = new System.Drawing.Size(690, 218);
            this.DGVFacturas.TabIndex = 3;
            this.DGVFacturas.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVFacturas_CellClick);
            this.DGVFacturas.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVFacturas_CellContentClick_1);
            this.DGVFacturas.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVFacturas_CellEndEdit);
            this.DGVFacturas.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DGVFacturas_KeyPress);
            // 
            // BCrear
            // 
            this.BCrear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.BCrear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BCrear.Font = new System.Drawing.Font("Palatino Linotype", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BCrear.Location = new System.Drawing.Point(710, 454);
            this.BCrear.Name = "BCrear";
            this.BCrear.Size = new System.Drawing.Size(122, 43);
            this.BCrear.TabIndex = 4;
            this.BCrear.Text = "NUEVO";
            this.BCrear.UseVisualStyleBackColor = false;
            this.BCrear.Click += new System.EventHandler(this.BCrear_Click);
            // 
            // BCancelarCambios
            // 
            this.BCancelarCambios.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.BCancelarCambios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BCancelarCambios.Font = new System.Drawing.Font("Palatino Linotype", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BCancelarCambios.Location = new System.Drawing.Point(453, 454);
            this.BCancelarCambios.Name = "BCancelarCambios";
            this.BCancelarCambios.Size = new System.Drawing.Size(236, 43);
            this.BCancelarCambios.TabIndex = 5;
            this.BCancelarCambios.Text = "CANCELAR CAMBIOS";
            this.BCancelarCambios.UseVisualStyleBackColor = false;
            this.BCancelarCambios.Click += new System.EventHandler(this.BCancelarCambios_Click);
            // 
            // BGuardar
            // 
            this.BGuardar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.BGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BGuardar.Font = new System.Drawing.Font("Palatino Linotype", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BGuardar.Location = new System.Drawing.Point(291, 454);
            this.BGuardar.Name = "BGuardar";
            this.BGuardar.Size = new System.Drawing.Size(146, 43);
            this.BGuardar.TabIndex = 6;
            this.BGuardar.Text = "GUARDAR";
            this.BGuardar.UseVisualStyleBackColor = false;
            this.BGuardar.Click += new System.EventHandler(this.BGuardar_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Gold;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(141, 172);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(691, 31);
            this.label1.TabIndex = 17;
            this.label1.Text = "FACTURAS";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(365, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(253, 168);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 27;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.UseWaitCursor = true;
            // 
            // AdministrarFacturas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.ClientSize = new System.Drawing.Size(938, 661);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BGuardar);
            this.Controls.Add(this.BCancelarCambios);
            this.Controls.Add(this.BCrear);
            this.Controls.Add(this.DGVFacturas);
            this.Controls.Add(this.BVolver);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AdministrarFacturas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AdministrarFacturas";
            this.Load += new System.EventHandler(this.AdministrarFacturas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGVFacturas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button BVolver;
        private System.Windows.Forms.DataGridView DGVFacturas;
        private System.Windows.Forms.Button BCrear;
        private System.Windows.Forms.Button BCancelarCambios;
        private System.Windows.Forms.Button BGuardar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}