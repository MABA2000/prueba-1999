﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrearNuevoMenu.Controller;

namespace CrearNuevoMenu.UI
{

    public partial class AgregarPrecioAItemDeMenu : Form
    {
        AgrItemDeMenuController agrItemdeMenuController; 
        int codItem;
        int idMenu;
        string nombre;
        string descripcion;
        string tipo;
        public AgregarPrecioAItemDeMenu(int codItem, int idMenu, string descripcion, string tipo, string nombre)
        {
            this.descripcion = descripcion;
            this.tipo = tipo;
            this.codItem = codItem;
            this.idMenu = idMenu;
            this.nombre = nombre;
            InitializeComponent();
            agrItemdeMenuController = new AgrItemDeMenuController();
        }

 

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void guardarBtn_Click(object sender, EventArgs e)
        {
            float precio;
           
            if (!float.TryParse(precioTxt.Text, out precio))
            {
                MessageBox.Show("El precio no puede ser un caracter o estar vacio!", "Error");

                return;
            }
            if (!agrItemdeMenuController.comprobarQueNoHayPunto(precioTxt.Text))
            {
                MessageBox.Show("Utilice ',' en vez de '.'!", "Error");
                return;
            }
            if (precio <= 0)
            {
                MessageBox.Show("Es obligatorio que se añada un precio distinto a 0 o mayor .", "Error");
                return;
            }

            var result = agrItemdeMenuController.InsertarItemDeMenu(codItem, idMenu, precio, DateTime.Today);
            if(result>0)
            {
                MessageBox.Show("El ítem fue añadido al menú!","Mensaje");
                this.Close();
                this.Dispose();

            }
            else
            {
                MessageBox.Show("Hubo un problema guardando los datos.", "Error");
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void AgregarPrecioAItemDeMenu_Load(object sender, EventArgs e)
        {

        }
    }
}
