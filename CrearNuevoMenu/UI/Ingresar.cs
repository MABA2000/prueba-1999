﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrearNuevoMenu.Controller;
using CrearNuevoMenu.UIPrincipal;
using CrearNuevoMenu.Model;
using CrearNuevoMenu.UI;
namespace CrearNuevoMenu.UIIngresar
{
    public partial class Ingresar : Form
    {
        private CredencialControlador controlador;
        private PersonalControlador controladorP;
        public Ingresar()
        {
            controlador = new CredencialControlador();
            controladorP = new PersonalControlador();
            InitializeComponent();
        }

        private void bSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Ingresar_Load(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
        public void limpiarTextos ()
        {
            textEmail.Text = "";
            textContraseña.Text = "";
        }
        private void bEntrar_Click(object sender, EventArgs e)
        {
            bool res;
            string rol;
            int ci;
            string ema = textEmail.Text;
            string cont = textContraseña.Text;
            if (string.IsNullOrEmpty(ema) || string.IsNullOrEmpty(cont))
            {
               
                MessageBox.Show("Debe llenar los campos.");
                return;
            }
            res = controlador.verificarCredencial(ema, cont, out rol, out ci);
            if (res)//aqui viene true
            {
                GlobalData.GlobalData.usuarioPrincipal = controladorP.conseguirUnPersonal(ci);
                if (GlobalData.GlobalData.usuarioPrincipal == null)
                    return;
                if (rol == "administrador")
                {
                    Principal interfazPrincipal = new Principal();
                        
                    this.Hide();
                    interfazPrincipal.ShowDialog(this);
                    this.Close();
                    this.Dispose();
                }
            }
            else
            {
                MessageBox.Show("No se hallo la cuenta o la contraseña.");
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
