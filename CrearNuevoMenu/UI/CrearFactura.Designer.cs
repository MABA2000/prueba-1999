﻿namespace CrearNuevoMenu.UI
{
    partial class CrearFactura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CrearFactura));
            this.DGVOrdenes = new System.Windows.Forms.DataGridView();
            this.BVolver = new System.Windows.Forms.Button();
            this.BCrear = new System.Windows.Forms.Button();
            this.CITextBox = new System.Windows.Forms.TextBox();
            this.lblCICliente = new System.Windows.Forms.Label();
            this.lblCodOrden = new System.Windows.Forms.Label();
            this.lblCod = new System.Windows.Forms.Label();
            this.NroMtextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BBuscarMesa = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DGVOrdenes)).BeginInit();
            this.SuspendLayout();
            // 
            // DGVOrdenes
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVOrdenes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGVOrdenes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightYellow;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVOrdenes.DefaultCellStyle = dataGridViewCellStyle2;
            this.DGVOrdenes.Location = new System.Drawing.Point(12, 85);
            this.DGVOrdenes.Name = "DGVOrdenes";
            this.DGVOrdenes.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVOrdenes.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DGVOrdenes.Size = new System.Drawing.Size(454, 322);
            this.DGVOrdenes.TabIndex = 0;
            this.DGVOrdenes.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVOrdenes_CellClick);
            this.DGVOrdenes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVOrdenes_CellContentClick);
            // 
            // BVolver
            // 
            this.BVolver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.BVolver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BVolver.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BVolver.Location = new System.Drawing.Point(495, 327);
            this.BVolver.Name = "BVolver";
            this.BVolver.Size = new System.Drawing.Size(109, 49);
            this.BVolver.TabIndex = 1;
            this.BVolver.Text = "VOLVER";
            this.BVolver.UseVisualStyleBackColor = false;
            this.BVolver.Click += new System.EventHandler(this.BVolver_Click);
            // 
            // BCrear
            // 
            this.BCrear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.BCrear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BCrear.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BCrear.Location = new System.Drawing.Point(495, 224);
            this.BCrear.Name = "BCrear";
            this.BCrear.Size = new System.Drawing.Size(109, 49);
            this.BCrear.TabIndex = 2;
            this.BCrear.Text = "CREAR";
            this.BCrear.UseVisualStyleBackColor = false;
            this.BCrear.Click += new System.EventHandler(this.BCrear_Click);
            // 
            // CITextBox
            // 
            this.CITextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CITextBox.Location = new System.Drawing.Point(476, 50);
            this.CITextBox.Name = "CITextBox";
            this.CITextBox.Size = new System.Drawing.Size(149, 26);
            this.CITextBox.TabIndex = 3;
            this.CITextBox.TextChanged += new System.EventHandler(this.CITextBox_TextChanged);
            this.CITextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CITextBox_KeyPress);
            // 
            // lblCICliente
            // 
            this.lblCICliente.AutoSize = true;
            this.lblCICliente.BackColor = System.Drawing.Color.ForestGreen;
            this.lblCICliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCICliente.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lblCICliente.Location = new System.Drawing.Point(472, 13);
            this.lblCICliente.Name = "lblCICliente";
            this.lblCICliente.Size = new System.Drawing.Size(90, 24);
            this.lblCICliente.TabIndex = 4;
            this.lblCICliente.Text = "CI Cliente";
            // 
            // lblCodOrden
            // 
            this.lblCodOrden.AutoSize = true;
            this.lblCodOrden.BackColor = System.Drawing.Color.ForestGreen;
            this.lblCodOrden.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodOrden.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lblCodOrden.Location = new System.Drawing.Point(472, 92);
            this.lblCodOrden.Name = "lblCodOrden";
            this.lblCodOrden.Size = new System.Drawing.Size(159, 24);
            this.lblCodOrden.TabIndex = 5;
            this.lblCodOrden.Text = "Codigo De Orden";
            this.lblCodOrden.Click += new System.EventHandler(this.label2_Click);
            // 
            // lblCod
            // 
            this.lblCod.AutoSize = true;
            this.lblCod.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCod.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblCod.Location = new System.Drawing.Point(472, 135);
            this.lblCod.Name = "lblCod";
            this.lblCod.Size = new System.Drawing.Size(75, 31);
            this.lblCod.TabIndex = 6;
            this.lblCod.Text = "COD";
            // 
            // NroMtextBox
            // 
            this.NroMtextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NroMtextBox.Location = new System.Drawing.Point(21, 50);
            this.NroMtextBox.Name = "NroMtextBox";
            this.NroMtextBox.Size = new System.Drawing.Size(82, 26);
            this.NroMtextBox.TabIndex = 7;
            this.NroMtextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NroMtextBox_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.ForestGreen;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label1.Location = new System.Drawing.Point(17, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 24);
            this.label1.TabIndex = 8;
            this.label1.Text = "Mesa";
            // 
            // BBuscarMesa
            // 
            this.BBuscarMesa.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.BBuscarMesa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BBuscarMesa.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BBuscarMesa.Location = new System.Drawing.Point(109, 41);
            this.BBuscarMesa.Name = "BBuscarMesa";
            this.BBuscarMesa.Size = new System.Drawing.Size(89, 35);
            this.BBuscarMesa.TabIndex = 9;
            this.BBuscarMesa.Text = "BUSCAR";
            this.BBuscarMesa.UseVisualStyleBackColor = false;
            this.BBuscarMesa.Click += new System.EventHandler(this.BBuscarMesa_Click);
            // 
            // CrearFactura
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.ForestGreen;
            this.ClientSize = new System.Drawing.Size(637, 419);
            this.Controls.Add(this.BBuscarMesa);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.NroMtextBox);
            this.Controls.Add(this.lblCod);
            this.Controls.Add(this.lblCodOrden);
            this.Controls.Add(this.lblCICliente);
            this.Controls.Add(this.CITextBox);
            this.Controls.Add(this.BCrear);
            this.Controls.Add(this.BVolver);
            this.Controls.Add(this.DGVOrdenes);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CrearFactura";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CrearFactura";
            this.Load += new System.EventHandler(this.CrearFactura_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGVOrdenes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DGVOrdenes;
        private System.Windows.Forms.Button BVolver;
        private System.Windows.Forms.Button BCrear;
        private System.Windows.Forms.TextBox CITextBox;
        private System.Windows.Forms.Label lblCICliente;
        private System.Windows.Forms.Label lblCodOrden;
        private System.Windows.Forms.Label lblCod;
        private System.Windows.Forms.TextBox NroMtextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BBuscarMesa;
    }
}