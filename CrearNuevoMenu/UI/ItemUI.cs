﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrearNuevoMenu.Controller;

namespace CrearNuevoMenu.UI
{
    public partial class ItemUI : Form
    {
        private ItemController _controller;
        public ItemUI()
        {
            InitializeComponent();
            _controller = new ItemController();
        }
        public ItemController Controller
        {
            get
            {
                return _controller;
            }
        }

        private void ItemUI_Load(object sender, EventArgs e)
        {
            SetItemGrid();
        }


        private void SetItemGrid()
        {
            dgItem.AutoGenerateColumns = false;


           
            AddColumnsToItemGrid();

            RebindItemGrid();


        }
        private void RebindItemGrid()
        {
            var source = new BindingSource();
            var itemsList = Controller.Items;
         //   Controller.ItemsView();
            source.DataSource = itemsList;
            dgItem.DataSource = source;
        }
        private void AddColumnsToItemGrid()
        {
            DataGridViewColumn dataGridViewColumn;

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "codItem";
            dataGridViewColumn.HeaderText = "CodItem";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "CodItem";
            dgItem.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "nombre";
            dataGridViewColumn.HeaderText = "Nombre";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "Nombre";
            dgItem.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "tiempoElaboracion";
            dataGridViewColumn.HeaderText = "Tiempo de elaboracion";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "TiempoElaboracion";
            dgItem.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "GradoAlc";
            dataGridViewColumn.HeaderText = "Grado Alcohólico";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "GradoAlc";
            dataGridViewColumn.Visible = false;//grado alcoholico
            dgItem.Columns.Add(dataGridViewColumn);



            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "Calorias";
            dataGridViewColumn.HeaderText = "Calorias";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "Calorias";
            dgItem.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "precioDeElaboracion";
            dataGridViewColumn.HeaderText = "Precio de Elaboracion";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "PrecioElaboracion";
            dgItem.Columns.Add(dataGridViewColumn);



            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "descripcion";
            dataGridViewColumn.HeaderText = "Descripcion";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "Descripcion";
            dgItem.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "tipo";
            dataGridViewColumn.HeaderText = "Tipo";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "Tipo";
            dgItem.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "ci";
            dataGridViewColumn.HeaderText = "CI";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "CIAdministrador";
            dataGridViewColumn.Visible = false;
            dgItem.Columns.Add(dataGridViewColumn);
            

            var CliclButton = new DataGridViewButtonColumn();
            CliclButton.DataPropertyName = "Insumos";
            CliclButton.Name = "dgInsumosBtn";
            CliclButton.HeaderText = "Insumos";
            CliclButton.Text = "Insumos";
            CliclButton.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            CliclButton.CellTemplate = new DataGridViewButtonCell();
            CliclButton.UseColumnTextForButtonValue = true;
            dgItem.Columns.Add(CliclButton);

            var deleteButton = new DataGridViewButtonColumn();
            deleteButton.DataPropertyName = "Delete";
            deleteButton.Name = "dgItemDeleteBtn";
            deleteButton.HeaderText = "Delete";
            deleteButton.Text = "Delete";
            deleteButton.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            deleteButton.CellTemplate = new DataGridViewButtonCell();
            deleteButton.UseColumnTextForButtonValue = true;
            dgItem.Columns.Add(deleteButton);

            

        }
        private void dgItem_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //Check if click is on specific column 
            if (e.ColumnIndex == dgItem.Columns["Description"].Index)
            {
                int index = e.RowIndex;
                Controller.Items[index].IsDirty = true;
            }
        }
        
       
        private void dgItem_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //si el clic está en una nueva fila o fila de encabezado
            if (e.RowIndex == dgItem.NewRowIndex || e.RowIndex < 0)
                return;

            //Compruebe si el clic está en una columna específica
            if (e.ColumnIndex == dgItem.Columns["dgItemDeleteBtn"].Index)
            {
                //Ponga algo de lógica aquí, por ejemplo, para eliminar la fila de su lista de enlaces.
                int index = e.RowIndex;

                DialogResult dialogResult = MessageBox.Show("Esta seguro de eliminar  " + Controller.Items[index].nombre + " ?", "Confirmacion", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    string message;
                    int recordsAffected = Controller.DeleteItemFromIndex(index, out message);

                    if (recordsAffected == 1)
                    {
                        dgItem.DataSource = null;
                        RebindItemGrid();
                        MessageBox.Show("Se elimino exitosamente");
                    }
                    else
                    {
                        MessageBox.Show("Se produjo un error al eliminar el registro.:" + message, "Error");
                    }
                }
            }
        }
        private void dgItem_ClickInsumos(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == dgItem.NewRowIndex || e.RowIndex < 0)
                return;

            //Compruebe si el clic está en una columna específica
            if (e.ColumnIndex == dgItem.Columns["dgInsumosBtn"].Index)
            {
                int index = e.RowIndex;
                string code = dgItem.Rows[index].Cells[0].Value.ToString(); //codItem
                string name = dgItem.Rows[index].Cells[1].Value.ToString();//name
                InsumoUI insumos=new InsumoUI(code,name); 
                insumos.ShowDialog(this);
                //aa
                dgItem.DataSource = null;
                Controller.GetItems();
                RebindItemGrid();
                insumos.Dispose();
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            ItemAddUI frm = new ItemAddUI();
            frm.ShowDialog(this);
            dgItem.DataSource = null;
            Controller.GetItems();
            RebindItemGrid();
            frm.Dispose();
        }

        private void btn_save_Click_1(object sender, EventArgs e)
        {
            string message;
            int recordsAffected = Controller.SaveModifiedItems(out message);
            if (message.Length > 0)
            {
                MessageBox.Show("Hubo errores durante la actualización:" + message);
            }
            else
            {
                MessageBox.Show("registros fueron afectados");
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
