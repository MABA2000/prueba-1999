﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrearNuevoMenu.Controller;

namespace CrearNuevoMenu.UI
{
    public partial class AdmEntregasUI : Form
    {
        private EntregaControlador controlador;
        public AdmEntregasUI()
        {
            controlador = new EntregaControlador();
            InitializeComponent();
        }
        private void AdmEntregasUI_Load(object sender, EventArgs e)
        {
            cambiarGridEntrega();

        }
        private void añadirEntrega_Click(object sender, EventArgs e)
        {
            AñadirEntrega interfazIngresar = new AñadirEntrega();
            this.Hide();
            interfazIngresar.ShowDialog();
            this.Show();
            interfazIngresar.Close();
            interfazIngresar.Dispose();
            verEntregas();
        }

        public EntregaControlador Controlador
        {
            get
            {
                return controlador;
            }
        }
       
        private void cambiarGridEntrega()
        {
            EntregasDG.AutoGenerateColumns = false;
            agregarColumnasEntregaGrid();
            verEntregas();
        }
        private void verEntregas()
        {
            var source = new BindingSource();
            EntregasDG.AutoGenerateColumns = false;

            var EntregaList = controlador.conseguirLasEntregasPorSentencia();
            source.DataSource = EntregaList;//EntregaList;
            EntregasDG.DataSource = source;
        }
        private void agregarColumnasEntregaGrid()
        {
            
            DataGridViewColumn dataGridViewColumn;

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "codEntrega";
            dataGridViewColumn.HeaderText = "Cod. Entrega";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.ReadOnly = true;
            //EntregasDG.Columns.Insert(0, dataGridViewColumn);
            dataGridViewColumn.Name = "codEntrega";
            EntregasDG.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "Proveedor.nombre";
            dataGridViewColumn.HeaderText = "Proveedor";
            dataGridViewColumn.ReadOnly = true;
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            //EntregasDG.Columns.Insert(1, dataGridViewColumn);
            dataGridViewColumn.Name = "Proveedor.nombre";
            EntregasDG.Columns.Add(dataGridViewColumn);



            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "Insumos.Nombre";
            dataGridViewColumn.ReadOnly = true;
            dataGridViewColumn.HeaderText = "Insumo";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            //EntregasDG.Columns.Insert(2, dataGridViewColumn);
            dataGridViewColumn.Name = "Insumos.Nombre";
            //dataGridViewColumn.Visible = false;
            EntregasDG.Columns.Add(dataGridViewColumn);

          


            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "Cantidad";
            dataGridViewColumn.HeaderText = "Cantidad";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.ReadOnly = true;
            dataGridViewColumn.Name = "Cantidad";
            //EntregasDG.Columns.Insert(3, dataGridViewColumn);
            EntregasDG.Columns.Add(dataGridViewColumn);



            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "Unidad";
            dataGridViewColumn.ReadOnly = true;
            dataGridViewColumn.HeaderText = "Unidad";
            dataGridViewColumn.ReadOnly = true;
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            //EntregasDG.Columns.Insert(2, dataGridViewColumn);
            dataGridViewColumn.Name = "Unidad";
            //dataGridViewColumn.Visible = false;
            EntregasDG.Columns.Add(dataGridViewColumn);




            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "PrecioUnidad";
            dataGridViewColumn.HeaderText = "PrecioUnidad";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.ReadOnly = true;
            dataGridViewColumn.Name = "PrecioUnidad";
            //EntregasDG.Columns.Insert(4, dataGridViewColumn);
            EntregasDG.Columns.Add(dataGridViewColumn);



            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "fechaDeEntrega";
            dataGridViewColumn.HeaderText = "Fecha de Entrega";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "fechaDeEntrega";
            dataGridViewColumn.ReadOnly = true;
            //EntregasDG.Columns.Insert(5, dataGridViewColumn);

             EntregasDG.Columns.Add(dataGridViewColumn);


            var deleteButton = new DataGridViewButtonColumn();
            deleteButton.DataPropertyName = "Borrar";
            deleteButton.Name = "Borrar";
            deleteButton.HeaderText = "Borrar";
            deleteButton.Text = "Borrar";
            //deleteButton.DefaultCellStyle.BackColor = Color.Red;
            deleteButton.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            deleteButton.CellTemplate = new DataGridViewButtonCell();
            deleteButton.UseColumnTextForButtonValue = true;
            //EntregasDG.Columns.Insert(6, deleteButton);

            EntregasDG.Columns.Add(deleteButton);

            var ingresarButton = new DataGridViewButtonColumn();
            ingresarButton.DataPropertyName = "Editar";
            ingresarButton.Name = "Editar";
            ingresarButton.HeaderText = "Editar";
            ingresarButton.Text = "Editar";
            ingresarButton.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            ingresarButton.CellTemplate = new DataGridViewButtonCell();
            ingresarButton.UseColumnTextForButtonValue = true;
            EntregasDG.Columns.Add(ingresarButton);


        }

        private void borrarEntrega(DataGridViewCellEventArgs e)
        {
            //Put some logic here, for example to remove row from your binding list.
            int index = e.RowIndex;

            DialogResult dialogResult = MessageBox.Show("Esta seguro de borrar el Entrega?", "Confirmar el dialogo.", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                string message;
                int recordsAffected = controlador.borrarEntregaDelIndice(index, out message);

                if (recordsAffected == 1)
                {
                    EntregasDG = null;
                    MessageBox.Show("Entrega correctamente borrado.");
                    cambiarGridEntrega();
                }
                else
                {
                    MessageBox.Show("Hubo problemas al borrar la columna:" + message, "Error");
                }
            }
        }
        private void editarEntrega(DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            int idEntrega = controlador.ListaDeEntregas[index].CodEntrega;
            ModificarEntrega interfazNuevoEntrega = new ModificarEntrega(idEntrega);
            interfazNuevoEntrega.ShowDialog(this);
            EntregasDG.DataSource = null;
            controlador.conseguirEntregas();
            verEntregas();
            interfazNuevoEntrega.Dispose();
        }
                                   

        private void EntregasDG_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == EntregasDG.NewRowIndex || e.RowIndex < 0)
                return;

            //Check if click is on specific column 
            if (e.ColumnIndex == EntregasDG.Columns["Borrar"].Index)
            {
                borrarEntrega(e);
            }
            if (e.ColumnIndex == EntregasDG.Columns["Editar"].Index)
            {
                editarEntrega(e);
            }
        }

        private void volverBtn_Click_1(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

    
    }
  
}
