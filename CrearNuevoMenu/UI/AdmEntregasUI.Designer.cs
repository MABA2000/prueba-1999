﻿namespace CrearNuevoMenu.UI
{
    partial class AdmEntregasUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdmEntregasUI));
            this.label1 = new System.Windows.Forms.Label();
            this.añadirEntrega = new System.Windows.Forms.Button();
            this.volverBtn = new System.Windows.Forms.Button();
            this.EntregasDG = new System.Windows.Forms.DataGridView();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.EntregasDG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Gold;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(33, 200);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(890, 31);
            this.label1.TabIndex = 30;
            this.label1.Text = "ENTREGAS REGISTRADAS EN EL SISTEMA";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // añadirEntrega
            // 
            this.añadirEntrega.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.añadirEntrega.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.añadirEntrega.Location = new System.Drawing.Point(592, 437);
            this.añadirEntrega.Name = "añadirEntrega";
            this.añadirEntrega.Size = new System.Drawing.Size(158, 34);
            this.añadirEntrega.TabIndex = 29;
            this.añadirEntrega.Text = "AÑADIR ENTREGA NUEVA";
            this.añadirEntrega.UseVisualStyleBackColor = false;
            this.añadirEntrega.Click += new System.EventHandler(this.añadirEntrega_Click);
            // 
            // volverBtn
            // 
            this.volverBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.volverBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.volverBtn.Location = new System.Drawing.Point(177, 437);
            this.volverBtn.Name = "volverBtn";
            this.volverBtn.Size = new System.Drawing.Size(84, 34);
            this.volverBtn.TabIndex = 26;
            this.volverBtn.Text = "VOLVER";
            this.volverBtn.UseVisualStyleBackColor = false;
            this.volverBtn.Click += new System.EventHandler(this.volverBtn_Click_1);
            // 
            // EntregasDG
            // 
            this.EntregasDG.AllowUserToAddRows = false;
            this.EntregasDG.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.EntregasDG.Location = new System.Drawing.Point(30, 231);
            this.EntregasDG.Name = "EntregasDG";
            this.EntregasDG.Size = new System.Drawing.Size(896, 200);
            this.EntregasDG.TabIndex = 25;
            this.EntregasDG.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.EntregasDG_CellClick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(346, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(253, 168);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 31;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.UseWaitCursor = true;
            // 
            // AdmEntregasUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.ClientSize = new System.Drawing.Size(938, 661);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.añadirEntrega);
            this.Controls.Add(this.volverBtn);
            this.Controls.Add(this.EntregasDG);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AdmEntregasUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AdmEntregasUI";
            this.Load += new System.EventHandler(this.AdmEntregasUI_Load);
            ((System.ComponentModel.ISupportInitialize)(this.EntregasDG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button añadirEntrega;
        private System.Windows.Forms.Button volverBtn;
        private System.Windows.Forms.DataGridView EntregasDG;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}