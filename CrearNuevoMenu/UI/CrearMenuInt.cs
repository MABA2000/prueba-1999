﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Controller;
using System.Windows.Forms;
namespace CrearNuevoMenu.CrearMenuForm
{
    public partial class CrearMenuInt : Form
    {
        private MenuControlador controlador;
        public CrearMenuInt()
        {
            controlador = new MenuControlador();
            InitializeComponent();
        }
        public MenuControlador Controller
        {
            get
            {
                return controlador;
            }
        }
        private void CrearMenuInt_Load_1(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void crear_Click(object sender, EventArgs e)
        {
            string nombre = textNombre.Text;
            string descripcion = textDescripcion.Text;
            if (string.IsNullOrEmpty(nombre) || string.IsNullOrEmpty(descripcion))
            {
                Console.Beep(1500, 300);
                MessageBox.Show("Es obligatorio que los campos esten llenos.");
                return;
            }
            var result = controlador.InsertarMenu(0, nombre, DateTime.Today, descripcion, false);
            if (result > 0)
            {
                MessageBox.Show("Guardado Exitoso.");
            }
            else
            {
                MessageBox.Show("Hubo un problema guardando los datos.", "Error");
            }
        }
    }
}
