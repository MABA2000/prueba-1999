﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrearNuevoMenu.Controller;
using CrearNuevoMenu.Mapper; 
namespace CrearNuevoMenu.UI
{
    public partial class DatosInsumoUI : Form
    {
        private InsumoControlador _controller;
        string codeItem;
        string codeInsumo;
        public DatosInsumoUI(string codeItem,string codeInsumo)
        {
            InitializeComponent();
            this.codeItem = codeItem;
            this.codeInsumo = codeInsumo;

            _controller = new InsumoControlador(codeItem);
        }
        public InsumoControlador Controller
        {
            get
            {
                return _controller;
            }
        }

        private void DatosInsumoUI_Load(object sender, EventArgs e)
        {

        }
        private bool validacion_Campos()
        {
            bool ok = true; int num;
            if (txt_cantidad.Text == "" )
            {
                ok = false;
                errorProvider1.SetError(txt_cantidad, "Ingrese el Cantidad");
            }
            if (txt_Medida.Text == "" )
            {
                ok = false;
                errorProvider1.SetError(txt_Medida, "Ingrese el Medida");
            }
            if ((!int.TryParse(txt_cantidad.Text, out num))){ ok = false; }
            else
            {
                if (Convert.ToInt32(txt_cantidad.Text) < 0)
                {
                    ok = false;
                }
            }
            if ((!int.TryParse(txt_Medida.Text, out num))) { ok = false; }
            else
            {
                if (float.Parse(txt_Medida.Text) < 0)
                {
                    ok = false;
                }
            }
            int a;
            if (!int.TryParse(txt_cantidad.Text, out a))
            {
                errorProvider1.SetError(txt_cantidad, "Ingrese valor en numeros");
            }
            else
            {
                if (Convert.ToInt32(txt_cantidad.Text) < 0)
                {
                    errorProvider1.SetError(txt_cantidad, "No se puede ingresar valores negativos");
                }
                else
                {
                    errorProvider1.SetError(txt_cantidad, "");
                }
            }
            float b;

            if (!float.TryParse(txt_Medida.Text, out b))
            {
                errorProvider1.SetError(txt_Medida, "Ingrese valor en numeros");
            }
            else
            {
                if (float.Parse(txt_Medida.Text) < 0)
                {
                    errorProvider1.SetError(txt_Medida, "No se puede ingresar valores negativos");
                }
                else
                {
                    errorProvider1.SetError(txt_Medida, "");
                }

            }

            return ok;
        }
        private void txt_Medida_Validating(object sender, CancelEventArgs e)
        {
            float num;
            if (!float.TryParse(txt_Medida.Text, out num))
            {
                errorProvider1.SetError(txt_Medida, "Ingrese valor en numeros");
            }
            else
            {
                if (float.Parse(txt_Medida.Text) < 0)
                {
                    errorProvider1.SetError(txt_Medida, "No se puede ingresar valores negativos");
                }
                else
                {
                    errorProvider1.SetError(txt_Medida, "");
                }
            }
        }
        private void txt_cantidad_Validating(object sender, CancelEventArgs e)
        {
            int num;
            if (!int.TryParse(txt_cantidad.Text, out num))
            {
                errorProvider1.SetError(txt_cantidad, "Ingrese valor en numeros");
            }
            else
            {
                if (Convert.ToInt32(txt_cantidad.Text) < 0)
                {
                    errorProvider1.SetError(txt_cantidad, "No se puede ingresar valores negativos");
                }
                else
                {
                    errorProvider1.SetError(txt_cantidad, "");
                }
            }
        }
        private void BorrarMensajeError()
        {
            errorProvider1.SetError(txt_cantidad, "");
            errorProvider1.SetError(txt_Medida, "");
        }
        private void btbGuardar_Click(object sender, EventArgs e) 
        {
            BorrarMensajeError();
            if (validacion_Campos())
            {
                var result = Controller.InsertarInsumoAItem(Convert.ToInt32(codeItem), Convert.ToInt32(codeInsumo), "", "", Convert.ToDouble(txt_Medida.Text), Convert.ToInt32(txt_cantidad.Text));

                if (result > 0)
                {

                    MessageBox.Show("Guardo Exitosamente");
                }
                else
                {
                    MessageBox.Show("Hubo problemas en el registro", "Error");
                }
                this.Close();
            }
        }

        private void txt_Medida_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
