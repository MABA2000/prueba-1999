﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Controller;
using CrearNuevoMenu.UI;
using System.Windows.Forms;

namespace CrearNuevoMenu.UI
{
    public partial class CrearFactura : Form
    {
        private OrdenControlador controlador;
        private FacturaControlador controladorFactura;
        private int codOrd;
        public CrearFactura()
        {
            InitializeComponent();
            controlador = new OrdenControlador();
            controladorFactura = new FacturaControlador();
            codOrd = -1;
        }

        private void BVolver_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void CrearFactura_Load(object sender, EventArgs e)
        {
            cambiarGridOrden();
        }
        private void cambiarGridOrden()
        {
            DGVOrdenes.AutoGenerateColumns = false;
            agregarColumnasOrdenesGrid();
            verOrdenes();
        }
        private void verOrdenes()
        {
            var source = new BindingSource();
            var ordenList = controlador.ListaDeOrdenes;
            source.DataSource = ordenList;
            DGVOrdenes.DataSource = source;
        }
        private void agregarColumnasOrdenesGrid()
        {

            DataGridViewColumn dataGridViewColumn;
            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "codOrden";
            dataGridViewColumn.HeaderText = "Codigo";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "codOrden";
            DGVOrdenes.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "estadoItem";
            dataGridViewColumn.HeaderText = "Estado Del Item";
            dataGridViewColumn.CellTemplate = new DataGridViewCheckBoxCell();
            dataGridViewColumn.Name = "estadoItem";
            DGVOrdenes.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "numeroMesa";
            dataGridViewColumn.HeaderText = "Numero De Mesa";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "numeroMesa";
            DGVOrdenes.Columns.Add(dataGridViewColumn);


            dataGridViewColumn = new DataGridViewColumn();
            //menuDGV
            dataGridViewColumn.DataPropertyName = "horaDeFinalizacion";
            dataGridViewColumn.HeaderText = "Hora De Finalización";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.ValueType = typeof(DateTime);
            dataGridViewColumn.DefaultCellStyle.Format = "hh:mm:ss tt";
            dataGridViewColumn.Name = "horaDeFinalizacion";
            DGVOrdenes.Columns.Add(dataGridViewColumn);   
        }
        private void DGVOrdenes_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            if (index >= 0)
            {
                codOrd = controlador.ListaDeOrdenes[index].codOrden;
                lblCod.Text = codOrd.ToString();
            }
        }

        private void DGVOrdenes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void BCrear_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(CITextBox.Text.ToString()))
            {
                MessageBox.Show("Es necesario agregar CI.", "Mensaje");
                return;
            }
            if (codOrd >= 0) {
                int ci = Convert.ToInt32(CITextBox.Text);
                var result=controladorFactura.InsertarFactura(DateTime.Now.ToLocalTime()
                , DateTime.Today, ci, codOrd);
                if (result > 0)
                {
                    MessageBox.Show("Factura Añadida.", "Mensaje");
                    this.Close();
                    this.Dispose();
                }
                else
                {
                    MessageBox.Show("Hubo un problema guardando los datos.", "Error");
                }
            }
            
            codOrd = -1;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
        private void CITextBox_TextChanged(object sender, EventArgs e)
        {

          
        }
        void soloNumeros(KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        private void CITextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            soloNumeros(e);
        }

        private void BBuscarMesa_Click(object sender, EventArgs e)
        {
            int mesa;
            if (string.IsNullOrEmpty(NroMtextBox.Text.ToString()))
                controlador.conseguirOrdenes();
            else
            {
                mesa = Convert.ToInt32(NroMtextBox.Text);
                controlador.conseguirOrdenes(mesa);
            }
            verOrdenes();
        }

        private void NroMtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            soloNumeros(e);
        }

        private void BCancelarCambios_Click(object sender, EventArgs e)
        {
        }
    }
}
