﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Controller;
using CrearNuevoMenu.CrearMenuForm;
using CrearNuevoMenu.UIPrincipal;
using System.Windows.Forms;
namespace CrearNuevoMenu.UI
{
    public partial class AdministrarFacturas : Form
    {
        private FacturaControlador controlador;
        public AdministrarFacturas()
        {
            InitializeComponent();
            controlador = new FacturaControlador();
        }
        private void cambiarGridFacturas()
        {
            DGVFacturas.AutoGenerateColumns = false;
            agregarColumnasFacturasGrid();
            verFacturas();
        }
        private void verFacturas()
        {
            var source = new BindingSource();
            var facturaList = controlador.ListaDeFacturas;
            source.DataSource = facturaList;
            DGVFacturas.DataSource = source;
        }
        private void agregarColumnasFacturasGrid()
        {

            DataGridViewColumn dataGridViewColumn;
            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "horaEmision";
            dataGridViewColumn.HeaderText = "Hora Emision";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.ValueType = typeof(DateTime);
            dataGridViewColumn.Visible = true;
            dataGridViewColumn.DefaultCellStyle.Format = "hh:mm:ss tt";
            dataGridViewColumn.Name = "HoraEmision";
            DGVFacturas.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "FechaEmision";
            dataGridViewColumn.HeaderText = "Fecha Emision";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.ValueType = typeof(DateTime);
            dataGridViewColumn.Visible = true;
            dataGridViewColumn.Name = "FechaEmision";
            DGVFacturas.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "ciCliente";
            dataGridViewColumn.HeaderText = "CI Cliente";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "ciCliente";
            DGVFacturas.Columns.Add(dataGridViewColumn);



            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "codOrden";
            dataGridViewColumn.HeaderText = "Codigo Orden";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "codOrden";
            DGVFacturas.Columns.Add(dataGridViewColumn);

            var deleteButton = new DataGridViewButtonColumn();
            deleteButton.DataPropertyName = "borrar";
            deleteButton.Name = "borrar";
            deleteButton.HeaderText = "borrar";
            deleteButton.Text = "borrar";
            deleteButton.DefaultCellStyle.BackColor = Color.Red;
            deleteButton.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            deleteButton.CellTemplate = new DataGridViewButtonCell();
            deleteButton.UseColumnTextForButtonValue = true;
            DGVFacturas.Columns.Add(deleteButton);

        }
        private void AdministrarFacturas_Load(object sender, EventArgs e)
        {
            cambiarGridFacturas();
        }

        private void BVolver_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
        private void borrarFactura(DataGridViewCellEventArgs e)
        {
            //Put some logic here, for example to remove row from your binding list.
            int index = e.RowIndex;
            DialogResult dialogResult = MessageBox.Show("Esta seguro de borrar la factura?", "Confirmar el dialogo.", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                string message;
                int recordsAffected = controlador.borrarFacturaDelIndice(index, out message);

                if (recordsAffected == 1)
                {
                    DGVFacturas.DataSource = null;
                    verFacturas();
                    MessageBox.Show("Columna fue exitosamente borrada.");
                }
                else
                {
                    MessageBox.Show("Hubo problemas al borrar la columna:" + message, "Error");
                }
            }
        }
        private void DGVFacturas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            MessageBox.Show("1");
            if (e.ColumnIndex == DGVFacturas.Columns["borrar"].Index)
            {
                MessageBox.Show("2");
                borrarFactura(e);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void BCrear_Click(object sender, EventArgs e)
        {
            CrearFactura interfazNuevaFactura = new CrearFactura();
            interfazNuevaFactura.ShowDialog(this);
            DGVFacturas.DataSource = null;
            controlador.conseguirFacturas();
            verFacturas();
            interfazNuevaFactura.Dispose();
        }

        private void DGVFacturas_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DGVFacturas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == DGVFacturas.Columns["borrar"].Index)
            {
                borrarFactura(e);
            }
        }

        private void DGVFacturas_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void BCancelarCambios_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Seguro de no cambiar los datos?", "Confirmar.", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                controlador.conseguirFacturas();
                verFacturas();
            }
        }

        private void BGuardar_Click(object sender, EventArgs e)
        {
            string mensaje;
            int filasAfectadas = controlador.guardarCambiosDeFacturas(out mensaje);
            if (mensaje.Length > 0)
            {
                MessageBox.Show("Hubo un error al guardar los cambios: " + mensaje);
            }
            else
            {
                MessageBox.Show(filasAfectadas.ToString() + " records were affected");
            }
            controlador.conseguirFacturas();
            verFacturas();
        }

        private void DGVFacturas_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            controlador.ListaDeFacturas[index].huboCambios = true;
        }
    }
}
