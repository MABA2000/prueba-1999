﻿namespace CrearNuevoMenu.UI 
{
    partial class ItemAddUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbl_Nombre = new System.Windows.Forms.Label();
            this.lbl_PrecioElaboracion = new System.Windows.Forms.Label();
            this.lbl_TiempoElaboracion = new System.Windows.Forms.Label();
            this.lbl_Calorias = new System.Windows.Forms.Label();
            this.txt_Nombre = new System.Windows.Forms.TextBox();
            this.txt_PrecioElaboracion = new System.Windows.Forms.TextBox();
            this.txt_TiempoElaboracion = new System.Windows.Forms.TextBox();
            this.txt_Calorias = new System.Windows.Forms.TextBox();
            this.btn_Aceptar = new System.Windows.Forms.Button();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.txt_tipo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_Descripcion = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_Nombre
            // 
            this.lbl_Nombre.AutoSize = true;
            this.lbl_Nombre.Location = new System.Drawing.Point(51, 25);
            this.lbl_Nombre.Name = "lbl_Nombre";
            this.lbl_Nombre.Size = new System.Drawing.Size(50, 13);
            this.lbl_Nombre.TabIndex = 1;
            this.lbl_Nombre.Text = "Nombre :";
            this.lbl_Nombre.Click += new System.EventHandler(this.lbl_Nombre_Click);
            // 
            // lbl_PrecioElaboracion
            // 
            this.lbl_PrecioElaboracion.AutoSize = true;
            this.lbl_PrecioElaboracion.Location = new System.Drawing.Point(50, 100);
            this.lbl_PrecioElaboracion.Name = "lbl_PrecioElaboracion";
            this.lbl_PrecioElaboracion.Size = new System.Drawing.Size(117, 13);
            this.lbl_PrecioElaboracion.TabIndex = 2;
            this.lbl_PrecioElaboracion.Text = "Precio de Elaboracion :";
            this.lbl_PrecioElaboracion.Click += new System.EventHandler(this.lbl_PrecioElaboracion_Click);
            // 
            // lbl_TiempoElaboracion
            // 
            this.lbl_TiempoElaboracion.AutoSize = true;
            this.lbl_TiempoElaboracion.Location = new System.Drawing.Point(50, 51);
            this.lbl_TiempoElaboracion.Name = "lbl_TiempoElaboracion";
            this.lbl_TiempoElaboracion.Size = new System.Drawing.Size(122, 13);
            this.lbl_TiempoElaboracion.TabIndex = 3;
            this.lbl_TiempoElaboracion.Text = "Tiempo de Elaboracion :";
            this.lbl_TiempoElaboracion.Click += new System.EventHandler(this.TiempoElaboracion_Click);
            // 
            // lbl_Calorias
            // 
            this.lbl_Calorias.AutoSize = true;
            this.lbl_Calorias.Location = new System.Drawing.Point(50, 73);
            this.lbl_Calorias.Name = "lbl_Calorias";
            this.lbl_Calorias.Size = new System.Drawing.Size(50, 13);
            this.lbl_Calorias.TabIndex = 4;
            this.lbl_Calorias.Text = "Calorias :";
            // 
            // txt_Nombre
            // 
            this.txt_Nombre.Location = new System.Drawing.Point(189, 19);
            this.txt_Nombre.Name = "txt_Nombre";
            this.txt_Nombre.Size = new System.Drawing.Size(130, 20);
            this.txt_Nombre.TabIndex = 8;
            this.txt_Nombre.TextChanged += new System.EventHandler(this.txt_Nombre_TextChanged);
            // 
            // txt_PrecioElaboracion
            // 
            this.txt_PrecioElaboracion.Location = new System.Drawing.Point(189, 97);
            this.txt_PrecioElaboracion.Name = "txt_PrecioElaboracion";
            this.txt_PrecioElaboracion.Size = new System.Drawing.Size(130, 20);
            this.txt_PrecioElaboracion.TabIndex = 9;
            this.txt_PrecioElaboracion.TextChanged += new System.EventHandler(this.txt_PrecioElaboracion_TextChanged);
            this.txt_PrecioElaboracion.Validating += new System.ComponentModel.CancelEventHandler(this.txt_PrecioElaboracion_Validating);
            // 
            // txt_TiempoElaboracion
            // 
            this.txt_TiempoElaboracion.Location = new System.Drawing.Point(189, 45);
            this.txt_TiempoElaboracion.Name = "txt_TiempoElaboracion";
            this.txt_TiempoElaboracion.Size = new System.Drawing.Size(130, 20);
            this.txt_TiempoElaboracion.TabIndex = 10;
            this.txt_TiempoElaboracion.TextChanged += new System.EventHandler(this.txt_TiempoElaboracion_TextChanged);
            this.txt_TiempoElaboracion.Validating += new System.ComponentModel.CancelEventHandler(this.txt_TiempoElaboracion_Validating);
            // 
            // txt_Calorias
            // 
            this.txt_Calorias.Location = new System.Drawing.Point(189, 71);
            this.txt_Calorias.Name = "txt_Calorias";
            this.txt_Calorias.Size = new System.Drawing.Size(130, 20);
            this.txt_Calorias.TabIndex = 11;
            this.txt_Calorias.TextChanged += new System.EventHandler(this.txt_Calorias_TextChanged);
            this.txt_Calorias.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Calorias_Validating);
            // 
            // btn_Aceptar
            // 
            this.btn_Aceptar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btn_Aceptar.Location = new System.Drawing.Point(35, 210);
            this.btn_Aceptar.Name = "btn_Aceptar";
            this.btn_Aceptar.Size = new System.Drawing.Size(93, 32);
            this.btn_Aceptar.TabIndex = 15;
            this.btn_Aceptar.Text = "Aceptar";
            this.btn_Aceptar.UseVisualStyleBackColor = false;
            this.btn_Aceptar.Click += new System.EventHandler(this.btn_Aceptar_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btn_Cancel.Location = new System.Drawing.Point(217, 210);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(102, 32);
            this.btn_Cancel.TabIndex = 16;
            this.btn_Cancel.Text = "Cancelar";
            this.btn_Cancel.UseVisualStyleBackColor = false;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // txt_tipo
            // 
            this.txt_tipo.Location = new System.Drawing.Point(189, 154);
            this.txt_tipo.Name = "txt_tipo";
            this.txt_tipo.Size = new System.Drawing.Size(130, 20);
            this.txt_tipo.TabIndex = 19;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(50, 157);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Tipo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Descripcion:";
            // 
            // txt_Descripcion
            // 
            this.txt_Descripcion.Location = new System.Drawing.Point(189, 128);
            this.txt_Descripcion.Name = "txt_Descripcion";
            this.txt_Descripcion.Size = new System.Drawing.Size(130, 20);
            this.txt_Descripcion.TabIndex = 22;
            this.txt_Descripcion.TextChanged += new System.EventHandler(this.txt_Descripcion_TextChanged_1);
            // 
            // ItemAddUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(372, 278);
            this.Controls.Add(this.txt_Descripcion);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_tipo);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Aceptar);
            this.Controls.Add(this.txt_Calorias);
            this.Controls.Add(this.txt_TiempoElaboracion);
            this.Controls.Add(this.txt_PrecioElaboracion);
            this.Controls.Add(this.txt_Nombre);
            this.Controls.Add(this.lbl_Calorias);
            this.Controls.Add(this.lbl_TiempoElaboracion);
            this.Controls.Add(this.lbl_PrecioElaboracion);
            this.Controls.Add(this.lbl_Nombre);
            this.Name = "ItemAddUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Añadir Item";
            this.Load += new System.EventHandler(this.ItemAddUI_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lbl_Nombre;
        private System.Windows.Forms.Label lbl_PrecioElaboracion;
        private System.Windows.Forms.Label lbl_TiempoElaboracion;
        private System.Windows.Forms.Label lbl_Calorias;
        private System.Windows.Forms.TextBox txt_Nombre;
        private System.Windows.Forms.TextBox txt_PrecioElaboracion;
        private System.Windows.Forms.TextBox txt_TiempoElaboracion;
        private System.Windows.Forms.TextBox txt_Calorias;
        private System.Windows.Forms.Button btn_Aceptar;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_tipo;
        private System.Windows.Forms.TextBox txt_Descripcion;
        private System.Windows.Forms.Label label3;
    }
}