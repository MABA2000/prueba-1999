﻿namespace CrearNuevoMenu.UI 
{
    partial class DatosInsumoUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btbGuardar = new System.Windows.Forms.Button();
            this.btbCancelar = new System.Windows.Forms.Button();
            this.txt_Medida = new System.Windows.Forms.TextBox();
            this.txt_cantidad = new System.Windows.Forms.TextBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ingrese la Medida : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ingrese la cantidad :";
            // 
            // btbGuardar
            // 
            this.btbGuardar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btbGuardar.Location = new System.Drawing.Point(46, 135);
            this.btbGuardar.Name = "btbGuardar";
            this.btbGuardar.Size = new System.Drawing.Size(75, 23);
            this.btbGuardar.TabIndex = 2;
            this.btbGuardar.Text = "Guardar";
            this.btbGuardar.UseVisualStyleBackColor = false;
            this.btbGuardar.Click += new System.EventHandler(this.btbGuardar_Click);
            // 
            // btbCancelar
            // 
            this.btbCancelar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btbCancelar.Location = new System.Drawing.Point(143, 135);
            this.btbCancelar.Name = "btbCancelar";
            this.btbCancelar.Size = new System.Drawing.Size(75, 23);
            this.btbCancelar.TabIndex = 3;
            this.btbCancelar.Text = "Cancelar";
            this.btbCancelar.UseVisualStyleBackColor = false;
            // 
            // txt_Medida
            // 
            this.txt_Medida.Location = new System.Drawing.Point(121, 49);
            this.txt_Medida.Name = "txt_Medida";
            this.txt_Medida.Size = new System.Drawing.Size(79, 20);
            this.txt_Medida.TabIndex = 4;
            this.txt_Medida.TextChanged += new System.EventHandler(this.txt_Medida_TextChanged);
            this.txt_Medida.Validating += new System.ComponentModel.CancelEventHandler(this.txt_Medida_Validating);
            // 
            // txt_cantidad
            // 
            this.txt_cantidad.Location = new System.Drawing.Point(121, 82);
            this.txt_cantidad.Name = "txt_cantidad";
            this.txt_cantidad.Size = new System.Drawing.Size(79, 20);
            this.txt_cantidad.TabIndex = 5;
            this.txt_cantidad.Validating += new System.ComponentModel.CancelEventHandler(this.txt_cantidad_Validating);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // DatosInsumoUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.ClientSize = new System.Drawing.Size(263, 201);
            this.Controls.Add(this.txt_cantidad);
            this.Controls.Add(this.txt_Medida);
            this.Controls.Add(this.btbCancelar);
            this.Controls.Add(this.btbGuardar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "DatosInsumoUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DatosInsumo";
            this.Load += new System.EventHandler(this.DatosInsumoUI_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btbGuardar;
        private System.Windows.Forms.Button btbCancelar;
        private System.Windows.Forms.TextBox txt_Medida;
        private System.Windows.Forms.TextBox txt_cantidad;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}