﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrearNuevoMenu.Controller;

namespace CrearNuevoMenu.UI
{
    public partial class AdmInsumoUI : Form
    {
        private InsumoControlador controlador;
        public AdmInsumoUI()
        {
            controlador = new InsumoControlador();
            InitializeComponent();
        }
        public InsumoControlador Controlador
        {
            get
            {
                return controlador;
            }
        }
        private void AdmInsumoUI_Load(object sender, EventArgs e)
        {
            cambiarGridInsumo();

        }
        private void cambiarGridInsumo()
        {
            InsumosDG.AutoGenerateColumns = false;
            agregarColumnasInsumoGrid();
            verInsumos();
        }
        private void verInsumos()
        {
            var source = new BindingSource();
            var InsumoList = controlador.ListaDeInsumos;
            source.DataSource = InsumoList;
            InsumosDG.DataSource = source;
        }
        private void agregarColumnasInsumoGrid()
        {
        
            DataGridViewColumn dataGridViewColumn;

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "codInsumo";
            dataGridViewColumn.HeaderText = "Cod. Insumo";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "codInsumo";
            dataGridViewColumn.Visible = false;
            InsumosDG.Columns.Add(dataGridViewColumn);


            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "nombre";
            dataGridViewColumn.HeaderText = "Nombre";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "nombre";
            InsumosDG.Columns.Add(dataGridViewColumn);


            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "cantidad";
            dataGridViewColumn.HeaderText = "Cantidad";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "cantidad";
            dataGridViewColumn.ReadOnly = true;
            InsumosDG.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "unidad";
            dataGridViewColumn.HeaderText = "Unidad";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "unidad";
            InsumosDG.Columns.Add(dataGridViewColumn);

            var deleteButton = new DataGridViewButtonColumn();
            deleteButton.DataPropertyName = "borrar";
            deleteButton.Name = "Borrar";
            deleteButton.HeaderText = "borrar";
            deleteButton.Text = "Borrar";
            deleteButton.DefaultCellStyle.BackColor = Color.Red;
            deleteButton.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            deleteButton.CellTemplate = new DataGridViewButtonCell();
            deleteButton.UseColumnTextForButtonValue = true;
            InsumosDG.Columns.Add(deleteButton);

            var ingresarButton = new DataGridViewButtonColumn();
            ingresarButton.DataPropertyName = "ver";
            ingresarButton.Name = "Ver";
            ingresarButton.HeaderText = "ver";
            ingresarButton.Text = "Ver";
            ingresarButton.DefaultCellStyle.BackColor = Color.Green;
            ingresarButton.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            ingresarButton.CellTemplate = new DataGridViewButtonCell();
            ingresarButton.UseColumnTextForButtonValue = true;
            InsumosDG.Columns.Add(ingresarButton);
      

        }
      

        private void button2_Click(object sender, EventArgs e)
        {

            this.Close();
        }


        private void borrarInsumo(DataGridViewCellEventArgs e)
        {
            //Put some logic here, for example to remove row from your binding list.
            int index = e.RowIndex;

            DialogResult dialogResult = MessageBox.Show("Esta seguro de borrar el Insumo?", "Confirmar el dialogo.", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                string message;
                int recordsAffected = controlador.borrarInsumoDelIndice(index, out message);

                if (recordsAffected == 1)
                {
                    InsumosDG.DataSource = null;
                    verInsumos();
                    MessageBox.Show("Insumo correctamente borrado.");
                }
                else
                {
                    MessageBox.Show("Hubo problemas al borrar la columna:" + message, "Error");
                }
            }
        }
        //private void verInsumos(DataGridViewCellEventArgs e)
        //{
        //    int index = e.RowIndex;
        //    AdministrarInsumo interfazNuevoInsumo = new AdministrarInsumo(index);
        //    interfazNuevoInsumo.ShowDialog(this);
        //    InsumosDG.DataSource = null;
        //    controlador.conseguirInsumos();
        //    verInsumos();
        //    interfazNuevoInsumo.Dispose();
        //}
        

        private void añadirInsumo_Click(object sender, EventArgs e)
        {
            CrearInsumo interfazNuevoInsumo = new CrearInsumo();
            interfazNuevoInsumo.ShowDialog(this);
            InsumosDG.DataSource = null;
            controlador.conseguirInsumos();
            verInsumos();
            interfazNuevoInsumo.Dispose();
        }

        private void InsumosDG_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == InsumosDG.NewRowIndex || e.RowIndex < 0)
                return;

            //Check if click is on specific column 
            if (e.ColumnIndex == InsumosDG.Columns["borrar"].Index)
            {
                borrarInsumo(e);
            }
            if (e.ColumnIndex == InsumosDG.Columns["ver"].Index)
            {
                // verInsumos(e);
            }
        }

    
        private void guardarBtn_Click(object sender, EventArgs e)
        {
            string message;
            int recordsAffected = controlador.guardarInsumoDeMenuModificado(out message);
            if (message.Length > 0)
            {
                MessageBox.Show("Hubo errores al modificar:" + message);
            }
            else
            {
                MessageBox.Show(recordsAffected.ToString() + " se modificaron los datos");
            }
        }

        private void InsumosDG_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == InsumosDG.Columns["nombre"].Index || e.ColumnIndex == InsumosDG.Columns["unidad"].Index)
            {
                int index = e.RowIndex;
                controlador.ListaDeInsumos[index].IsDirty = true;
            }
        }

        private void cancelarCambiosBtn_Click(object sender, EventArgs e)
        {
            InsumosDG.DataSource = null;
            controlador.conseguirInsumos();
            verInsumos();
        }

        private void volverBtn_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
    }
}
