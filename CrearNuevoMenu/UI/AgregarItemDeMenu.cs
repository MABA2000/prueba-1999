﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrearNuevoMenu.Controller;

namespace CrearNuevoMenu.UI
{
    public partial class AgregarItemDeMenu : Form
    {
        int idMenu;
        private adminMenuControlador admiControlador;
        private AgrItemDeMenuController agrItemDeMenuController;
        public AgregarItemDeMenu(int idMenu)
        {
            this.idMenu = idMenu;
            InitializeComponent();
            admiControlador = new adminMenuControlador(idMenu);
            agrItemDeMenuController = new AgrItemDeMenuController();
        }
        public AgrItemDeMenuController AgrItemDeMenuController
        {
            get
            {
                return agrItemDeMenuController;
            }
        }
        public adminMenuControlador AdminControlador
        {
            get
            {
                return admiControlador;
            }
        }
        private void AgregarItemDeMenu_Load(object sender, EventArgs e)
        {
            cambiarGridItemsDeMenu();

        }
        private void cambiarGridItemsDeMenu()
        {
            ItemsDelSistDG.AutoGenerateColumns = false;
            agregarColumnasMenuGrid();
            verItemsDeSistema();
        }
        private void verItemsDeSistema()
        {
            var source = new BindingSource();
            var itemsDeMenuList = agrItemDeMenuController.ListaDeitemsDeSistema;
            source.DataSource = itemsDeMenuList;
            ItemsDelSistDG.DataSource = source;
            
        }
        private void volver_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
     
        private void agregarColumnasMenuGrid()
        {
            DataGridViewColumn dataGridViewColumn;

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "codItem";
            dataGridViewColumn.HeaderText = "Cod. Item";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "codItem";
            dataGridViewColumn.ReadOnly = true;
            ItemsDelSistDG.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "nombre";
            dataGridViewColumn.HeaderText = "Nombre";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Visible = true;
            dataGridViewColumn.Name = "nombre";
            dataGridViewColumn.ReadOnly = true;
            ItemsDelSistDG.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "tipo";
            dataGridViewColumn.HeaderText = "Tipo";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Visible = true;
            dataGridViewColumn.Name = "tipo";
            dataGridViewColumn.ReadOnly = true;
            ItemsDelSistDG.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "tiempoElaboracion";
            dataGridViewColumn.HeaderText = "Tiempo de elaboración [min]";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Visible = true;
            dataGridViewColumn.Name = "tiempoElaboracion";
            dataGridViewColumn.ReadOnly = true;
            ItemsDelSistDG.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "descripcion";
            dataGridViewColumn.HeaderText = "Descripcion";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "descripcion";
            dataGridViewColumn.ReadOnly = true;
            ItemsDelSistDG.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "precioDeElaboracion";
            dataGridViewColumn.HeaderText = "Precio de Elaboracion [Bs]";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "precioDeElaboracion";
            ItemsDelSistDG.Columns.Add(dataGridViewColumn);

            var deleteButton = new DataGridViewButtonColumn();
            deleteButton.DataPropertyName = "añadir";
            deleteButton.Name = "añadir";
            deleteButton.HeaderText = "Añadir";
            deleteButton.Text = "Añadir";
            deleteButton.DefaultCellStyle.BackColor = Color.Green;
            deleteButton.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            deleteButton.CellTemplate = new DataGridViewButtonCell();
            deleteButton.UseColumnTextForButtonValue = true;
            ItemsDelSistDG.Columns.Add(deleteButton);

        }
        private void ItemsDelSistDG_CellClick(object sender, DataGridViewCellEventArgs e)
        {
      
            string descripcion = "";
            string tipo="";
            string nombre = "";
            int index = e.RowIndex;
            int idItem = AgrItemDeMenuController.ListaDeitemsDeSistema[index].codItem;

            if (!admiControlador.existeItem(idMenu, idItem))
            {
                AgregarPrecioAItemDeMenu interfazNuevoMenu = new AgregarPrecioAItemDeMenu(idItem, idMenu, descripcion, tipo, nombre);
                interfazNuevoMenu.ShowDialog(this);
                ItemsDelSistDG.DataSource = null;
                agrItemDeMenuController.conseguiritemsDeSistema();
                verItemsDeSistema();
                interfazNuevoMenu.Dispose();
            }
            else
            {

                MessageBox.Show("El ítem ya se encuentra en el menú.", "Error");

            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void bEntrar_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
    }
}
