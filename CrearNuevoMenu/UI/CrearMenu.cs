﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Controller;
using CrearNuevoMenu.CrearMenuForm;
using CrearNuevoMenu.IngresarMenu;
using CrearNuevoMenu.UIPrincipal;
using System.Windows.Forms;
namespace CrearNuevoMenu
{
    public partial class CrearMenu : Form
    {
        private MenuControlador controlador;

        public CrearMenu()
        {
            InitializeComponent();
            controlador = new MenuControlador();
            //hora.Text = DateTime.Now.ToString("hh:mm:ss");
            //fecha.Text = DateTime.Now.ToString("dddd-dd/MM/yy");
        }
        public MenuControlador Controlador
        {
            get
            {
                return controlador;
            }
        }
        private void menuCarga(object sender, EventArgs e)
        {
            cambiarGridMenu();
        }
        private void cambiarGridMenu()
        {
            menuDGV.AutoGenerateColumns = false;
            agregarColumnasMenuGrid();
            verMenus();
        }
        private void verMenus()
        {
            var source = new BindingSource();
            var menuList = controlador.ListaDeMenus;
            source.DataSource = menuList;
            menuDGV.DataSource = source;
        }
        private void agregarColumnasMenuGrid()
        {

            DataGridViewColumn dataGridViewColumn;

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "id";
            dataGridViewColumn.HeaderText = "id";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "ID";
            dataGridViewColumn.Visible = false;
            menuDGV.Columns.Add(dataGridViewColumn);


            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "nombre";
            dataGridViewColumn.HeaderText = "Nombre";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "nombre";
            menuDGV.Columns.Add(dataGridViewColumn);


            dataGridViewColumn = new DataGridViewColumn();
            //menuDGV
            dataGridViewColumn.DataPropertyName = "fecha";
            dataGridViewColumn.HeaderText = "fecha";
            // dataGridViewColumn.DefaultCellStyle.Format = "dd/MM/YYYY";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.ValueType = typeof(DateTime);
            dataGridViewColumn.Visible = true;
            dataGridViewColumn.Name = "Fecha";
            menuDGV.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "descripcion";
            dataGridViewColumn.HeaderText = "descripcion";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "Descripcion";
            menuDGV.Columns.Add(dataGridViewColumn);

 

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "visible";
            dataGridViewColumn.HeaderText = "visible";
            dataGridViewColumn.CellTemplate = new DataGridViewCheckBoxCell();
            dataGridViewColumn.Name = "Visible";
            menuDGV.Columns.Add(dataGridViewColumn);

            var deleteButton = new DataGridViewButtonColumn();
            deleteButton.DataPropertyName = "borrar";
            deleteButton.Name = "Borrar";
            deleteButton.HeaderText = "borrar";
            deleteButton.Text = "Borrar";
            deleteButton.DefaultCellStyle.BackColor = Color.Red;
            deleteButton.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            deleteButton.CellTemplate = new DataGridViewButtonCell();
            deleteButton.UseColumnTextForButtonValue = true;
            menuDGV.Columns.Add(deleteButton);

            var ingresarButton = new DataGridViewButtonColumn();
            ingresarButton.DataPropertyName = "ver";
            ingresarButton.Name = "Ver";
            ingresarButton.HeaderText = "ver";
            ingresarButton.Text = "Ver";
            ingresarButton.DefaultCellStyle.BackColor = Color.Green;
            ingresarButton.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            ingresarButton.CellTemplate = new DataGridViewButtonCell();
            ingresarButton.UseColumnTextForButtonValue = true;
            menuDGV.Columns.Add(ingresarButton);
            /*
            var deleteButton = new DataGridViewButtonColumn();
            deleteButton.DataPropertyName = "Delete";
            deleteButton.Name = "dgAirportDeleteBtn";
            deleteButton.HeaderText = "Delete";
            deleteButton.Text = "Delete";
            deleteButton.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            deleteButton.CellTemplate = new DataGridViewButtonCell();
            deleteButton.UseColumnTextForButtonValue = true;
            menuDGV.Columns.Add(deleteButton);
            */


        }
        private void button1_Click(object sender, EventArgs e)
        {
            CrearMenuInt interfazNuevoMenu = new CrearMenuInt();
            interfazNuevoMenu.ShowDialog(this);
            menuDGV.DataSource = null;
            controlador.conseguirMenus();
            verMenus();
            interfazNuevoMenu.Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            this.Close();
        }


        private void borrarMenu(DataGridViewCellEventArgs e)
        {
            //Put some logic here, for example to remove row from your binding list.
            int index = e.RowIndex;

            DialogResult dialogResult = MessageBox.Show("Esta seguro de borrar el menu?", "Confirmar el dialogo.", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                string message;
                int recordsAffected = controlador.borrarMenuDelIndice(index, out message);

                if (recordsAffected == 1)
                {
                    menuDGV.DataSource = null;
                    verMenus();
                    MessageBox.Show("Columna fue exitosamente borrada.");
                }
                else
                {
                    MessageBox.Show("Hubo problemas al borrar la columna:" + message, "Error");
                }
            }
        }
        private void verMenus(DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            AdministrarMenu interfazNuevoMenu = new AdministrarMenu(index);
            this.Hide();
            interfazNuevoMenu.ShowDialog(this);
            this.Show();
            menuDGV.DataSource = null;
            controlador.conseguirMenus();
            verMenus();
            interfazNuevoMenu.Dispose();


        }
        private void menuDGV_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //if click is on new row or header row
            if (e.RowIndex == menuDGV.NewRowIndex || e.RowIndex < 0)
                return;

            //Check if click is on specific column 
            if (e.ColumnIndex == menuDGV.Columns["borrar"].Index)
            {
                borrarMenu(e);
            }
            if (e.ColumnIndex == menuDGV.Columns["ver"].Index)
            {
                verMenus(e);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        public static Boolean EsFecha(String fecha)
        {
            try
            {
                DateTime.Parse(fecha);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private void menuDGV_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void BGuardar_Click(object sender, EventArgs e)
        {
            string mensaje;
            int filasAfectadas = controlador.guardarCambiosDeMenus(out mensaje);
            if (mensaje.Length > 0)
            {
                MessageBox.Show("Hubo un error al guardar los cambios: " + mensaje);
            }
            else
            {
                MessageBox.Show(filasAfectadas.ToString() + " records were affected");
            }
            controlador.conseguirMenus();
            verMenus();
        }

        private void menuDGV_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
                int index = e.RowIndex;
                controlador.ListaDeMenus[index].IsDirty = true;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Seguro de no cambiar los datos?", "Confirmar.", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                controlador.conseguirMenus();
                verMenus();
            }
        }
    }
}
