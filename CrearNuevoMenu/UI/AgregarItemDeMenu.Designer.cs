﻿namespace CrearNuevoMenu.UI
{
    partial class AgregarItemDeMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AgregarItemDeMenu));
            this.ItemsDelSistDG = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.bEntrar = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.ItemsDelSistDG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // ItemsDelSistDG
            // 
            this.ItemsDelSistDG.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.ItemsDelSistDG.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ItemsDelSistDG.Location = new System.Drawing.Point(101, 245);
            this.ItemsDelSistDG.Name = "ItemsDelSistDG";
            this.ItemsDelSistDG.Size = new System.Drawing.Size(723, 151);
            this.ItemsDelSistDG.TabIndex = 5;
            this.ItemsDelSistDG.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ItemsDelSistDG_CellClick);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Gold;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(101, 226);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(723, 19);
            this.label2.TabIndex = 17;
            this.label2.Text = "ITÉMS DEL SISTEMA";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bEntrar
            // 
            this.bEntrar.BackColor = System.Drawing.Color.DarkOrange;
            this.bEntrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bEntrar.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bEntrar.Location = new System.Drawing.Point(403, 418);
            this.bEntrar.Name = "bEntrar";
            this.bEntrar.Size = new System.Drawing.Size(113, 54);
            this.bEntrar.TabIndex = 18;
            this.bEntrar.Text = "VOLVER";
            this.bEntrar.UseVisualStyleBackColor = false;
            this.bEntrar.Click += new System.EventHandler(this.bEntrar_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(338, 33);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(253, 168);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 27;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.UseWaitCursor = true;
            // 
            // AgregarItemDeMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.ClientSize = new System.Drawing.Size(938, 661);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.bEntrar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ItemsDelSistDG);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AgregarItemDeMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AgregarItemDeMenu";
            this.Load += new System.EventHandler(this.AgregarItemDeMenu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ItemsDelSistDG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView ItemsDelSistDG;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bEntrar;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}