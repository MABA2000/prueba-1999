﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Controller;
using System.Windows.Forms;
using CrearNuevoMenu.Model;
using CrearNuevoMenu.UI;

namespace CrearNuevoMenu.IngresarMenu
{
    public partial class AdministrarMenu : Form
    {
        int index;
        int idMenu;
        private adminMenuControlador admiControlador;
        private MenuControlador menControlador;
        private string nom;
        private DateTime fec;
        private string desc;
        private bool vis;
        public AdministrarMenu(int ind)
        {
            index = ind;
            
            InitializeComponent();
            menControlador = new MenuControlador();
            nom = "";
            desc = "";
            vis = false;
        }
        public MenuControlador MenuControlador
        {
            get
            {
                return menControlador;
            }
        }
        public adminMenuControlador AdminControlador
        {
            get
            {
                return admiControlador;
            }
        }
        private void AdministrarMenu_Load(object sender, EventArgs e) {
            menControlador.conseguirMenuDelIndice(index, out nom, out fec, out desc, out vis, out idMenu);
            admiControlador = new adminMenuControlador(idMenu);

            nombre.Text = nom;
            fecha.Text = fec.ToUniversalTime().ToString();
            descripcion.Text = desc;
            cbvisible.Checked = vis;
            cambiarGridItemsDeMenu();
        }
        private void cambiarGridItemsDeMenu()
        {
            itemsDelMenuDG.AutoGenerateColumns = false;
            agregarColumnasMenuGrid();
            verItemsDeMenu();
        }
        private void verItemsDeMenu()
        {
            cantidad.Text = admiControlador.conseguirCantidadDeItmesDelMenu(idMenu).ToString();
            var source = new BindingSource();
            itemsDelMenuDG.AutoGenerateColumns = false;
            var itemsDeMenuList = AdminControlador.listaPorSentencia(idMenu);//ListaDeItemsDeMenus;
            source.DataSource = itemsDeMenuList;
            itemsDelMenuDG.DataSource = source;
        }
        private void volver_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
        private void agregarColumnasMenuGrid()
        {
            DataGridViewColumn dataGridViewColumn;

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "codItem";
            dataGridViewColumn.HeaderText = "Cod. Item";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "codItem";
            dataGridViewColumn.ReadOnly = true;
            itemsDelMenuDG.Columns.Add(dataGridViewColumn);


            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "nombre";
            dataGridViewColumn.HeaderText = "Nombre";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "nombre";
            dataGridViewColumn.ReadOnly = true;
            itemsDelMenuDG.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "tipo";
            dataGridViewColumn.HeaderText = "Tipo";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Visible = true;
            dataGridViewColumn.Name = "tipo";
            dataGridViewColumn.ReadOnly = true;
            itemsDelMenuDG.Columns.Add(dataGridViewColumn);

         

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "precio";
            dataGridViewColumn.HeaderText = "Precio [Bs]";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "precio";
            itemsDelMenuDG.Columns.Add(dataGridViewColumn);


            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "fechaDeAdicion";
            dataGridViewColumn.HeaderText = "Fecha de Adición";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "fechaDeAdicion";
            dataGridViewColumn.ReadOnly = true;
            itemsDelMenuDG.Columns.Add(dataGridViewColumn);

            var deleteButton = new DataGridViewButtonColumn();
            deleteButton.DataPropertyName = "borrar";
            deleteButton.Name = "borrar";
            deleteButton.HeaderText = "Borrar";
            deleteButton.Text = "Borrar";
            deleteButton.DefaultCellStyle.BackColor = Color.Red;
            deleteButton.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            deleteButton.CellTemplate = new DataGridViewButtonCell();
            deleteButton.UseColumnTextForButtonValue = true;
            itemsDelMenuDG.Columns.Add(deleteButton);

        }
        private void añadirBtn_Click(object sender, EventArgs e)
        {
            AgregarItemDeMenu interfazNuevoMenu = new AgregarItemDeMenu(idMenu);
            
            interfazNuevoMenu.ShowDialog(this);
            itemsDelMenuDG.DataSource = null;
            admiControlador.conseguirItemsDeMenus(idMenu);
            verItemsDeMenu();
            interfazNuevoMenu.Close();
            interfazNuevoMenu.Dispose();
        }
        private void itemsDelMenuDG_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == itemsDelMenuDG.NewRowIndex || e.RowIndex < 0)
                return;


            //Check if click is on specific column 
           // if (admiControlador.existeItem(idMenu,idItem))
            if (e.ColumnIndex == itemsDelMenuDG.Columns["borrar"].Index)
            {
                int index = e.RowIndex;

                //Put some logic here, for example to remove row from your binding list.
                DialogResult dialogResult = MessageBox.Show("Esta seguro que quiere eliminar el item " + admiControlador.listaPorSentencia(idMenu)[index]["nombre"] + " ?", "Confirmación", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    string message;
                    int recordsAffected = admiControlador.eliminarItemDelMenuDelMenu(index, out message);

                    if (recordsAffected == 1)
                    {

                        itemsDelMenuDG.DataSource = null;
                        admiControlador.conseguirItemsDeMenus(idMenu);
                        verItemsDeMenu();
                        MessageBox.Show("Se borro el ítem del menú de manera satisfactoria!");
                        itemsDelMenuDG.DataSource = null;
                        admiControlador.conseguirItemsDeMenus(idMenu);
                        verItemsDeMenu();
                    }
                    else
                    {
                        MessageBox.Show("Hubo un error borando el item:" + message, "Error");
                    }
                }
            }
        }
        private void guardarBtn_Click(object sender, EventArgs e)
        {
            string message;
            int recordsAffected = admiControlador.guardarItemDeMenuModificado(out message);
            if (message.Length > 0)
            {
                MessageBox.Show("Hubo errores al modificar:" + message);
            }
            else
            {
                MessageBox.Show(recordsAffected.ToString() + " se modificaron los datos");
            }
        }
        private void itemsDelMenuDG_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            itemsDelMenuDG.Rows[e.RowIndex].ErrorText = "";
            float newfloat = 0;

            // Don't try to validate the 'new row' until finished 
            // editing since there
            // is not any point in validating its initial value.
            if (itemsDelMenuDG.Rows[e.RowIndex].IsNewRow) { return; }
            if (e.ColumnIndex == itemsDelMenuDG.Columns["precio"].Index && !float.TryParse(e.FormattedValue.ToString(), out newfloat))
            {

                e.Cancel = true;
                MessageBox.Show("El Precio de be ser un número.", "Error");
                itemsDelMenuDG.Rows[e.RowIndex].ErrorText = "El Precio de be ser un número.";

            }
            if (e.ColumnIndex == itemsDelMenuDG.Columns["precio"].Index && !admiControlador.comprobarQueNoHayPunto(e.FormattedValue.ToString()))//Esta condicion evita que se ingrese datos que no esten en el rangpo
            {
                e.Cancel = true;
                MessageBox.Show("Utilice ',' en vez de '.'", "Error");
                itemsDelMenuDG.Rows[e.RowIndex].ErrorText = "Utilice ',' en vez de '.'";
            }
        }
        private void itemsDelMenuDG_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == itemsDelMenuDG.Columns["precio"].Index )
            {
                int index = e.RowIndex;
                var precio = itemsDelMenuDG.Rows[index].Cells[e.ColumnIndex].Value;
                admiControlador.ListaDeItemsDeMenus[index].IsDirty = true;
                admiControlador.ListaDeItemsDeMenus[index].precio = Convert.ToSingle(precio);
            }
        }
        private void cancelarBtn_Click(object sender, EventArgs e)
        {
            itemsDelMenuDG.DataSource = null;
            admiControlador.conseguirItemsDeMenus(idMenu);
            verItemsDeMenu();
            MessageBox.Show("LA TABLA FUE ACTUALIZADA!");
        }

    }
}
