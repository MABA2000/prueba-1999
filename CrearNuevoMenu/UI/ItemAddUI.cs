﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrearNuevoMenu.Controller;

namespace CrearNuevoMenu.UI  
{
    public partial class ItemAddUI : Form
    {
        private ItemController _controller;
        public ItemAddUI()
        {
            InitializeComponent();
            _controller = new ItemController();
        }

        private void ItemAddUI_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void lbl_Nombre_Click(object sender, EventArgs e)
        {

        }

        private void TiempoElaboracion_Click(object sender, EventArgs e)
        {

        }

        private void PreciodeVenta_Click(object sender, EventArgs e)
        {

        }

        private void txt_Nombre_TextChanged(object sender, EventArgs e)
        {

        }

       
        public ItemController Controller
        {
            get
            {
                return _controller;
            }
        }
        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private bool validacion_Campos()
        {
            bool ok = true;
            float num; int num1; 
            if (txt_Nombre.Text == "")
            {
                ok = false;
                errorProvider1.SetError(txt_Nombre, "Ingrese el nombre");
            }
            if (txt_TiempoElaboracion.Text == "" )
            {
                ok = false;
                errorProvider1.SetError(txt_TiempoElaboracion, "Ingrese el Tiempo");
            }
            /*if (txt_GradoAlc.Text == "" || (!int.TryParse(txt_GradoAlc.Text, out num1)))
            {
                ok = false;
                errorProvider1.SetError(txt_GradoAlc, "Ingrese las Calorias");
            }*/
            if (txt_Calorias.Text == "")
            {
                ok = false;
                errorProvider1.SetError(txt_Calorias, "Ingrese las calorias");
            }
            if (txt_PrecioElaboracion.Text == "")
            {
                ok = false;
                errorProvider1.SetError(txt_PrecioElaboracion, "Ingrese el Precio");
            }
        

            if (txt_Descripcion.Text == "")
            {
                ok = false;
                errorProvider1.SetError(txt_Descripcion, "Ingrese la descripcion");
            }
   
            if (txt_tipo.Text == "")
            {
                ok = false;
                errorProvider1.SetError(txt_tipo, "Ingrese el tipo");
            }
            /* if (txt_CiAdmin.Text == ""  || (!int.TryParse(txt_CiAdmin.Text, out num1)))
             {
                 ok = false;
                 errorProvider1.SetError(txt_CiAdmin, "Ingrese el ci");
             }*/
            if ((!int.TryParse(txt_TiempoElaboracion.Text, out num1)) ){ ok = false;}
            else{            
                    if (float.Parse(txt_TiempoElaboracion.Text) < 0)
                    {
                        ok = false;
                    }
            }
            if((!int.TryParse(txt_Calorias.Text, out num1)) )
            {
                ok = false;
            }
            else
            {
                if (Convert.ToInt32(txt_Calorias.Text) < 0)
                {
                    ok = false;
                }
            }
            if ((!float.TryParse(txt_PrecioElaboracion.Text, out num)))
            {
                ok = false;
            }
            else
            {
                if ((Convert.ToInt32(txt_PrecioElaboracion.Text) < 0))
                {
                    ok = false;
                }
            }
          
            float b;

            if (!float.TryParse(txt_PrecioElaboracion.Text, out b))
            {
                errorProvider1.SetError(txt_PrecioElaboracion, "Ingrese valor en numeros");
            }
            else
            {
                if (float.Parse(txt_PrecioElaboracion.Text) < 0)
                {
                    errorProvider1.SetError(txt_PrecioElaboracion, "No se puede ingresar valores negativos");
                }
                else
                {
                    errorProvider1.SetError(txt_PrecioElaboracion, "");
                }

            }
            int a;
            if (!int.TryParse(txt_TiempoElaboracion.Text, out a))
            {
                errorProvider1.SetError(txt_TiempoElaboracion, "Ingrese valor en numeros");
            }
            else
            {
                if (Convert.ToInt32(txt_TiempoElaboracion.Text) < 0)
                {
                    errorProvider1.SetError(txt_TiempoElaboracion, "No se puede ingresar valores negativos");
                }
                else
                {
                    errorProvider1.SetError(txt_TiempoElaboracion, "");
                }
            }
            
            if (!int.TryParse(txt_Calorias.Text, out a))
            {
                errorProvider1.SetError(txt_Calorias, "Ingrese valor en numeros");
            }
            else
            {
                if (Convert.ToInt32(txt_Calorias.Text) < 0)
                {
                    errorProvider1.SetError(txt_Calorias, "No se puede ingresar valores negativos");
                }
                else
                {
                    errorProvider1.SetError(txt_Calorias, "");
                }
            }





            return ok;
        }
        private void txt_PrecioElaboracion_Validating(object sender, CancelEventArgs e)
        {
            float num;
            if (!float.TryParse(txt_PrecioElaboracion.Text, out num))
            {
                errorProvider1.SetError(txt_PrecioElaboracion, "Ingrese valor en numeros");
            }
            else
            {
                if (float.Parse(txt_PrecioElaboracion.Text)<0)
                {
                    errorProvider1.SetError(txt_PrecioElaboracion, "No se puede ingresar valores negativos");
                }
                else
                {
                    errorProvider1.SetError(txt_PrecioElaboracion, "");
                }
                
            }
            
        }

        private void txt_TiempoElaboracion_Validating(object sender, CancelEventArgs e)
        {
            int num;
            if (!int.TryParse(txt_TiempoElaboracion.Text, out num))
            {
                errorProvider1.SetError(txt_TiempoElaboracion, "Ingrese valor en numeros");
            }
            else
            {
                if (Convert.ToInt32(txt_TiempoElaboracion.Text)<0)
                {
                    errorProvider1.SetError(txt_TiempoElaboracion, "No se puede ingresar valores negativos");
                }
                else
                {
                    errorProvider1.SetError(txt_TiempoElaboracion, "");
                }
            }
           
        }

        private void txt_Calorias_Validating(object sender, CancelEventArgs e)
        {
            int num;
            if (!int.TryParse(txt_Calorias.Text, out num))
            {
                errorProvider1.SetError(txt_Calorias, "Ingrese valor en numeros");
            }
            else
            {
                if (Convert.ToInt32(txt_Calorias.Text)<0)
                {
                    errorProvider1.SetError(txt_Calorias, "No se puede ingresar valores negativos");
                }
                else
                {
                    errorProvider1.SetError(txt_Calorias, "");
                }
            }
        }
       /* private void txt_GradoAlc_Validating(object sender, CancelEventArgs e)
        {
            int num;
            if (!int.TryParse(txt_GradoAlc.Text, out num))
            {
                errorProvider1.SetError(txt_GradoAlc, "Ingrese valor en numeros");
            }
            else
            {
                errorProvider1.SetError(txt_GradoAlc, "");
            }
        }*/

        private void txt_CiAdmin_Validating(object sender, CancelEventArgs e)
        {
            int num;
           /* if (!int.TryParse(txt_CiAdmin.Text, out num))
            {
                errorProvider1.SetError(txt_CiAdmin, "Ingrese valor en numeros");
            }
            else
            {
                errorProvider1.SetError(txt_CiAdmin, "");
            }*/
        }
        private void BorrarMensajeError()
        {
            errorProvider1.SetError(txt_Nombre, "");
            errorProvider1.SetError(txt_PrecioElaboracion, "");
            errorProvider1.SetError(txt_TiempoElaboracion, "");
            errorProvider1.SetError(txt_Calorias, "");
          //  errorProvider1.SetError(txt_GradoAlc, "");
            errorProvider1.SetError(txt_Descripcion, "");
            errorProvider1.SetError(txt_tipo, "");
            //errorProvider1.SetError(txt_CiAdmin, "");
        }
        
               
           
        
        private void btn_Aceptar_Click(object sender, EventArgs e)
        {
            BorrarMensajeError();
            if (validacion_Campos())
            {
               
                string nom = txt_Nombre.Text;
                //
                if (Controller.ItemCodeExists(nom))
                {
                    MessageBox.Show("El nombre de la receta ya exister", "Error");
                    return;
                }
                //Convert.ToInt32(this.txt_GradoAlc.Text)
                //Convert.ToInt32(this.txt_CiAdmin.Text)
                var result = Controller.InsertItem(txt_Nombre.Text.Trim(), Convert.ToInt32(this.txt_TiempoElaboracion.Text), 0, Convert.ToInt32(this.txt_Calorias.Text), float.Parse(this.txt_PrecioElaboracion.Text), txt_Descripcion.Text.Trim(),txt_tipo.Text.Trim(),123);
                //var result = Controller.InsertItem(txt_Nombre.Text.ToString(), 12,12,12, 12.222,"text","tipo",000);
                if (result > 0)
                {
                    MessageBox.Show("Guardo Exitosamente");
                }
                else
                {
                    MessageBox.Show("Hubo problemas en el registro", "Error");
                }
                this.Close();
            }
            else
            {
                MessageBox.Show("Datos incorrectos. Por favor, verifíquelos", "Mensaje");
            }

            
        }

        private void lbl_Admi_Click(object sender, EventArgs e)
        {

        }

        private void txt_CiAdmin_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_Descripcion_TextChanged(object sender, EventArgs e)
        {

        }

      


        private void txt_Calorias_TextChanged(object sender, EventArgs e)
        {

        }

        private void lbl_PrecioElaboracion_Click(object sender, EventArgs e)
        {

        }

        private void txt_PrecioElaboracion_TextChanged(object sender, EventArgs e)
        {

        }

       

        private void txt_Descripcion_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void txt_TiempoElaboracion_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
