﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrearNuevoMenu.Controller;
using CrearNuevoMenu.Mapper;
namespace CrearNuevoMenu.UI 
{

    public partial class InsumoAddUI : Form
    {
        private InsumoControlador _controller;
        string codeItem;
        public InsumoAddUI(string code)
        {
            InitializeComponent();

            codeItem = code;
            _controller = new InsumoControlador(codeItem);
        }
     
        public InsumoControlador Controller
        {
            get
            {
                return _controller;
            }
        }
        private void InsumoAddUI_Load(object sender, EventArgs e)
        {
            SetInsumoGrid();

        }

        private void SetInsumoGrid()
        {
            dbd_insumos.AutoGenerateColumns = false;



            AddColumnsToItemGrid();

            RebindInsumoGrid();



        }
        private void RebindInsumoGrid()
        {
            var source = new BindingSource();
            var itemsList = Controller.InsumosSimpleConditional;
            //  dgInsumo.DataSource = itemsList;
            source.DataSource = itemsList;

            dbd_insumos.DataSource = source;
        }
        private void AddColumnsToItemGrid()
        {
            DataGridViewColumn dataGridViewColumn;

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "codInsumo";
            dataGridViewColumn.HeaderText = "CodInsumo";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "CodInsumo";
            //dataGridViewColumn.Visible = false;
            dbd_insumos.Columns.Add(dataGridViewColumn);


            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "nombre";
            dataGridViewColumn.HeaderText = "Nombre";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "Nombre";
            dataGridViewColumn.ReadOnly = true;
            dbd_insumos.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "unidad";
            dataGridViewColumn.HeaderText = "Unidad";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "Unidad";
            dataGridViewColumn.ReadOnly = true;
            dbd_insumos.Columns.Add(dataGridViewColumn);



            var deleteButton = new DataGridViewButtonColumn();
            deleteButton.DataPropertyName = "Seleccione";
            deleteButton.Name = "dgSeleccioneBtn";
            deleteButton.HeaderText = "Seleccione";
            deleteButton.Text = "Seleccione";
            deleteButton.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            deleteButton.CellTemplate = new DataGridViewButtonCell();
            deleteButton.UseColumnTextForButtonValue = true;
            dbd_insumos.Columns.Add(deleteButton);


        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == dbd_insumos.NewRowIndex || e.RowIndex < 0)
                return;

            //Compruebe si el clic está en una columna específica
            if (e.ColumnIndex == dbd_insumos.Columns["dgSeleccioneBtn"].Index)
            {
                int index = e.RowIndex;
                string codeInsumo = dbd_insumos.Rows[index].Cells[0].Value.ToString();//codInsumo
                

                DatosInsumoUI insumos = new DatosInsumoUI(codeItem,codeInsumo);
                insumos.ShowDialog(this);

                dbd_insumos.DataSource = null;

                RebindInsumoGrid();
                insumos.Dispose();
                this.Close();



            }
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       
    }
}
