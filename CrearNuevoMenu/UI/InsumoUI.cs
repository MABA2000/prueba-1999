﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrearNuevoMenu.Controller;
using CrearNuevoMenu.Mapper;

namespace CrearNuevoMenu.UI 
{
    public partial class InsumoUI : Form
    {
        private InsumoControlador _controller;
        string codeitem;
        public InsumoUI()
        {
            InitializeComponent();
            _controller = new InsumoControlador();
            codeitem = "";
        }
        public InsumoUI(string code,string name)//CodItem
        {
            
            InitializeComponent();
            lbl_nombreItem.Text= name;
            codeitem = code;
            _controller = new InsumoControlador(code);
           
        }
        public InsumoControlador Controller
        {
            get
            {
                return _controller;
            }
        }
        private void InsumoUI_Load(object sender, EventArgs e)
        {
            SetInsumoGrid();

        }

        private void SetInsumoGrid() 
        {
            dgInsumo.AutoGenerateColumns = false;



            AddColumnsToItemGrid();

         
                RebindInsumoGrid();
         



        }
        private void RebindInsumoGrid()
        {
            var source = new BindingSource();
            var itemsList = Controller.ListaDeInsumos;
          //  dgInsumo.DataSource = itemsList;
             source.DataSource = itemsList;

             dgInsumo.DataSource = source;
        }
        private void RebindInsumoGridDeItem() 
        {
            var source = new BindingSource();
          
            var itemsList = Controller.GetInsumodeItem; 
            //  dgInsumo.DataSource = itemsList;
            source.DataSource = itemsList;

            dgInsumo.DataSource = source;
        }
        private void AddColumnsToItemGrid()
        {
            DataGridViewColumn dataGridViewColumn;

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "codInsumo";
            dataGridViewColumn.HeaderText = "CodInsumo";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "CodInsumo";
            dgInsumo.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "nombre";
            dataGridViewColumn.HeaderText = "Nombre";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "Nombre";
            dataGridViewColumn.ReadOnly = true;
            dgInsumo.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "unidad";
            dataGridViewColumn.HeaderText = "Unidad";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "Unidad";
            dataGridViewColumn.ReadOnly = true;
            dgInsumo.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "Medida";
            dataGridViewColumn.HeaderText = "Medida";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "Medida";
            dataGridViewColumn.ReadOnly = true;
            dgInsumo.Columns.Add(dataGridViewColumn);

            dataGridViewColumn = new DataGridViewColumn();
            dataGridViewColumn.DataPropertyName = "cantidad";
            dataGridViewColumn.HeaderText = "Cantidad";
            dataGridViewColumn.CellTemplate = new DataGridViewTextBoxCell();
            dataGridViewColumn.Name = "Cantidad";
            dataGridViewColumn.ReadOnly = true;
            dgInsumo.Columns.Add(dataGridViewColumn);

            var deleteButton = new DataGridViewButtonColumn();
            deleteButton.DataPropertyName = "Delete";
            deleteButton.Name = "dgItemDeleteBtn";
            deleteButton.HeaderText = "Delete";
            deleteButton.Text = "Delete";
            deleteButton.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            deleteButton.CellTemplate = new DataGridViewButtonCell();
            deleteButton.UseColumnTextForButtonValue = true;
            dgInsumo.Columns.Add(deleteButton);


        }
        private void dgItem_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //if click is on new row or header row
            if (e.RowIndex == dgInsumo.NewRowIndex || e.RowIndex < 0)
                return;

            //Check if click is on specific column 
            if (e.ColumnIndex == dgInsumo.Columns["dgItemDeleteBtn"].Index)
            {
                //Put some logic here, for example to remove row from your binding list.
                int index = e.RowIndex;

                DialogResult dialogResult = MessageBox.Show("Esta seguro de eliminar  " + Controller.ListaDeInsumos[index].nombre + " ?", "Confirmacion", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    string message;
                    int recordsAffected = Controller.borrarInsumoDelIndice(index, out message);

                    if (recordsAffected == 1)
                    {
                        dgInsumo.DataSource = null;
                        RebindInsumoGrid();
                        MessageBox.Show("Se elimino exitosamente");
                    }
                    else
                    {
                        MessageBox.Show("Se produjo un error al eliminar el registro.:" + message, "Error");
                    }
                }
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //si el clic está en una nueva fila o fila de encabezado
            if (e.RowIndex == dgInsumo.NewRowIndex || e.RowIndex < 0)
                return;

            //Compruebe si el clic está en una columna específica
            if (e.ColumnIndex == dgInsumo.Columns["dgItemDeleteBtn"].Index)
            {
                //Ponga algo de lógica aquí, por ejemplo, para eliminar la fila de su lista de enlaces.
                int index = e.RowIndex;

                DialogResult dialogResult = MessageBox.Show("Esta seguro de eliminar  " + Controller.ListaDeInsumos[index].nombre + " ?", "Confirmacion", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    string message;
                    int recordsAffected = Controller.BorrarInsumoDeUnItem(index, out message);

                    if (recordsAffected == 1)
                    {
                        dgInsumo.DataSource = null;
                        RebindInsumoGrid();
                        MessageBox.Show("Se elimino exitosamente");
                    }
                    else
                    {
                        MessageBox.Show("se elimino.." + message, "Mensaje");
                    }
                }
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
          
            InsumoAddUI frm = new InsumoAddUI(codeitem);
            
            
                      frm.ShowDialog(this);
             dgInsumo.DataSource = null;

           Controller.GetInsumos(codeitem);
            RebindInsumoGrid();
            frm.Dispose();
             

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lbl_nombreItem_Click(object sender, EventArgs e)
        {
          
        }
    }
}
