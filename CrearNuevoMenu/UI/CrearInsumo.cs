﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrearNuevoMenu.Controller;

namespace CrearNuevoMenu.UI
{
    public partial class CrearInsumo : Form
    {
        CrearInsumoControlador crearInsumoControlador;
        public CrearInsumo()
        {
            InitializeComponent();
            crearInsumoControlador = new CrearInsumoControlador();
        }

        private void guardarBtn_Click(object sender, EventArgs e)
        {
            string nombre = nombreTxt.Text;
            string unidad = unidadTxt.Text;
            if (string.IsNullOrEmpty(nombre) || string.IsNullOrEmpty(unidad))
            {
                MessageBox.Show("Es obligatorio que los campos esten llenos.");
                return;
            }
            if (!crearInsumoControlador.existeInsumo(nombre))
            {
                var result = crearInsumoControlador.InsertarInsumo(-1, nombre, 0, unidad);
                if (result > 0)
                {
                    MessageBox.Show("El ítem fue añadido al menú!", "Mensaje");
                    this.Close();
                    this.Dispose();

                }
                else
                {
                    MessageBox.Show("Hubo un problema guardando los datos.", "Error");
                }
            }
            else
                MessageBox.Show("Ya existe un insumo con el mismo nombre en el sistema.", "Error");



        }

        private void cancelarBtn_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void CrearInsumo_Load(object sender, EventArgs e)
        {

        }
    }
}
