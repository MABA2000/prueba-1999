﻿namespace CrearNuevoMenu.IngresarMenu
{
    partial class AdministrarMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdministrarMenu));
            this.nombre = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.fecha = new System.Windows.Forms.TextBox();
            this.cantidad = new System.Windows.Forms.TextBox();
            this.descripcion = new System.Windows.Forms.RichTextBox();
            this.lblfecha = new System.Windows.Forms.Label();
            this.lbldescripcion = new System.Windows.Forms.Label();
            this.lblcantidad = new System.Windows.Forms.Label();
            this.cbvisible = new System.Windows.Forms.CheckBox();
            this.volver = new System.Windows.Forms.Button();
            this.itemsDelMenuDG = new System.Windows.Forms.DataGridView();
            this.guardarBtn = new System.Windows.Forms.Button();
            this.cancelarBtn = new System.Windows.Forms.Button();
            this.añadirBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.itemsDelMenuDG)).BeginInit();
            this.SuspendLayout();
            // 
            // nombre
            // 
            this.nombre.BackColor = System.Drawing.Color.Green;
            this.nombre.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nombre.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nombre.Location = new System.Drawing.Point(222, 36);
            this.nombre.Name = "nombre";
            this.nombre.ReadOnly = true;
            this.nombre.Size = new System.Drawing.Size(198, 21);
            this.nombre.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // fecha
            // 
            this.fecha.BackColor = System.Drawing.Color.Green;
            this.fecha.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.fecha.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fecha.Location = new System.Drawing.Point(651, 193);
            this.fecha.Name = "fecha";
            this.fecha.Size = new System.Drawing.Size(100, 21);
            this.fecha.TabIndex = 2;
            // 
            // cantidad
            // 
            this.cantidad.BackColor = System.Drawing.Color.Green;
            this.cantidad.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.cantidad.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cantidad.Location = new System.Drawing.Point(374, 193);
            this.cantidad.Name = "cantidad";
            this.cantidad.ReadOnly = true;
            this.cantidad.Size = new System.Drawing.Size(35, 21);
            this.cantidad.TabIndex = 3;
            // 
            // descripcion
            // 
            this.descripcion.BackColor = System.Drawing.Color.Green;
            this.descripcion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.descripcion.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.descripcion.Location = new System.Drawing.Point(262, 110);
            this.descripcion.Name = "descripcion";
            this.descripcion.ReadOnly = true;
            this.descripcion.Size = new System.Drawing.Size(481, 75);
            this.descripcion.TabIndex = 4;
            this.descripcion.Text = "";
            // 
            // lblfecha
            // 
            this.lblfecha.AutoSize = true;
            this.lblfecha.BackColor = System.Drawing.Color.Green;
            this.lblfecha.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfecha.Location = new System.Drawing.Point(484, 193);
            this.lblfecha.Name = "lblfecha";
            this.lblfecha.Size = new System.Drawing.Size(161, 21);
            this.lblfecha.TabIndex = 6;
            this.lblfecha.Text = "Fecha de elaboración:";
            // 
            // lbldescripcion
            // 
            this.lbldescripcion.AutoSize = true;
            this.lbldescripcion.BackColor = System.Drawing.Color.Green;
            this.lbldescripcion.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldescripcion.Location = new System.Drawing.Point(160, 106);
            this.lbldescripcion.Name = "lbldescripcion";
            this.lbldescripcion.Size = new System.Drawing.Size(98, 21);
            this.lbldescripcion.TabIndex = 7;
            this.lbldescripcion.Text = "Descripción:";
            // 
            // lblcantidad
            // 
            this.lblcantidad.AutoSize = true;
            this.lblcantidad.BackColor = System.Drawing.Color.Green;
            this.lblcantidad.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcantidad.Location = new System.Drawing.Point(160, 193);
            this.lblcantidad.Name = "lblcantidad";
            this.lblcantidad.Size = new System.Drawing.Size(209, 21);
            this.lblcantidad.TabIndex = 8;
            this.lblcantidad.Text = "Cantidad de ítems del menú:";
            // 
            // cbvisible
            // 
            this.cbvisible.AutoSize = true;
            this.cbvisible.BackColor = System.Drawing.Color.Green;
            this.cbvisible.Enabled = false;
            this.cbvisible.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbvisible.Location = new System.Drawing.Point(683, 38);
            this.cbvisible.Name = "cbvisible";
            this.cbvisible.Size = new System.Drawing.Size(79, 25);
            this.cbvisible.TabIndex = 10;
            this.cbvisible.Text = "Visible";
            this.cbvisible.UseVisualStyleBackColor = false;
            // 
            // volver
            // 
            this.volver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.volver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.volver.Location = new System.Drawing.Point(167, 468);
            this.volver.Name = "volver";
            this.volver.Size = new System.Drawing.Size(113, 42);
            this.volver.TabIndex = 0;
            this.volver.Text = "VOLVER";
            this.volver.UseVisualStyleBackColor = false;
            this.volver.Click += new System.EventHandler(this.volver_Click);
            // 
            // itemsDelMenuDG
            // 
            this.itemsDelMenuDG.AllowUserToAddRows = false;
            this.itemsDelMenuDG.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.itemsDelMenuDG.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.itemsDelMenuDG.Location = new System.Drawing.Point(164, 247);
            this.itemsDelMenuDG.Name = "itemsDelMenuDG";
            this.itemsDelMenuDG.Size = new System.Drawing.Size(587, 197);
            this.itemsDelMenuDG.TabIndex = 12;
            this.itemsDelMenuDG.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.itemsDelMenuDG_CellClick);
            this.itemsDelMenuDG.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.itemsDelMenuDG_CellEndEdit);
            this.itemsDelMenuDG.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.itemsDelMenuDG_CellValidating);
            // 
            // guardarBtn
            // 
            this.guardarBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.guardarBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.guardarBtn.Location = new System.Drawing.Point(341, 468);
            this.guardarBtn.Name = "guardarBtn";
            this.guardarBtn.Size = new System.Drawing.Size(113, 42);
            this.guardarBtn.TabIndex = 1;
            this.guardarBtn.Text = "GUARDAR";
            this.guardarBtn.UseVisualStyleBackColor = false;
            this.guardarBtn.Click += new System.EventHandler(this.guardarBtn_Click);
            // 
            // cancelarBtn
            // 
            this.cancelarBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.cancelarBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarBtn.Location = new System.Drawing.Point(488, 468);
            this.cancelarBtn.Name = "cancelarBtn";
            this.cancelarBtn.Size = new System.Drawing.Size(113, 42);
            this.cancelarBtn.TabIndex = 2;
            this.cancelarBtn.Text = "ACTUALIZAR TABLA";
            this.cancelarBtn.UseVisualStyleBackColor = false;
            this.cancelarBtn.Click += new System.EventHandler(this.cancelarBtn_Click);
            // 
            // añadirBtn
            // 
            this.añadirBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.añadirBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.añadirBtn.Location = new System.Drawing.Point(639, 468);
            this.añadirBtn.Name = "añadirBtn";
            this.añadirBtn.Size = new System.Drawing.Size(136, 42);
            this.añadirBtn.TabIndex = 3;
            this.añadirBtn.Text = "AÑADIR ITEM AL MENU";
            this.añadirBtn.UseVisualStyleBackColor = false;
            this.añadirBtn.Click += new System.EventHandler(this.añadirBtn_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Gold;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(164, 216);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(587, 31);
            this.label1.TabIndex = 16;
            this.label1.Text = "ITÉMS DEL MENÚ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Green;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(160, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 21);
            this.label2.TabIndex = 17;
            this.label2.Text = "Menú:";
            // 
            // AdministrarMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.ClientSize = new System.Drawing.Size(938, 661);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.añadirBtn);
            this.Controls.Add(this.cancelarBtn);
            this.Controls.Add(this.guardarBtn);
            this.Controls.Add(this.itemsDelMenuDG);
            this.Controls.Add(this.volver);
            this.Controls.Add(this.cbvisible);
            this.Controls.Add(this.lblcantidad);
            this.Controls.Add(this.lbldescripcion);
            this.Controls.Add(this.lblfecha);
            this.Controls.Add(this.descripcion);
            this.Controls.Add(this.cantidad);
            this.Controls.Add(this.fecha);
            this.Controls.Add(this.nombre);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AdministrarMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AdministrarMenu";
            this.Load += new System.EventHandler(this.AdministrarMenu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.itemsDelMenuDG)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox nombre;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.TextBox fecha;
        private System.Windows.Forms.TextBox cantidad;
        private System.Windows.Forms.RichTextBox descripcion;
        private System.Windows.Forms.Label lblfecha;
        private System.Windows.Forms.Label lbldescripcion;
        private System.Windows.Forms.Label lblcantidad;
        private System.Windows.Forms.CheckBox cbvisible;
        private System.Windows.Forms.Button volver;
        private System.Windows.Forms.DataGridView itemsDelMenuDG;
        private System.Windows.Forms.Button guardarBtn;
        private System.Windows.Forms.Button cancelarBtn;
        private System.Windows.Forms.Button añadirBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}