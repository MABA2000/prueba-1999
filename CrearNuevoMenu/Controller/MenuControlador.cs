﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Mapper;
using CrearNuevoMenu.Model;


namespace CrearNuevoMenu.Controller
{
    public class MenuControlador
    {
        private DataAccess.DataAccess _dataAccess;
        private MapperMenu mapperMenu;
        private List<Menu> menus;
        private MapperMenu Mapper
        {
            get { return mapperMenu; }
        }
        public MenuControlador()
        {
            string constr = GlobalData.GlobalData.connectionString;

            _dataAccess = new DataAccess.DataAccess(constr);
            mapperMenu = new MapperMenu(_dataAccess);

            conseguirMenus();
        }
        public List<Menu> ListaDeMenus
        {
            get { return menus; }
        }

        public void conseguirMenus()
        {
            menus = Mapper.conseguirMenusList();
        }
        public int InsertarMenu(int id, string nom, DateTime fec, string desc, bool vis)
        {
            var men = new Menu(Mapper, _dataAccess, id, nom, fec, desc, vis);
            return men.Persists();
        }
        public int borrarMenuDelIndice(int index, out string message)
        {
            int recordsAffected = Mapper.borrarMenu(menus[index], out message);
            if (recordsAffected == 1)
            {
                menus.RemoveAt(index);
            }
            return recordsAffected;
        }
        public void conseguirMenuDelIndice(int index, out string nom, out DateTime fec,
            out string desc, out bool vis, out int idMenu)
        {
            nom = menus[index].nombre;
            fec = menus[index].conseguirFecha();
            desc = menus[index].descripcion;
            idMenu= menus[index].Id;
            //cant = menus[index].cantidad;
            vis = menus[index].visible;
        }
        public int guardarCambiosDeMenus(out string mensaje)
        {
            var menusModificados = menus.Where(x => x.IsDirty).ToList();
            string outError = "";
            mensaje = "";
            int cont = 0;
            foreach (var men in menusModificados)
            {
                int filaAfectada = 0;
                filaAfectada = Mapper.actualizarMenu(men, out outError);
                if (filaAfectada < 1)
                {
                    mensaje = mensaje + " Error actualizar " + men.Id + " : " + outError;
                }


                cont += filaAfectada;
            }
            return cont;
        }
    }

}
