﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Mapper;
using CrearNuevoMenu.Model;
namespace CrearNuevoMenu.Controller
{
    public class OrdenControlador
    {
        private DataAccess.DataAccess _dataAccess;
        private MapperOrden mapperOrden;
        private List<Orden> ordenes;
        private MapperOrden Mapper
        {
            get { return mapperOrden; }
        }
        public OrdenControlador()
        {
            string constr = GlobalData.GlobalData.connectionString;

            _dataAccess = new DataAccess.DataAccess(constr);
            mapperOrden = new MapperOrden(_dataAccess);
            conseguirOrdenes();
        }
        public List<Orden> ListaDeOrdenes
        {
            get { return ordenes; }
        }

        public void conseguirOrdenes()
        {
            ordenes = Mapper.conseguirOrdenesList();
        }
        public void conseguirOrdenes(int numM)
        {
            ordenes = Mapper.conseguirOrdenesList(numM);
        }
        public int InsertarOrden(int cod,
            bool estado, int num, DateTime hora)
        {
            var ord = new Orden(Mapper, _dataAccess, cod, estado, num, hora);
            return ord.Persists();
        }
        public int borrarMenuDelIndice(int index, out string message)
        {
            int filaAfectada= Mapper.borrarOrden(ordenes[index], out message);
            if (filaAfectada == 1)
            {
                ordenes.RemoveAt(index);
            }
            return filaAfectada;
        }
        public int guardarCambiosDeOrdenes(out string mensaje)
        {
            var ordenesModificados = ordenes.Where(x => x.estaLimpio).ToList();
            string outError = "";
            mensaje = "";
            int cont = 0;
            foreach (var ord in ordenesModificados)
            {
                int filaAfectada = 0;
                filaAfectada = Mapper.actualizarOrdenes(ord, out outError);
                if (filaAfectada < 1)
                {
                    mensaje = mensaje + " Error actualizar " + ord.codOrden + " : " + outError;
                }
                cont += filaAfectada;
            }
            return cont;
        }
    }
}
