﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Mapper;
using CrearNuevoMenu.Model;
namespace CrearNuevoMenu.Controller
{
    class FacturaControlador
    {
        private DataAccess.DataAccess _dataAccess;
        private MapperFactura mapperFactura;
        private List<Factura> facturas;
        private MapperFactura Mapper
        {
            get { return mapperFactura; }
        }
        public FacturaControlador()
        {
            string constr = GlobalData.GlobalData.connectionString;
            _dataAccess = new DataAccess.DataAccess(constr);
            mapperFactura = new MapperFactura(_dataAccess);

            conseguirFacturas();
        }
        public List<Factura> ListaDeFacturas
        {
            get { return facturas; }
        }

        public void conseguirFacturas()
        {
            facturas = Mapper.conseguirFacturasList();
        }
        public int InsertarFactura(DateTime hora, DateTime fecha, int ciCli, int codOrd)
        {
            var factura = new Factura(Mapper, _dataAccess, 0, hora, fecha, ciCli, codOrd);
            return factura.Persists();
        }
        public int borrarFacturaDelIndice(int index, out string message)
        {
            int recordsAffected = Mapper.borrarFactura(facturas[index], out message);
            if (recordsAffected == 1)
            {
                facturas.RemoveAt(index);
            }
            return recordsAffected;
        }
        public int guardarCambiosDeFacturas(out string mensaje)
        {
            var facturasModificadas = facturas.Where(x => x.huboCambios).ToList();
            string outError = "";
            mensaje = "";
            int cont = 0;
            foreach (var fact in facturasModificadas)
            {
                int filaAfectada = 0;
                filaAfectada = Mapper.actualizarFactura(fact, out outError);
                if (filaAfectada < 1)
                {
                    mensaje = mensaje + " Error actualizar " + fact.codFac + " : " + outError;
                }
                cont += filaAfectada;
            }
            return cont;
        }
    }
}
