﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Mapper;
using CrearNuevoMenu.Model;
using CrearNuevoMenu.GlobalData;

namespace CrearNuevoMenu.Controller
{
    public class InsumoControlador
    {
        private DataAccess.DataAccess _dataAccess;
        private MapperInsumo mapperInsumo;
        private List<string> nombreDeInsumos;
        private List<Insumo> Insumos;
        string codeItem;
        private MapperInsumo Mapper
        {
            get { return mapperInsumo; }
        }
        public InsumoControlador()
        {
            string constr = GlobalData.GlobalData.connectionString;

            _dataAccess = new DataAccess.DataAccess(constr);
            mapperInsumo = new MapperInsumo(_dataAccess);
            conseguirNombresDeInsumos();
            conseguirInsumos();
        }
        public InsumoControlador(string code) 
        {
            string constr = GlobalData.GlobalData.connectionString;

            _dataAccess = new DataAccess.DataAccess(constr);
            mapperInsumo = new MapperInsumo(_dataAccess);

            codeItem = code;
            GetInsumos(code);
        }
        public void GetInsumos(string code)//SON LOS INSUMOS DE UN ITEM
        {
            Insumos = Mapper.GetAllInsumodeUnItem(code);
        }
        public List<Insumo> ListaDeInsumos
        {
            get { return Insumos; }
        }
        public List<Insumo> InsumosSimpleConditional//muestra insumos que no estan en un item
        {
            get { return mapperInsumo.GetInsumoSimpleConditional(codeItem); }
        }
        public List<Insumo> GetInsumodeItem //insumos de un item
        {
            get { return mapperInsumo.GetAllInsumosDelItemList(); }
        }
        public void conseguirInsumos()
        {
            Insumos = Mapper.conseguirInsumosList();
        }

        public void conseguirNombresDeInsumos()
        {
            nombreDeInsumos = Mapper.conseguirNombresDeInsumosList();
        }

        public int borrarInsumoDelIndice(int index, out string message)
        {
            int recordsAffected = Mapper.borrarInsumo(Insumos[index], out message);
            if (recordsAffected == 1)
            {
                Insumos.RemoveAt(index);
            }
            return recordsAffected;
        }
        public int BorrarInsumoDeUnItem(int index, out string message)
        { 
            int recordsAffected = Mapper.DeleteItem(Insumos[index], out message);
            if (recordsAffected == 1)
            {
                Insumos.RemoveAt(index);
            }
            return recordsAffected;
        }
        public int InsertInsumo(int CodItem, int CodInsumo, string Nombre, string unidad, double Medida, int Cantidad)
        {
            var item = new Insumo(Mapper, _dataAccess, CodInsumo, CodItem, Nombre, unidad, Medida, Cantidad);

            return item.Persists();
        }
        
        public int InsertarInsumoAItem(int CodItem, int CodInsumo, string Nombre, string unidad, double Medida, int Cantidad)
        { 
            var item = new Insumo(Mapper, _dataAccess, CodInsumo, CodItem, Nombre, unidad, Medida, Cantidad);

            return item.Persists2();
        }
        public void conseguirInsumoDelIndice(int index, out int codInsumo, out string nombre, out float cantidad, out string unidad)
        {
            codInsumo = Insumos[index].codInsumo;
            nombre = Insumos[index].nombre;
            cantidad = Insumos[index].cantidad;
            unidad = Insumos[index].unidad;
        }
        public int guardarInsumoDeMenuModificado(out string message)
        {
            var airportsModified = Insumos.Where(x => x.IsDirty).ToList();
            string outError;

            message = "";

            int count = 0;
            foreach (var insu in airportsModified)
            {
                int recordsAffected = Mapper.ModificiarInsumo(insu, out outError);
                if (recordsAffected < 1)
                {
                    message = message + "Error Modificando " + insu.nombre + " : " + outError;
                }
                count += recordsAffected;
            }

            return count;
        }
        public bool comprobarQueNoHayPunto(string num)
        {
            char[] numero = num.ToArray();
            for (int i = 0; i < numero.Length; i++)
            {
                if (numero[i] == '.')
                    return false;
            }
            return true;
        }
        public List<string> ListaNombreInsum
        {
            get { return nombreDeInsumos; }
        }
        public int devolverIDDeNombre(string nombre)
        {
            for(int i =0;i< Insumos.Count; i++)
            {
                if (Insumos[i].nombre == nombre)
                {
                    return Insumos[i].codInsumo;
                }
            }
            return -1;
        }

    }
}
