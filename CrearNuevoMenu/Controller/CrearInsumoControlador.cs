﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Mapper;
using CrearNuevoMenu.Model;
using CrearNuevoMenu.GlobalData;

namespace CrearNuevoMenu.Controller
{
    public class CrearInsumoControlador
    {
        private DataAccess.DataAccess _dataAccess;
        private MapperInsumo mapperInsumo;
        private List<Insumo> Insumos;
        private MapperInsumo Mapper
        {
            get { return mapperInsumo; }
        }
        public CrearInsumoControlador()
        {
            string constr = GlobalData.GlobalData.connectionString;

            _dataAccess = new DataAccess.DataAccess(constr);
            mapperInsumo = new MapperInsumo(_dataAccess);

            conseguirInsumos();
        }
        public bool existeInsumo(string nombre)
        {
            return mapperInsumo.existeInsumo(nombre);
        }
        public List<Insumo> ListaDeInsumos
        {
            get { return Insumos; }
        }

        public void conseguirInsumos()
        {
            Insumos = Mapper.conseguirInsumosList();
        }

        public int InsertarInsumo(int codInsumo, string nombre, float cantidad, string unidad)
        {
            var men = new Insumo(Mapper, _dataAccess, codInsumo, nombre, cantidad, unidad);
            return men.Persists();
        }
       
    }
}
