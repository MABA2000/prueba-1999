﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Mapper;
using CrearNuevoMenu.Model;


namespace CrearNuevoMenu.Controller
{
    class CredencialControlador
    {
        private DataAccess.DataAccess _dataAccess;
        private MapperCredencial mapperCredencial;
        private Credencial cred;
        private MapperCredencial Mapper
        {
            get { return mapperCredencial; }
        }
        public CredencialControlador()
        {
            string constr = GlobalData.GlobalData.connectionString;

            _dataAccess = new DataAccess.DataAccess(constr);
            mapperCredencial = new MapperCredencial(_dataAccess);
            cred = new Credencial();
        }
        public bool verificarCredencial(string ema, string cont, out string rol, out int ci)
        {
            cred.limpiar();
            cred.email = ema;
            cred.contraseña = cont;
            string error;
            cred = Mapper.buscarUsuario(cred, out error);
            rol = cred.asociacion;
            ci = cred.ci;
            return cred.encontrado;
        }
    }
}
