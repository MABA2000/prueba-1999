﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Mapper;
using CrearNuevoMenu.Model;
using CrearNuevoMenu.GlobalData;
using System.Data;

namespace CrearNuevoMenu.Controller
{
    public class EntregaControlador
    {
        private DataAccess.DataAccess _dataAccess;
        private MapperEntrega mapperEntrega;
        private List<Entrega> Entregas;
        private MapperEntrega Mapper
        {
            get { return mapperEntrega; }
        }
        public EntregaControlador()
        {
            string constr = GlobalData.GlobalData.connectionString;

            _dataAccess = new DataAccess.DataAccess(constr);
            mapperEntrega = new MapperEntrega(_dataAccess);

            conseguirEntregas();
        }
        public List<Entrega> ListaDeEntregas
        {
            get { return Entregas; }
        }

        public void conseguirEntregas()
        {
            Entregas = Mapper.conseguirEntregasList();
        }



        public int modificarEntrega(int codEntrega, int codInsumo, int codProveedor, float cantidad, float precioPorUnidad)
        {

            var men = new Entrega(Mapper, _dataAccess, codEntrega, codInsumo, codProveedor, precioPorUnidad, cantidad, DateTime.Today);
            return men.PersistsEditar();
        }
        public int InsertarEntrega(int codEntrega, int codInsumo, int codProveedor, float cantidad, float precioPorUnidad)
        {

            var men = new Entrega(Mapper, _dataAccess, codEntrega, codInsumo, codProveedor, precioPorUnidad, cantidad, DateTime.Today);
            return men.Persists();
        }

        public int borrarEntregaDelIndice(int index, out string message)
        {
            int recordsAffected = Mapper.borrarEntrega(Entregas[index], out message);
            if (recordsAffected == 1)
            {
                Entregas.RemoveAt(index);
            }
            return recordsAffected;
        }
        public void conseguirEntregaDelIndice(int index,out int codEntrega, out int codInsumo, out int codProveedor, out float cantidad, out float precioPorUnidad, out DateTime fechaDeEntrega)
        {
            codEntrega = Entregas[index].CodEntrega;
            codInsumo = Entregas[index].CodInsumo;
            codProveedor = Entregas[index].CIProveedor;
            cantidad = Entregas[index].Cantidad;
            precioPorUnidad = Entregas[index].PrecioUnidad;
            fechaDeEntrega = Entregas[index].fechaDeEntrega;
        }
        public int guardarEntregaDeMenuModificado(out string message)
        {
            var airportsModified = Entregas.Where(x => x.IsDirty).ToList();
            string outError;

            message = "";

            int count = 0;
            foreach (var insu in airportsModified)
            {
                int recordsAffected = Mapper.ModificiarEntrega(insu, out outError);
                if (recordsAffected < 1)
                {
                    message = message + "Error Modificando " + insu.CodEntrega + " : " + outError;
                }
                count += recordsAffected;
            }

            return count;
        }
        public DataView conseguirLasEntregasPorSentencia()
        {
            return mapperEntrega.conseguirLasEntregasPorSentencia();
        }
    }
}
