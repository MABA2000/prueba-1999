﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Mapper;
using CrearNuevoMenu.Model;
using System.Data;
using CrearNuevoMenu.GlobalData;

namespace CrearNuevoMenu.Controller
{
    public class adminMenuControlador
    {
        private DataAccess.DataAccess _dataAccess;
        private MapperItemDeMenu MapperItemDeMenu;
        private List<ItemDeMenu> itemsDeMenus;
        private MapperItemDeMenu Mapper
        {
            get { return MapperItemDeMenu; }
        }
        public adminMenuControlador(int idMenu)
        {
            string constr = GlobalData.GlobalData.connectionString;

            _dataAccess = new DataAccess.DataAccess(constr);
            MapperItemDeMenu = new MapperItemDeMenu(_dataAccess);

            conseguirItemsDeMenus(idMenu);
        }
        public List<ItemDeMenu> ListaDeItemsDeMenus
        {
            
            get { return itemsDeMenus; }
        }
        public int guardarItemDeMenuModificado(out string message)
        {
            var airportsModified = itemsDeMenus.Where(x => x.IsDirty).ToList();
            string outError;

            message = "";

            int count = 0;
            foreach (var item in airportsModified)
            {
                int recordsAffected = Mapper.ModificiarItemMenu(item, out outError);
                if (recordsAffected < 1)
                {
                    message = message + "Error Modificando: " + outError;
                }
                count += recordsAffected;
            }

            return count;
        }
        
        public void conseguirItemsDeMenus(int idMenu)
        {
            itemsDeMenus = Mapper.conseguirItemsDeMenuList(idMenu);
        }
        public int conseguirCantidadDeItmesDelMenu(int idMenu)
        {
            return Mapper.conseguirCantidadDeItemsDelMenu(idMenu);
        }
     

        public void conseguirItemsDeMenuDelIndice(int index, out int codItem, out int codMenu, out float precio, out DateTime fechaDeAdicion)
        {
            codItem = itemsDeMenus[index].codItem;
            codMenu = itemsDeMenus[index].codMenu;
            precio = itemsDeMenus[index].precio;
            fechaDeAdicion = itemsDeMenus[index].fechaDeAdicion;
        }
        public int eliminarItemDelMenuDelMenu(int index, out string message)
        {
            int recordsAffected = Mapper.eliminarItemDElMenu(itemsDeMenus[index], out message);
            if (recordsAffected == 1)
            {
                itemsDeMenus.RemoveAt(index);
            }
            return recordsAffected;
        }
        public bool comprobarQueNoHayPunto(string num)
        {
            char[] numero = num.ToArray();
            for (int i = 0; i < numero.Length; i++)
            {
                if (numero[i] == '.')
                    return false;
            }
            return true;
        }
        public bool existeItem(int codMenu, int codItem)
        {
            return MapperItemDeMenu.itemExisteEnMenu(codMenu, codItem);
        }
        public DataView listaPorSentencia(int idMenu)
        {
            return Mapper.conseguirLosItemsDeMenu(idMenu);
        }

    }
}
