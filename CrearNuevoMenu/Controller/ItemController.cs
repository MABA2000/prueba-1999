﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Mapper;
using CrearNuevoMenu.Model;
using CrearNuevoMenu.GlobalData;
namespace CrearNuevoMenu.Controller
{
    public class ItemController
    {
        private DataAccess.DataAccess _dataAccess;
        private  MapperItem _itemMapper;
        private List<Item> _items;

        private MapperItem Mapper
        {
            get { return _itemMapper; }
        }


        public ItemController()
        {
            string constr = GlobalData.GlobalData.connectionString; // aqui venia el connesc string

            _dataAccess = new DataAccess.DataAccess(constr);
            _itemMapper = new MapperItem(_dataAccess);

            GetItems();
        }

        public List<Item> Items
        {
            get { return _items; }
        }
        public void GetItems()
        {
            _items = Mapper.conseguirItemsList();
        }
        public int DeleteItemFromIndex(int index, out string message)
        {
            int recordsAffected = Mapper.DeleteItem(Items[index], out message);
            if (recordsAffected == 1)
            {
                Items.RemoveAt(index);
            }
            return recordsAffected;
        }
        public int SaveModifiedItems(out string message)
        {
            var itemsModified = Items.Where(x => x.IsDirty).ToList();
            string outError;

            message = "";

            int count = 0;
            foreach (var aipt in itemsModified)
            {
                int recordsAffected = Mapper.UpdateItem(aipt, out outError);
                if (recordsAffected < 1)
                {
                    message = message + "Error updating " + aipt.codItem + " : " + outError;
                }
                count += recordsAffected;
            }

            return count;
        }
        public bool ItemCodeExists(string code)
        {
            return Mapper.ItemCodeExists(code);
        }

        public int InsertItem(string Nombre, int TiempoElaboracion, int gradoAlc, int Calorias, float PrecioElaboracion, string Descripcion, string Tipo, int CIAdministrador)
        {
            var item = new Item(Mapper, _dataAccess, -1, Nombre, TiempoElaboracion, gradoAlc, Calorias, PrecioElaboracion, Descripcion, Tipo,123);
            return item.Persists();
        }


    }
}
