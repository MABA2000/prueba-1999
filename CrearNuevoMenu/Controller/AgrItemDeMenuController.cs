﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Mapper;
using CrearNuevoMenu.Model;
using CrearNuevoMenu.GlobalData;

namespace CrearNuevoMenu.Controller
{
    public class AgrItemDeMenuController

    {
        private DataAccess.DataAccess _dataAccess;
        private MapperItem MapperItem;
        private MapperItemDeMenu MAperItemDeMenu;
        private List<Item> itemsDeSistema;
        private MapperItem Mapper
        {
            get { return MapperItem; }
        }
        public AgrItemDeMenuController()
        {
            string constr = GlobalData.GlobalData.connectionString;

            _dataAccess = new DataAccess.DataAccess(constr);
            MapperItem = new MapperItem(_dataAccess);
            MAperItemDeMenu = new MapperItemDeMenu(_dataAccess);

            conseguiritemsDeSistema();
        }
        public List<Item> ListaDeitemsDeSistema
        {

            get { return itemsDeSistema; }
        }

        public void conseguiritemsDeSistema()
        {
            itemsDeSistema = Mapper.conseguirItemsList();
        }
        public int conseguirCantidadDeItmesDelSistema()
        {
            return Mapper.conseguirCantidadDeItemsDelSistema();
        }
        public int InsertarItemDeMenu(int codItem, int codMenu, float precio, DateTime fechaDeAdicion)
        {
            var ite = new ItemDeMenu(MAperItemDeMenu, _dataAccess,  codItem, codMenu, precio, fechaDeAdicion);
            return ite.Persists();
        }
        public int borrarItemsDeMenuDelIndice(int index, out string message)
        {
            int recordsAffected = Mapper.borrarItem(itemsDeSistema[index], out message);
            if (recordsAffected == 1)
            {
                itemsDeSistema.RemoveAt(index);
            }
            return recordsAffected;
        }

        public bool buscarItemEnListaDeItems(int index,  int idMenu,ref int codItem, ref string descripcion,  ref string tipo, ref string nombre)
        {
            codItem = itemsDeSistema[index].codItem;
            descripcion = itemsDeSistema[index].descripcion;
            tipo = itemsDeSistema[index].tipo;
            nombre = itemsDeSistema[index].nombre;
            List <ItemDeMenu> itemsDeMenu = MAperItemDeMenu.conseguirItemsDeMenuList(idMenu);
            for (int i=0;i< itemsDeMenu.Count; i++)
            {
                if (codItem == itemsDeMenu[i].codItem)
                {
                    return true;
                }

            }
            return false;

        }

        public bool comprobarQueNoHayPunto(string num)
        {
            char[] numero = num.ToArray();
            for(int i = 0; i < numero.Length; i++)
            {
                if (numero[i] == '.')
                    return false;
            }
            return true;
        }
        
    }
   
}
