﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Mapper;
using CrearNuevoMenu.Model;
namespace CrearNuevoMenu.Controller
{
    public class PersonalControlador
    {
        private DataAccess.DataAccess _dataAccess;
        private MapperPersonal mapperPersonal;
        private List<Personal> personales;
        private MapperPersonal Mapper
        {
            get { return mapperPersonal; }
        }
        public PersonalControlador()
        {
            string constr = GlobalData.GlobalData.connectionString;
            _dataAccess = new DataAccess.DataAccess(constr);
            mapperPersonal = new MapperPersonal(_dataAccess);
        }
        public List<Personal> ListaDeMenus
        {
            get { return personales; }
        }
        public void conseguirPersonales()
        {
            personales = Mapper.conseguirPersonalesList();
        }
        /*
        public int InsertarPersonal(int c, string nom,
            string pat, string mat, DateTime nac, DateTime cont,
            int sueld, int cel, string dir)
        {
            var per = new Personal(Mapper, _dataAccess, c, nom, pat, mat, nac, cont, sueld, cel, dir);
            return per.Persists();
        }*/

        /*
        public int borrarMenuDelIndice(int index, out string message)
        {
            int recordsAffected = Mapper.borrarMenu(menus[index], out message);
            if (recordsAffected == 1)
            {
                menus.RemoveAt(index);
            }
            return recordsAffected;
        }
        */
        public Personal conseguirUnPersonal(int ci)
        {
            Personal user;
            user = Mapper.conseguirPersonal(ci);
            return user;
        }
    }
}
