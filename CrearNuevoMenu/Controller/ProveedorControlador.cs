﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Mapper;
using CrearNuevoMenu.Model;
using CrearNuevoMenu.GlobalData;

namespace CrearNuevoMenu.Controller
{
    public class ProveedorControlador
    {
        private DataAccess.DataAccess _dataAccess;
        private MapperProveedor mapperProveedor;
        private List<Proveedor> Proveedors;
        private List<string> nombreDeProveedores;
        private MapperProveedor Mapper
        {
            get { return mapperProveedor; }
        }
        public ProveedorControlador()
        {
            string constr = GlobalData.GlobalData.connectionString;

            _dataAccess = new DataAccess.DataAccess(constr);
            mapperProveedor = new MapperProveedor(_dataAccess);

            conseguirProveedors();
            conseguirNombresDeProoveedores();
        }
        public List<Proveedor> ListaDeProveedor
        {
            get { return Proveedors; }
        }
        public List<string> ListaNombreProv
        {
            get { return nombreDeProveedores; }
        }
        public void conseguirProveedors()
        {
            Proveedors = Mapper.conseguirProveedorsList();
        }
        public void conseguirNombresDeProoveedores()
        {
            nombreDeProveedores = Mapper.conseguirNombresDeProveedorsList();
        }


        public int InsertarProveedor(int CIProveedor, string nombre, string email, string dirLocal, int numCel)
        {
            var men = new Proveedor(Mapper, _dataAccess, CIProveedor, nombre, email, dirLocal, numCel);
            return men.Persists();
        }
        public int borrarProveedorDelIndice(int index, out string message)
        {
            int recordsAffected = Mapper.borrarProveedor(Proveedors[index], out message);
            if (recordsAffected == 1)
            {
                Proveedors.RemoveAt(index);
            }
            return recordsAffected;
        }
      
        public void conseguirProveedorDelIndice(int index, out int CIProveedor, out string nombre, out string email, out string dirLocal, out int numCel)
        {
            CIProveedor = Proveedors[index].ci;
            nombre = Proveedors[index].nombre;
            dirLocal = Proveedors[index].dirLocal;
            email = Proveedors[index].email;
            numCel = Proveedors[index].numCel;
        }
        public int guardarProveedorDeMenuModificado(out string message)
        {
            var airportsModified = Proveedors.Where(x => x.IsDirty).ToList();
            string outError;

            message = "";

            int count = 0;
            foreach (var insu in airportsModified)
            {
                int recordsAffected = Mapper.ModificiarProveedor(insu, out outError);
                if (recordsAffected < 1)
                {
                    message = message + "Error Modificando " + insu.nombre + " : " + outError;
                }
                count += recordsAffected;
            }

            return count;
        }
        public int devolverIDDeNombre(string nombre)
        {
            for (int i = 0; i < Proveedors.Count; i++)
            {
                if (Proveedors[i].nombre == nombre)
                {
                    return Proveedors[i].ci;
                }
            }
            return -1;
        }
    }
}
