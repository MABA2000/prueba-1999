﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Mapper;
namespace CrearNuevoMenu.Model
{
    class Credencial
    {
        private MapperCredencial mapper;
        private DataAccess.DataAccess dataAccess;
        public string email { get; set; }
        public int ci { get; set; }
        public string contraseña { get; set; }
        public string asociacion { get; set; }
        public bool encontrado { get; set; }
        public void limpiar()
        {
            email = "";
            ci = 0;
            contraseña = "";
            asociacion = "";
            encontrado = false;
        }
        internal Credencial()
        {
            limpiar();
        }
        public Credencial(MapperCredencial map, DataAccess.DataAccess data)
        {
            mapper = map;
            dataAccess = data;
        }
        public Credencial(MapperCredencial map, DataAccess.DataAccess data,
            string ema, int c, string cont, string rol) : this(map, data)
        {
            email = ema;
            ci = c;
            contraseña = cont;
            asociacion = rol;
        }
        /*
        public int Persists()
        {
            //string out_error;
            //return mapper.InsertarMenu(this, out out_error);
        }
        */
    }
}
