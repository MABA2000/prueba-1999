﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Mapper;
using CrearNuevoMenu.Controller;
using CrearNuevoMenu.CrearMenuForm;
using System.Windows.Forms;

namespace CrearNuevoMenu.Model
{
    public class Entrega
    {
        private MapperEntrega mapper;
        private DataAccess.DataAccess dataAccess;
        public int CodEntrega { get; set; }
        public int CodInsumo { get; set; }
        public int CIProveedor { get; set; }
        public float PrecioUnidad { get; set; }
        public float Cantidad { get; set; }
        public DateTime fechaDeEntrega { get; set; }
        public bool IsDirty { get; set; }
        internal Entrega()
        {
            CodEntrega = 0;
            CodInsumo = 0;
            CIProveedor = 0;
            PrecioUnidad = 0;
            Cantidad = 0;
            fechaDeEntrega = DateTime.Today;
        }
        public Entrega(MapperEntrega map, DataAccess.DataAccess data)
        {
            mapper = map;
            dataAccess = data;
        }
        public Entrega(MapperEntrega map, DataAccess.DataAccess data,int codEntrega,int CodInsumo, int CIProveedor, float PrecioUnidad, float Cantidad,DateTime fecha) : this(map, data)
        {
            this.CodEntrega = codEntrega;
            this.CodInsumo = CodInsumo;
            this.CIProveedor = CIProveedor;
            this.PrecioUnidad = PrecioUnidad;
            this.Cantidad = Cantidad;
            this.fechaDeEntrega = fecha;
            IsDirty = false;
        }
        public int Persists()
        {
            string out_error;
            return mapper.InsertarEntrega(this, out out_error);
        }
        public int PersistsEditar()
        {
            string out_error;
            return mapper.ModificiarEntrega(this, out out_error);
        }
    }
}
