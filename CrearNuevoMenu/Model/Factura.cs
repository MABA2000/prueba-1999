﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Mapper;
namespace CrearNuevoMenu.Model
{
    public class Factura
    {
        private MapperFactura mapper;
        private DataAccess.DataAccess dataAccess;
        public int codFac { get; set; }
        public DateTime horaEmision { get; set; }
        public DateTime fechaEmision { get; set; }
        public int ciCliente { get; set; }
        public int codOrden { get; set; }
        public bool huboCambios { get; set; }
        internal Factura()
        {
            codFac = 0;
            horaEmision = DateTime.Now;
            fechaEmision = DateTime.Today;
            ciCliente = 0;
            codOrden = 0;
            huboCambios = false;
        }
        public DateTime conseguirHoraEmision()
        {
            return horaEmision;
        }
        public DateTime conseguirFechaEmision()
        {
            return fechaEmision;
        }
        public Factura(MapperFactura map, DataAccess.DataAccess data)
        {
            mapper = map;
            dataAccess = data;
        }
        public Factura(MapperFactura map, DataAccess.DataAccess data, int id,DateTime hora, DateTime fecha, int ciCli,
            int codOrd) : this(map, data)
        {
            codFac = id;
            horaEmision = hora;
            fechaEmision = fecha;
            ciCliente = ciCli;
            codOrden = codOrd;
            huboCambios = false;
        }
        public int Persists()
        {
            string out_error;
            return mapper.InsertarFactura(this, out out_error);
        }
    }
}
