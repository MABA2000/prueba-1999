﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Mapper;
namespace CrearNuevoMenu.Model
{
    public class Menu
    {
        private MapperMenu mapper;
        private DataAccess.DataAccess dataAccess;
        public int Id { get; set; }
        public string nombre { get; set; }
        public DateTime fecha { get; set; }
        public string descripcion { get; set; }
        //public int cantidad { get; set; }
        public bool visible { get; set; }
        public bool IsDirty;
        internal Menu()
        {
            Id = 0;
            nombre = "";
            fecha = DateTime.Today;
            descripcion = "";
          //  cantidad = 0;
            IsDirty = false;
            visible = false;
        }
        public DateTime conseguirFecha()
        {
            return fecha;
        }
        public Menu(MapperMenu map, DataAccess.DataAccess data)
        {
            mapper = map;
            dataAccess = data;
        }
        public Menu(MapperMenu map, DataAccess.DataAccess data, int id,
            string nom, DateTime fec, string desc, bool vis) : this(map, data)
        {
            Id = id;
            nombre = nom;
            fecha = fec;
            descripcion = desc;
        //    cantidad = cant;
            IsDirty = false;
            visible = vis;
        }
        public int Persists()
        {
            string out_error;
            return mapper.InsertarMenu(this, out out_error);
        }
    }
}
