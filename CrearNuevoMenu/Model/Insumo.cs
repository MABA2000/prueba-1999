﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Controller;
using CrearNuevoMenu.CrearMenuForm;
using CrearNuevoMenu.Mapper;
using System.Windows.Forms;

namespace CrearNuevoMenu.Model
{
    public class Insumo
    {
        private MapperInsumo mapper;
        private DataAccess.DataAccess dataAccess;
        public int CodItem { get; set; }//
        public int codInsumo { get; set; }
        public string nombre { get; set; }
        public string unidad { get; set; }
        public double Medida { get; set; }
        public float cantidad { get; set; }
        public bool IsDirty;
        internal Insumo()
        {
            CodItem = -1;
            codInsumo = 0;
            nombre = "";
            unidad = "";
            Medida = 0;
            cantidad =0;
            IsDirty = false;
        }
        public Insumo(MapperInsumo map, DataAccess.DataAccess data)
        {
            mapper = map;
            dataAccess = data;
        }
        public Insumo(MapperInsumo mapper, DataAccess.DataAccess dac, int CodInsumo, int CodItem, string Nombre, string unidad, double Medida, int Cantidad) : this(mapper, dac)//para combinar tablas
        {
            this.CodItem = CodItem;
            this.codInsumo = CodInsumo;
            this.nombre = Nombre;
            this.unidad = unidad;
            this.Medida = Medida;
            this.cantidad = Cantidad;
        }
        public Insumo(MapperInsumo map, DataAccess.DataAccess data, int codInsumo, string nombre, float cantidad, string unidad) : this(map, data)
        {
            this.codInsumo = codInsumo;
            this.nombre = nombre;
            this.cantidad = cantidad;
            this.unidad = unidad;
            IsDirty = false;
        }
        public int Persists()
        {
            string out_error;
            return mapper.InsertarInsumo(this, out out_error);
        }
        public int Persists2() 
        {
            string out_error;
            return mapper.InsertarInsumoDeUnItem(this, out out_error);
        }
    }
}
