﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Mapper;
namespace CrearNuevoMenu.Model
{
    public class Orden
    {
        private MapperOrden mapper;
        private DataAccess.DataAccess dataAccess;
        public int codOrden { get; set; }
        public bool estadoItem { get; set; }
        public int numeroMesa { get; set; }
        public DateTime horaDeFinalizacion { get; set; }
        public bool estaLimpio { get; set; }
        internal Orden()
        {
            codOrden = 0;
            estadoItem = false;
            numeroMesa = 0;
            horaDeFinalizacion = DateTime.Today;
            estaLimpio = false;
        }
        public Orden(MapperOrden map, DataAccess.DataAccess data)
        {
            mapper = map;
            dataAccess = data;
        }
        public Orden(MapperOrden map, DataAccess.DataAccess data, int cod,
            bool estado, int num, DateTime hora) : this(map, data)
        {
            codOrden = cod;
            estadoItem = estado;
            numeroMesa = num;
            horaDeFinalizacion = hora;
        }
        public int Persists()
        {
            string out_error;
            return mapper.InsertarOrden(this, out out_error);
        }
    }
}
