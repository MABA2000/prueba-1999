﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Mapper;


namespace CrearNuevoMenu.Model
{
    public class ItemDeMenu
    {
        private MapperItemDeMenu mapper;
        private DataAccess.DataAccess dataAccess;
        public int codItem { get; set; }
        public int codMenu { get; set; }
        public float precio { get; set; }
        public DateTime fechaDeAdicion { get; set; }
        public bool IsDirty { get; set; }
        internal ItemDeMenu()
        {
            codItem = 0;
            codMenu = 0;
            precio = 0;
            fechaDeAdicion = DateTime.Today;
            IsDirty = false;
        }
        public ItemDeMenu(MapperItemDeMenu map, DataAccess.DataAccess data)
        {
            mapper = map;
            dataAccess = data;
        }
        public ItemDeMenu(MapperItemDeMenu map, DataAccess.DataAccess data, int codItem, int codMenu, float precio, DateTime fechaDeAdicion) : this(map, data)
        {
            this.codItem = codItem;
            this.codMenu = codMenu;
            this.precio = precio;
            this.fechaDeAdicion = fechaDeAdicion;
            IsDirty = false;
        }
        public int Persists()
        {
            string out_error;
            return mapper.InsertarItemDeMenu(this, out out_error);
        }
    }
}
