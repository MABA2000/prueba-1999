﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Mapper;
namespace CrearNuevoMenu.Model
{
    public class Personal
    {
        private MapperPersonal mapper;
        private DataAccess.DataAccess dataAccess;
        public int ci { get; set; }
        public string nombre { get; set; }
        public string apellidoPat { get; set; }
        public string apellidoMat { get; set; }

        public DateTime fechaNac { get; set; }
        public DateTime fechaCont { get; set; }
        public int sueldo { get; set; }
        public int numCel { get; set; }
        public string direccion { get; set; }
        public bool IsDirty;
        internal Personal()
        {
            ci = 0;
            nombre = "";
            apellidoPat = "";
            apellidoMat = "";
            fechaNac = DateTime.Today;
            fechaCont = DateTime.Today;
            sueldo = 0;
            numCel = 0;
            direccion = "";
            IsDirty = false;
        }
        public Personal(MapperPersonal map, DataAccess.DataAccess data)
        {
            mapper = map;
            dataAccess = data;
        }
        public Personal(MapperPersonal map, DataAccess.DataAccess data, int c, string nom,
            string pat, string mat, DateTime nac, DateTime cont,
            int sueld, int cel, string dir) : this(map, data)
        {
            ci = c;
            nombre = nom;
            apellidoPat = pat;
            apellidoMat = mat;
            fechaNac = nac;
            fechaCont = cont;
            sueldo = sueld;
            numCel = cel;
            direccion = dir;
            IsDirty = false;
        }
    }
}


