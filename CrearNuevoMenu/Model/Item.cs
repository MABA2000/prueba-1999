﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Mapper;
using System.Windows.Forms;
namespace CrearNuevoMenu.Model
{
    public class Item
    {
        private MapperItem mapper;
        private DataAccess.DataAccess dataAccess;
        public int codItem { get; set; }
        public string nombre { get; set; }
        public int tiempoElaboracion { get; set; }
        public int GradoAlc { get; set; }
        public int Calorias { get; set; }
        public float precioDeElaboracion { get; set; }
        public string descripcion { get; set; }
        public string tipo{ get; set; }
        public int ci { get; set; } 
        public bool IsDirty;
        internal Item()
        {
            codItem = 0;
            nombre = "";
            tiempoElaboracion = 0;
            GradoAlc = 0;
            Calorias = 0;
            precioDeElaboracion = 0;
            descripcion = "";
            tipo = "";
            IsDirty = false;
        }
        public Item(MapperItem map, DataAccess.DataAccess data)
        {
            mapper = map;
            dataAccess = data;
        }
        public Item(MapperItem map, DataAccess.DataAccess data, int codItem, string nombre, int tiempoElaboracion,int gradoAlc,int calorias, float precioDeElaboracion , string descripcion,string tipo,int ci) : this(map, data)
        {
            this.codItem = codItem;
            this.nombre = nombre;
            this.tiempoElaboracion = tiempoElaboracion;
            this.GradoAlc = gradoAlc;
            this.Calorias = calorias;
            this.precioDeElaboracion = precioDeElaboracion;
            this.descripcion = descripcion;
            this.tipo = tipo;
            this.ci = ci;
            IsDirty = false;
        }

        public int Persists()
        {
            string out_error;

            return mapper.InsertItem(this, out out_error);
        }
    }
}
