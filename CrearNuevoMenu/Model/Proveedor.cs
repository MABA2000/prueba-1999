﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrearNuevoMenu.Mapper;

namespace CrearNuevoMenu.Model
{
    public class Proveedor
    {
        private MapperProveedor mapper;
        private DataAccess.DataAccess dataAccess;
        public int ci { get; set; }
        public int numCel { get; set; }
        public string nombre { get; set; }
        public string dirLocal { get; set; }
        public string email { get; set; }
        public bool IsDirty;
        internal Proveedor()
        {
            ci = 0;
            nombre = "";
            email = "";
            dirLocal = "";
            numCel = 0;
            IsDirty = false;
        }
        public Proveedor(MapperProveedor map, DataAccess.DataAccess data)
        {
            mapper = map;
            dataAccess = data;
        }
        public Proveedor(MapperProveedor map, DataAccess.DataAccess data, int ci, string nombre, string email, string dirLocal,int numCel) : this(map, data)
        {
            this.ci = ci;
            this.nombre = nombre;
            this.email = email;
            this.dirLocal = dirLocal;
            this.numCel = numCel;
            IsDirty = false;
        }
        public int Persists()
        {
            string out_error;
            return mapper.InsertarProveedor(this, out out_error);
        }
    }
}
